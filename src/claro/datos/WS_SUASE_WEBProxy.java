package claro.datos;

public class WS_SUASE_WEBProxy implements claro.datos.WS_SUASE_WEB {
  private String _endpoint = null;
  private claro.datos.WS_SUASE_WEB wS_SUASE_WEB = null;
  
  public WS_SUASE_WEBProxy() {
    _initWS_SUASE_WEBProxy();
  }
  
  public WS_SUASE_WEBProxy(String endpoint) {
    _endpoint = endpoint;
    _initWS_SUASE_WEBProxy();
  }
  
  private void _initWS_SUASE_WEBProxy() {
    try {
      wS_SUASE_WEB = (new claro.datos.WS_SUASE_WEBServiceLocator()).getWS_SUASE_WEBPort();
      if (wS_SUASE_WEB != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wS_SUASE_WEB)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wS_SUASE_WEB)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wS_SUASE_WEB != null)
      ((javax.xml.rpc.Stub)wS_SUASE_WEB)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public claro.datos.WS_SUASE_WEB getWS_SUASE_WEB() {
    if (wS_SUASE_WEB == null)
      _initWS_SUASE_WEBProxy();
    return wS_SUASE_WEB;
  }
  
  public java.lang.String ingresarSolicitudWeb(java.lang.String usuarioApp, java.lang.String claveApp, java.lang.String canalApp, java.lang.String entidad, java.lang.String motivo, java.lang.String concesionario, java.lang.String numeroOrigen, java.lang.String fecha, java.lang.String hora, java.lang.String codReferencia) throws java.rmi.RemoteException{
    if (wS_SUASE_WEB == null)
      _initWS_SUASE_WEBProxy();
    return wS_SUASE_WEB.ingresarSolicitudWeb(usuarioApp, claveApp, canalApp, entidad, motivo, concesionario, numeroOrigen, fecha, hora, codReferencia);
  }
  
  public java.lang.String cambioClave(java.lang.String usuarioApp, java.lang.String claveApp, java.lang.String canalApp, java.lang.String usuario, java.lang.String claveAnt, java.lang.String claveNueva, java.lang.String claveConfirma) throws java.rmi.RemoteException{
    if (wS_SUASE_WEB == null)
      _initWS_SUASE_WEBProxy();
    return wS_SUASE_WEB.cambioClave(usuarioApp, claveApp, canalApp, usuario, claveAnt, claveNueva, claveConfirma);
  }
  
  
}