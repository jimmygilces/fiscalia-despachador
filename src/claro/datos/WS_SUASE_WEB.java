/**
 * WS_SUASE_WEB.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package claro.datos;

public interface WS_SUASE_WEB extends java.rmi.Remote {
    public java.lang.String ingresarSolicitudWeb(java.lang.String usuarioApp, java.lang.String claveApp, java.lang.String canalApp, java.lang.String entidad, java.lang.String motivo, java.lang.String concesionario, java.lang.String numeroOrigen, java.lang.String fecha, java.lang.String hora, java.lang.String codReferencia) throws java.rmi.RemoteException;
    public java.lang.String cambioClave(java.lang.String usuarioApp, java.lang.String claveApp, java.lang.String canalApp, java.lang.String usuario, java.lang.String claveAnt, java.lang.String claveNueva, java.lang.String claveConfirma) throws java.rmi.RemoteException;
}
