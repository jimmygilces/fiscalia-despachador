/**
 * WS_SUASE_WEBService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package claro.datos;

public interface WS_SUASE_WEBService extends javax.xml.rpc.Service {
    public java.lang.String getWS_SUASE_WEBPortAddress();

    public claro.datos.WS_SUASE_WEB getWS_SUASE_WEBPort() throws javax.xml.rpc.ServiceException;

    public claro.datos.WS_SUASE_WEB getWS_SUASE_WEBPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
