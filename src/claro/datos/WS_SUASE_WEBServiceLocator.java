/**
 * WS_SUASE_WEBServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package claro.datos;

public class WS_SUASE_WEBServiceLocator extends org.apache.axis.client.Service implements claro.datos.WS_SUASE_WEBService {

    public WS_SUASE_WEBServiceLocator() {
    }


    public WS_SUASE_WEBServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WS_SUASE_WEBServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WS_SUASE_WEBPort
    private java.lang.String WS_SUASE_WEBPort_address = "http://ggcchubservice01:7777/ws_suase/WS_SUASE_WEB";

    public java.lang.String getWS_SUASE_WEBPortAddress() {
        return WS_SUASE_WEBPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WS_SUASE_WEBPortWSDDServiceName = "WS_SUASE_WEBPort";

    public java.lang.String getWS_SUASE_WEBPortWSDDServiceName() {
        return WS_SUASE_WEBPortWSDDServiceName;
    }

    public void setWS_SUASE_WEBPortWSDDServiceName(java.lang.String name) {
        WS_SUASE_WEBPortWSDDServiceName = name;
    }

    public claro.datos.WS_SUASE_WEB getWS_SUASE_WEBPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WS_SUASE_WEBPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWS_SUASE_WEBPort(endpoint);
    }

    public claro.datos.WS_SUASE_WEB getWS_SUASE_WEBPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            claro.datos.WS_SUASE_WEBPortBindingStub _stub = new claro.datos.WS_SUASE_WEBPortBindingStub(portAddress, this);
            _stub.setPortName(getWS_SUASE_WEBPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWS_SUASE_WEBPortEndpointAddress(java.lang.String address) {
        WS_SUASE_WEBPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (claro.datos.WS_SUASE_WEB.class.isAssignableFrom(serviceEndpointInterface)) {
                claro.datos.WS_SUASE_WEBPortBindingStub _stub = new claro.datos.WS_SUASE_WEBPortBindingStub(new java.net.URL(WS_SUASE_WEBPort_address), this);
                _stub.setPortName(getWS_SUASE_WEBPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WS_SUASE_WEBPort".equals(inputPortName)) {
            return getWS_SUASE_WEBPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://datos.claro/", "WS_SUASE_WEBService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://datos.claro/", "WS_SUASE_WEBPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WS_SUASE_WEBPort".equals(portName)) {
            setWS_SUASE_WEBPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
