/**
 * Eis_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package axis.EISApiOnlineWS_wsdl.types;

public interface Eis_PortType extends java.rmi.Remote {
    public axis.EISApiOnlineWS_wsdl.types.EipConsumeServicioResponseElement eipConsumeServicio(axis.EISApiOnlineWS_wsdl.types.EipConsumeServicioElement parameters) throws java.rmi.RemoteException;
    public axis.EISApiOnlineWS_wsdl.types.TestConnectionResponseElement testConnection(axis.EISApiOnlineWS_wsdl.types.TestConnectionElement parameters) throws java.rmi.RemoteException;
}
