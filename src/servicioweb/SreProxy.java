package servicioweb;

public class SreProxy implements servicioweb.Sre_PortType {
  private String _endpoint = null;
  private servicioweb.Sre_PortType sre_PortType = null;
  
  public SreProxy() {
    _initSreProxy();
  }
  
  public SreProxy(String endpoint) {
    _endpoint = endpoint;
    _initSreProxy();
  }
  
  private void _initSreProxy() {
    try {
      sre_PortType = (new servicioweb.Sre_ServiceLocator()).getsreSoapHttpPort();
      if (sre_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)sre_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)sre_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (sre_PortType != null)
      ((javax.xml.rpc.Stub)sre_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servicioweb.Sre_PortType getSre_PortType() {
    if (sre_PortType == null)
      _initSreProxy();
    return sre_PortType;
  }
  
  public servicioweb.SreConsultaTransaccionResponseElement sreConsultaTransaccion(servicioweb.SreConsultaTransaccionElement parameters) throws java.rmi.RemoteException{
    if (sre_PortType == null)
      _initSreProxy();
    return sre_PortType.sreConsultaTransaccion(parameters);
  }
  
  public servicioweb.SreReceptaTransaccionResponseElement sreReceptaTransaccion(servicioweb.SreReceptaTransaccionElement parameters) throws java.rmi.RemoteException{
    if (sre_PortType == null)
      _initSreProxy();
    return sre_PortType.sreReceptaTransaccion(parameters);
  }
  
  
}