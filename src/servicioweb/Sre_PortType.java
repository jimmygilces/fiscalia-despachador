/**
 * Sre_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicioweb;

public interface Sre_PortType extends java.rmi.Remote {
    public servicioweb.SreConsultaTransaccionResponseElement sreConsultaTransaccion(servicioweb.SreConsultaTransaccionElement parameters) throws java.rmi.RemoteException;
    public servicioweb.SreReceptaTransaccionResponseElement sreReceptaTransaccion(servicioweb.SreReceptaTransaccionElement parameters) throws java.rmi.RemoteException;
}
