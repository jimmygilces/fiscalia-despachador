package com.rgt.objetos;

public class Suase {
	String id_solicitud;
	String nombre_completo;
	String provincia;
	String cantonCiudad;
	String direccion;
	String celda;
	String nombreCelda;
	String longitud;
	String latitud;
	String azimut;
	String metodoConsulta;   //CLS JTO 02102018
	public Suase(String id_solicitud, String nombre_completo, String provincia,
			String cantonCiudad, String direccion, String celda,String nombreCelda,
			String longitud, String latitud, String azimut,String metodoConsulta) {  //CLS JTO 19092018
		super();
		this.id_solicitud = id_solicitud;
		this.nombre_completo = nombre_completo;
		this.provincia = provincia;
		this.cantonCiudad = cantonCiudad;
		this.direccion = direccion;
		this.celda = celda;
		this.nombreCelda = nombreCelda;
		this.longitud = longitud;
		this.latitud = latitud;
		this.azimut = azimut;
		this.metodoConsulta= metodoConsulta;  //CLS JTO 02102018
	}
	
	public Suase()
	{}

	
	public String getNombreCelda() {
		return nombreCelda;
	}

	public void setNombreCelda(String nombreCelda) {
		this.nombreCelda = nombreCelda;
	}

	public String getId_solicitud() {
		return id_solicitud;
	}

	public void setId_solicitud(String id_solicitud) {
		this.id_solicitud = id_solicitud;
	}

	public String getNombre_completo() {
		return nombre_completo;
	}

	public void setNombre_completo(String nombre_completo) {
		this.nombre_completo = nombre_completo;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCantonCiudad() {
		return cantonCiudad;
	}

	public void setCantonCiudad(String cantonCiudad) {
		this.cantonCiudad = cantonCiudad;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCelda() {
		return celda;
	}

	public void setCelda(String celda) {
		this.celda = celda;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getAzimut() {
		return azimut;
	}

	public void setAzimut(String azimut) {
		this.azimut = azimut;
	}
	 //INI CLS JTO 02102018	
		public String getMetodoConsulta() {
			return metodoConsulta;
		}

		public void setMetodoConsulta(String metodoConsulta) {
			this.metodoConsulta = metodoConsulta;
		}
	 //FIN CLS JTO 02102018	
	
}


