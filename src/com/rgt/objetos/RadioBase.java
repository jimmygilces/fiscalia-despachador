package com.rgt.objetos;

public class RadioBase {
	
	String id_solicitud;
	String ID_CELDA;
	String PROVINCIA;
	String CANTON;
	String CIUDAD_PARROQUIA;
	String NOMBRE_ESTACION;
	String DIRECCION;
	String LATITUD;
	String LONGITUD;
	
	public RadioBase()
	{}

	public RadioBase(String id_solicitud, String iD_CELDA, String pROVINCIA,
			String cANTON, String cIUDAD_PARROQUIA, String nOMBRE_ESTACION,
			String dIRECCION, String lATITUD, String lONGITUD) {
		super();
		this.id_solicitud = id_solicitud;
		ID_CELDA = iD_CELDA;
		PROVINCIA = pROVINCIA;
		CANTON = cANTON;
		CIUDAD_PARROQUIA = cIUDAD_PARROQUIA;
		NOMBRE_ESTACION = nOMBRE_ESTACION;
		DIRECCION = dIRECCION;
		LATITUD = lATITUD;
		LONGITUD = lONGITUD;
	}

	public String getId_solicitud() {
		return id_solicitud;
	}

	public void setId_solicitud(String id_solicitud) {
		this.id_solicitud = id_solicitud;
	}

	public String getID_CELDA() {
		return ID_CELDA;
	}

	public void setID_CELDA(String iD_CELDA) {
		ID_CELDA = iD_CELDA;
	}

	public String getPROVINCIA() {
		return PROVINCIA;
	}

	public void setPROVINCIA(String pROVINCIA) {
		PROVINCIA = pROVINCIA;
	}

	public String getCANTON() {
		return CANTON;
	}

	public void setCANTON(String cANTON) {
		CANTON = cANTON;
	}

	public String getCIUDAD_PARROQUIA() {
		return CIUDAD_PARROQUIA;
	}

	public void setCIUDAD_PARROQUIA(String cIUDAD_PARROQUIA) {
		CIUDAD_PARROQUIA = cIUDAD_PARROQUIA;
	}

	public String getNOMBRE_ESTACION() {
		return NOMBRE_ESTACION;
	}

	public void setNOMBRE_ESTACION(String nOMBRE_ESTACION) {
		NOMBRE_ESTACION = nOMBRE_ESTACION;
	}

	public String getDIRECCION() {
		return DIRECCION;
	}

	public void setDIRECCION(String dIRECCION) {
		DIRECCION = dIRECCION;
	}

	public String getLATITUD() {
		return LATITUD;
	}

	public void setLATITUD(String lATITUD) {
		LATITUD = lATITUD;
	}

	public String getLONGITUD() {
		return LONGITUD;
	}

	public void setLONGITUD(String lONGITUD) {
		LONGITUD = lONGITUD;
	}

	
	
	

}
