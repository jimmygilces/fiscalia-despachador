package com.rgt.objetos;

public class ReporteFiscalia {
  private String codReporte;
  
  private String servicio;
  
  private String cuentaCelular;
  
  private String nombre;
  
  private String identificacion;
  
  private String ciudad;
  
  private String imei;
  
  private String simcard;
  
  private String direccion;
  
  private String telefono;
  
  private String fechaActicacion;
  
  private String fechaInactivacion;
  
  private String formaPago;
  
  private String producto;
  
  private String plan;
  
  private String tiempoInicial;
  
  private String tiempoFinal;
  
  private String duracionLLamada;
  
  private String oficio;
  
  private String requerimiento;
  
  private String referencia;
  
  private String observacion;
  
  public ReporteFiscalia() {}
  
  public ReporteFiscalia(String pCodReporte, String pServicio, String pOficio, String pReferencia, String pCuentaCelular, String pNombre, String pIdentificacion, String pCiudad, String pImei, String pSimcard, String pDireccion, String ptelefono, String pFechaActivacion, String pFechaInactiva, String pFormaPago, String pProducto, String pPlan) {
    this.codReporte = pCodReporte;
    this.servicio = pServicio;
    this.cuentaCelular = pCuentaCelular;
    this.nombre = pNombre;
    this.identificacion = pIdentificacion;
    this.ciudad = pCiudad;
    this.imei = pImei;
    this.simcard = pSimcard;
    this.direccion = pDireccion;
    this.telefono = ptelefono;
    this.fechaActicacion = pFechaActivacion;
    this.fechaInactivacion = pFechaInactiva;
    this.formaPago = pFormaPago;
    this.producto = pProducto;
    this.plan = pPlan;
    this.oficio = pOficio;
    this.referencia = pReferencia;
  }
  
  public String getCodReporte() {
    return this.codReporte;
  }
  
  public void setCodReporte(String codReporte) {
    this.codReporte = codReporte;
  }
  
  public String getServicio() {
    return this.servicio;
  }
  
  public void setServicio(String servicio) {
    this.servicio = servicio;
  }
  
  public String getCuentaCelular() {
    return this.cuentaCelular;
  }
  
  public void setCuentaCelular(String cuentaCelular) {
    this.cuentaCelular = cuentaCelular;
  }
  
  public String getNombre() {
    return this.nombre;
  }
  
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  
  public String getIdentificacion() {
    return this.identificacion;
  }
  
  public void setIdentificacion(String identificacion) {
    this.identificacion = identificacion;
  }
  
  public String getCiudad() {
    return this.ciudad;
  }
  
  public void setCiudad(String ciudad) {
    this.ciudad = ciudad;
  }
  
  public String getImei() {
    return this.imei;
  }
  
  public void setImei(String imei) {
    this.imei = imei;
  }
  
  public String getSimcard() {
    return this.simcard;
  }
  
  public void setSimcard(String simcard) {
    this.simcard = simcard;
  }
  
  public String getDireccion() {
    return this.direccion;
  }
  
  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }
  
  public String getTelefono() {
    return this.telefono;
  }
  
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }
  
  public String getFechaActicacion() {
    return this.fechaActicacion;
  }
  
  public void setFechaActicacion(String fechaActicacion) {
    this.fechaActicacion = fechaActicacion;
  }
  
  public String getFechaInactivacion() {
    return this.fechaInactivacion;
  }
  
  public void setFechaInactivacion(String fechaInactivacion) {
    this.fechaInactivacion = fechaInactivacion;
  }
  
  public String getFormaPago() {
    return this.formaPago;
  }
  
  public void setFormaPago(String formaPago) {
    this.formaPago = formaPago;
  }
  
  public String getProducto() {
    return this.producto;
  }
  
  public void setProducto(String producto) {
    this.producto = producto;
  }
  
  public String getPlan() {
    return this.plan;
  }
  
  public void setPlan(String plan) {
    this.plan = plan;
  }
  
  public String getTiempoInicial() {
    return this.tiempoInicial;
  }
  
  public void setTiempoInicial(String tiempoInicial) {
    this.tiempoInicial = tiempoInicial;
  }
  
  public String getTiempoFinal() {
    return this.tiempoFinal;
  }
  
  public void setTiempoFinal(String tiempoFinal) {
    this.tiempoFinal = tiempoFinal;
  }
  
  public String getDuracionLLamada() {
    return this.duracionLLamada;
  }
  
  public void setDuracionLLamada(String duracionLLamada) {
    this.duracionLLamada = duracionLLamada;
  }
  
  public String getOficio() {
    return this.oficio;
  }
  
  public void setOficio(String oficio) {
    this.oficio = oficio;
  }
  
  public String getRequerimiento() {
    return this.requerimiento;
  }
  
  public void setRequerimiento(String requerimiento) {
    this.requerimiento = requerimiento;
  }
  
  public String getReferencia() {
    return this.referencia;
  }
  
  public void setReferencia(String referencia) {
    this.referencia = referencia;
  }
  
  public String getObservacion() {
    return this.observacion;
  }
  
  public void setObservacion(String observacion) {
    this.observacion = observacion;
  }
}
