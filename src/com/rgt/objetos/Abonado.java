package com.rgt.objetos;

public class Abonado {
	private String ID_SOLICITUD;
	private String NOMBRE_COMPLETO;
	private String IMEI;
	private String IMSI;
	private String ICCID;
	private String IDENTIFICACION;
	private String DIRECCION_COMPLETA;
	private String TELEFONO1;
	private String FECHA_INICIO;
	private String FECHA_FIN;
	private String ID_SERVICIO;
	private String SUBPRODUCTO;
	private String CIUDAD;
	private String DESCRIP_ADICIONAL;
	private String DESCRIP_VALOR;
	private String HOJAEXCEL;
	private String MIGRACION;
	
	public Abonado()
	{
		
	}
	public Abonado(String iD_SOLICITUD, String nOMBRE_COMPLETO, String iMEI,
			String iMSI, String iCCID, String iDENTIFICACION,
			String dIRECCION_COMPLETA, String tELEFONO1, String fECHA_INICIO,
			String fECHA_FIN, String iD_SERVICIO, String sUBPRODUCTO,
			String cIUDAD, String dESCRIP_ADICIONAL, String dESCRIP_VALOR,
			String hOJAEXCEL) {
		super();
		ID_SOLICITUD = iD_SOLICITUD;
		NOMBRE_COMPLETO = nOMBRE_COMPLETO;
		IMEI = iMEI;
		IMSI = iMSI;
		ICCID = iCCID;
		IDENTIFICACION = iDENTIFICACION;
		DIRECCION_COMPLETA = dIRECCION_COMPLETA;
		TELEFONO1 = tELEFONO1;
		FECHA_INICIO = fECHA_INICIO;
		FECHA_FIN = fECHA_FIN;
		ID_SERVICIO = iD_SERVICIO;
		SUBPRODUCTO = sUBPRODUCTO;
		CIUDAD = cIUDAD;
		DESCRIP_ADICIONAL = dESCRIP_ADICIONAL;
		DESCRIP_VALOR = dESCRIP_VALOR;
		HOJAEXCEL = hOJAEXCEL;
	}
	public Abonado(String iD_SOLICITUD, String nOMBRE_COMPLETO, String iMEI,
			String iMSI, String iCCID, String iDENTIFICACION,
			String dIRECCION_COMPLETA, String tELEFONO1, String fECHA_INICIO,
			String fECHA_FIN, String iD_SERVICIO, String sUBPRODUCTO,
			String cIUDAD, String dESCRIP_ADICIONAL, String dESCRIP_VALOR,
			String hOJAEXCEL, String mIGRACION) {
		super();
		ID_SOLICITUD = iD_SOLICITUD;
		NOMBRE_COMPLETO = nOMBRE_COMPLETO;
		IMEI = iMEI;
		IMSI = iMSI;
		ICCID = iCCID;
		IDENTIFICACION = iDENTIFICACION;
		DIRECCION_COMPLETA = dIRECCION_COMPLETA;
		TELEFONO1 = tELEFONO1;
		FECHA_INICIO = fECHA_INICIO;
		FECHA_FIN = fECHA_FIN;
		ID_SERVICIO = iD_SERVICIO;
		SUBPRODUCTO = sUBPRODUCTO;
		CIUDAD = cIUDAD;
		DESCRIP_ADICIONAL = dESCRIP_ADICIONAL;
		DESCRIP_VALOR = dESCRIP_VALOR;
		HOJAEXCEL = hOJAEXCEL;
		MIGRACION = mIGRACION;
	}





	public String getDESCRIP_ADICIONAL() {
		return DESCRIP_ADICIONAL;
	}
	public void setDESCRIP_ADICIONAL(String dESCRIP_ADICIONAL) {
		DESCRIP_ADICIONAL = dESCRIP_ADICIONAL;
	}

	public String getDESCRIP_VALOR() {
		return DESCRIP_VALOR;
	}
	public void setDESCRIP_VALOR(String dESCRIP_VALOR) {
		DESCRIP_VALOR = dESCRIP_VALOR;
	}
	public String getHOJAEXCEL() {
		return HOJAEXCEL;
	}
	public void setHOJAEXCEL(String hOJAEXCEL) {
		HOJAEXCEL = hOJAEXCEL;
	}
	public String getID_SERVICIO() {
		return ID_SERVICIO;
	}
	public void setID_SERVICIO(String iD_SERVICIO) {
		ID_SERVICIO = iD_SERVICIO;
	}
	public String getFECHA_INICIO() {
		return FECHA_INICIO;
	}

	public void setFECHA_INICIO(String fECHA_INICIO) {
		FECHA_INICIO = fECHA_INICIO;
	}

	public String getFECHA_FIN() {
		return FECHA_FIN;
	}

	public void setFECHA_FIN(String fECHA_FIN) {
		FECHA_FIN = fECHA_FIN;
	}

	public String getCIUDAD() {
		return CIUDAD;
	}
	public void setCIUDAD(String cIUDAD) {
		CIUDAD = cIUDAD;
	}

	public String getID_SOLICITUD() {
		return ID_SOLICITUD;
	}

	public void setID_SOLICITUD(String iD_SOLICITUD) {
		ID_SOLICITUD = iD_SOLICITUD;
	}

	public String getNOMBRE_COMPLETO() {
		return NOMBRE_COMPLETO;
	}

	public void setNOMBRE_COMPLETO(String nOMBRE_COMPLETO) {
		NOMBRE_COMPLETO = nOMBRE_COMPLETO;
	}

	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String iMEI) {
		IMEI = iMEI;
	}

	public String getIMSI() {
		return IMSI;
	}

	public void setIMSI(String iMSI) {
		IMSI = iMSI;
	}

	public String getICCID() {
		return ICCID;
	}

	public void setICCID(String iCCID) {
		ICCID = iCCID;
	}

	public String getIDENTIFICACION() {
		return IDENTIFICACION;
	}

	public void setIDENTIFICACION(String iDENTIFICACION) {
		IDENTIFICACION = iDENTIFICACION;
	}

	public String getDIRECCION_COMPLETA() {
		return DIRECCION_COMPLETA;
	}

	public void setDIRECCION_COMPLETA(String dIRECCION_COMPLETA) {
		DIRECCION_COMPLETA = dIRECCION_COMPLETA;
	}

	public String getTELEFONO1() {
		return TELEFONO1;
	}

	public void setTELEFONO1(String tELEFONO1) {
		TELEFONO1 = tELEFONO1;
	}

	public String getSUBPRODUCTO() {
		return SUBPRODUCTO;
	}

	public void setSUBPRODUCTO(String sUBPRODUCTO) {
		SUBPRODUCTO = sUBPRODUCTO;
	}
	
	
	
	public String getMIGRACION() {
		return MIGRACION;
	}
	public void setMIGRACION(String mIGRACION) {
		MIGRACION = mIGRACION;
	}
	
	

}
