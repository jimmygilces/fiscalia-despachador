package com.rgt.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.rgt.conexion.ConexionO;

public class otrasValidaciones {
	
	/***
	 * Metodo para verificar si un numero de celular es de casilla de voz
	 * @param numCelular - String - solo 9 digitos
	 * @return true --> num se encuentra en lista de voiceMail (si es)<br>
	 *  	   false --> num no se encuentra en la lista (no es)
	 * @throws Exception
	 */
	public static boolean numVoiceMail(String numCelular) throws Exception {
		int lenght = numCelular.length();
		numCelular = numCelular.substring(lenght - 9);

		if (numCelular.length() != 9) {
			throw new Exception("El numero a validar " + numCelular + " contiene menos de 9 digitos!");
		}

		boolean result = false;
		ConexionO cnx = new ConexionO();
		Connection con = cnx.conectar().getConexion();

		String queryConsulta = "select 1 from fis_voice_mail where casilla = " + numCelular;

		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(queryConsulta);

		if (rs.next())
			result = true;
		rs.close();
		st.close(); // RGT-CRG
		con.close();

		return result;
	}

}
