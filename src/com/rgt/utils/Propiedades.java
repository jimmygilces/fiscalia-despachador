package com.rgt.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;


public  class Propiedades
{

	
    public Properties getPropiedades() {
        try {
 
            Properties propiedades = new Properties();

            String configPathd = "C:\\Users\\Desarrollo\\Desktop\\RESPALDO\\WorkSpaceMedidasCautelares\\properties\\home\\medidas_cautelares\\Configuracion.properties";

			String configPathp = "/procesos/home/medidas_cautelares/jars/Configuracion.properties";
            
         
            propiedades.load(new FileInputStream(configPathp));
 
            if (!propiedades.isEmpty()) {
                return propiedades;
            } else { 
                return null;
            }
        } catch (IOException ex) {
        	System.out.println("Error, No se pudo cargar archivo  'Configuracion.properties': "+ex.toString());
            ex.printStackTrace();
            return null;
        }
   }
    
    public static Logger getMyLogger(){
    	
    	try{ 
    		Logger log = org.apache.logging.log4j.LogManager.getLogger("logger");
  
    		String directd = "C:\\Users\\Desarrollo\\Desktop\\medcaud\\log4j2Fis.properties";
    		 
    		String directp = "/procesos/home/medidas_cautelares/jars/log4j2Fis.properties";
 
    		LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
    		File file = new File(directp);
 
    		context.setConfigLocation(file.toURI());
    		
    		return log;
    	} catch (Exception ex) {
            System.out.println("Error, No se pudo cargar la configuracion para el logger"+ex.toString());
            ex.printStackTrace();
            return null;
        }

    }


}
