package com.rgt.utils;
import org.apache.logging.log4j.Logger;
public class DigitoVerificador {
	
	static Logger log = Propiedades.getMyLogger();

	
	/*
	 * function fisf_obtiene_dig_verificador(pv_iccid In Varchar2) Return 
Varchar2 Is
     ln_resultado Number := 0;
     ln_contador  Number := length(pv_iccid);
     ln_digito    Number := 0;
     ln_auxiliar  Number := 0;
   Begin
     Loop
       Exit When ln_contador < 1;
       ln_digito := to_number(substr(pv_iccid, ln_contador, 1));
       If Mod(ln_contador, 2) = 0 Then
         ln_auxiliar := ln_digito * 2;
         If ln_auxiliar > 9 Then
           ln_resultado := ln_resultado +
                           
(to_number(substr(rtrim(ltrim(to_char(ln_auxiliar))),1,1))+
                           
to_number(substr(rtrim(ltrim(to_char(ln_auxiliar))),2,1)));
         Else
           ln_resultado := ln_resultado + (ln_digito * 2);
         End If;
       Else
         ln_resultado := ln_resultado + (ln_digito * 1);
       End If;
       ln_contador := ln_contador - 1;
     End Loop;
     Loop
       Exit When Mod(ln_resultado + ln_contador, 10) = 0;
       ln_contador := ln_contador + 1;
     End Loop;
     Return to_char(ln_contador);
   End fisf_obtiene_dig_verificador;

	 * */
	
public static String obtieneDigitoVerificador(String codigo)
{
	String proceso="Obtiene Digito Verificador";
	String result=null;
try {
	long resultado=0;
	int contador = codigo.length();
	long digito=0;
	long auxiliar=0;
	while(true)
	{
	if (contador < 1) break;	
		digito = Long.parseLong(codigo.substring(contador-1,contador));
		if (contador%2 == 0)
		{
			auxiliar = digito*2;
			if (auxiliar > 9)
			{
//				String auxAux= Long.toString(auxiliar);
//				auxAux = auxAux.trim();
//				auxAux = auxAux.substring(0,1);
				resultado=resultado +(Long.parseLong(Long.toString(auxiliar).trim().substring(0,1)))+(Long.parseLong(Long.toString(auxiliar).trim().substring(1,2)));
				//resultado=resultado +(Long.parseLong(auxAux))+(Long.parseLong(Long.toString(auxiliar).trim().substring(1,2)));
			}
			else
			{
				resultado=resultado+(digito*2);
			}
		}
		else
		{
			resultado=resultado+(digito*1);
		}
		contador=contador-1;
	}
	while (true)
	{
		if ((resultado+contador)%10 == 0) break;
		contador=contador+1;
	}
	
	result = Long.toString(contador);
	//return result;

} catch (Exception e) {
	log.error("Proceso: "+proceso+": "+e.toString(),e);
	e.printStackTrace();
	log.error(e.toString(),e);
	return null;
}	
return result;
}



public static void main (String [] args)
{
	//String nombre="Imei";
	String nombre="Iccid";
	log.info("Validando digito verificador "+nombre);
	String resultado = obtieneDigitoVerificador("01240100529009");
	log.info(nombre+" Resultado Digito verificador :"+resultado);
}
}
