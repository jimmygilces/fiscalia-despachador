package com.rgt.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Properties;

import org.apache.logging.log4j.Logger;

public class ConsumeServlet {
	
	static Logger log = Propiedades.getMyLogger();
	
	public static String consumePost(String urlP, String parametros) throws Exception{
		int cTimeOut=0;
		int rTimeOut=0;
		String respuest="";

			Properties props= new Propiedades().getPropiedades();
			URL  url = new URL(urlP);/*"http://192.168.37.130:7777/WebServiceSprTransaccionesDMZ/SprSoapHttpPort"*/
			//URL("http://192.168.37.130:7777/eis/eisSoapHttpPort");
			cTimeOut=Integer.parseInt(props.getProperty("cTimeOut"));
			rTimeOut=Integer.parseInt(props.getProperty("rTimeOut"));
			HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
			conexion.setDoOutput(true);
			conexion.setRequestMethod("POST");
			conexion.setRequestProperty("Content-Type", "text/xml");
			conexion.setRequestProperty("charset", "ISO-8859-1");
			conexion.setConnectTimeout(cTimeOut);
			conexion.setReadTimeout(rTimeOut);
			log.info("Escribir");
			String request = parametros;
			OutputStreamWriter writer = new OutputStreamWriter(conexion.getOutputStream());
			writer.write(request);
			writer.close();
			InputStreamReader reader = new InputStreamReader(conexion.getInputStream());
			BufferedReader br = new BufferedReader(reader);
			String tmp = null;
			while ((tmp=br.readLine())!= null){
				respuest+=URLDecoder.decode(tmp, "ISO-8859-1");
			}
			reader.close();
			br.close();
			log.info("Respuesta");
			log.info(respuest);
			
			log.info(conexion.getResponseCode());
			log.info(conexion.getResponseMessage());

		return respuest;

	}
}
