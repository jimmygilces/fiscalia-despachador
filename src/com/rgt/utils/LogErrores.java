package com.rgt.utils;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import com.rgt.conexion.ConexionO;
import com.rgt.procesos.enviaMail;

import org.apache.logging.log4j.Logger;


public class LogErrores {
	
	static Logger log = Propiedades.getMyLogger();

    public static  void escribirLog(String mensaje)
    {
    	String proceso="Escribir Log";
    	Properties props= new Propiedades().getPropiedades();
        String rutaLog = props.getProperty("Log");

        Date fechaActual = new Date();


        SimpleDateFormat sdf1 = new SimpleDateFormat("MM");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy");

        String mes = sdf1.format(fechaActual);
        String year = sdf2.format(fechaActual);

        String nombreTxt = "Log"+mes+year+".log";
        //String nombreTxt = "Log.log";
        //ConexionOper cnx = new ConexionOper();

        try
        {
            File fl = new File(rutaLog);
            fl.mkdir();
            File file = new File(rutaLog+nombreTxt);
            if (!file.exists()) {
            
                if (file.createNewFile()) {
                	log.info("Log creado en: "+rutaLog+nombreTxt);
                } else {

                	log.info("No ha podido ser creado el fichero");
                }
            }
        /*la clave de activar o no la sobreescritura esta en FileOutputStream(file, true) si le ponemos en true entonces agregas al final de la linea */

        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true), "UTF8"));

        out.write(mensaje);
        out.newLine();
        out.close();
        }catch(Exception ex)
        {
        	log.error("Proceso: "+proceso+": "+ex.toString(),ex);
        	ex.printStackTrace();
        	log.error(ex.toString(),ex);
        	}

    }
    
    
    public static void RegistrarError(String idSolicitud,String oficioSol, String estadoGenError, String proceso, String detalleError)
    {
    	ConexionO cnx = new ConexionO();
        Connection con = null;
        String procesos="Registrar Error";
        try
        {
        //con = ConexionO.conectarDB();
        con = cnx.conectar().getConexion();	
        String ipOrigen = InetAddress.getLocalHost().getHostAddress();
        String codError="";
        String msjError="";
        String usuarioSolicita = enviaMail.ObtenerUserIngreso(idSolicitud);//Usuario que ingresa la solicitud
        String usuarioEjecuta = System.getProperty("user.name");//Usuario que ejecuta el proceso
        String oficio=oficioSol;
        
        CallableStatement cst = con.prepareCall("{CALL FISK_API_PORTAL_TRANSACCIONES.FISP_GRABA_BITACORA_ERRORES(?,?,?,?,?,?,?,?,?,?)}");
        cst.setLong(1, Long.parseLong(idSolicitud));
        cst.setString(2, usuarioEjecuta);
        cst.setString(3, ipOrigen);
        cst.setString(4, estadoGenError);
        cst.setString(5, proceso);
        cst.setString(6, detalleError);
        cst.setString(7, usuarioSolicita);
        cst.setString(8, oficio);
        cst.registerOutParameter(9, java.sql.Types.VARCHAR);
        cst.registerOutParameter(10, java.sql.Types.VARCHAR);
        cst.execute();

        codError= cst.getString(9);
        msjError= cst.getString(10);
        cst.close();
        con.close();
        if(codError.equals("1"))
        {
            throw new Exception("Error, Solicitud: "+idSolicitud+", "+msjError);
        }
        }catch(Exception ex)
        {
        	log.error("No se pudo registrar el error en la BD,solicitud,solicitud: "+idSolicitud+" Proceso: "+procesos+": "+ex.getMessage());
        	ex.printStackTrace();
        	log.error(ex.toString(),ex);
        }
        finally{
        	//29/10/2015 tratar de cerrar conexiones
        	try {
        		if (cnx.getConexion()!=null && !cnx.getConexion().isClosed())
        			cnx.getConexion().close();
        		
			} catch (Exception e2) {
				log.fatal("Cerrar Conexion: "+ e2.toString(),e2);
			}
        }
    }
    
    public static void registrarIntentosErrorSolicitud(String idSolicitud) throws Exception
    {
    	String queryUpdate = "update fis_solicitudes set intentos_error = intentos_error+1 where id_solicitud="+idSolicitud;
    	ConexionO cnx = new ConexionO();
        Connection con = null;
        con = cnx.conectar().getConexion();
        Statement st = con.createStatement();
        st.executeUpdate(queryUpdate);
        st.close();
        con.close();        
    }

}
