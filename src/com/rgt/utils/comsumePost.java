package com.rgt.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Properties;

import org.apache.logging.log4j.Logger;

public class comsumePost {
	
	static Logger log = Propiedades.getMyLogger();
	
	public String Request(String request,String urlServlet)
	{
		String proceso="Request de Consume Post";
	 String response="";	
	 try{
		 	Properties props= new Propiedades().getPropiedades();
			URL  url = new URL(urlServlet);/*"http://192.168.37.130:7777/WebServiceSprTransaccionesDMZ/SprSoapHttpPort"*/ 
			//URL("http://192.168.37.130:7777/eis/eisSoapHttpPort");
			int cTimeOut=Integer.parseInt(props.getProperty("cnxTimeOutLBS"));
			int rTimeOut=Integer.parseInt(props.getProperty("readTimeOutLBS"));
			HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
			conexion.setDoOutput(true);
			conexion.setRequestMethod("POST");
			conexion.setRequestProperty("Content-Type", "text/xml");
			conexion.setRequestProperty("charset", "ISO-8859-1");
			conexion.setConnectTimeout(cTimeOut);
			conexion.setReadTimeout(rTimeOut);
			//conexion.setRequestProperty("Accept", "application/xml");
			//conexion.setRequestProperty("SOAPAction", "http://axis/EISApiOnlineWS.wsdl/types//eipConsumeServicio");
			log.info("Escribir");
		
			
//			String request="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:clar=\"http://claro/\">"+
//					"<soapenv:Header/>"+
//					"<soapenv:Body>"+
//					"<clar:receptaTransaccionElement>"+
//					"<clar:Transaccion>"+transaccion+"</clar:Transaccion>"+
//					"<clar:CadenaParametros>"+cadenaParametros+"</clar:CadenaParametros>"+
//					"<clar:Procesamiento>"+procesamiento+"</clar:Procesamiento>"+
//					"<clar:Debug>"+debug+"</clar:Debug>"+
//					"<clar:id_transaccion>"+id_transaccion+"</clar:id_transaccion>"+
//					"<clar:banderaRepeticion>"+banderaRepeticion+"</clar:banderaRepeticion>"+
//					"<clar:contadorReintentoB>"+contadorReintentoB+"</clar:contadorReintentoB>"+
//					"</clar:receptaTransaccionElement>"+
//					"</soapenv:Body>"+
//					"</soapenv:Envelope>";
			
			
			OutputStreamWriter writer = new OutputStreamWriter(conexion.getOutputStream());
			writer.write(request);
			writer.close();
			InputStreamReader reader = new InputStreamReader(conexion.getInputStream());
			BufferedReader br = new BufferedReader(reader);
			String tmp = null;
			//logger.info("SPR_REQUEST: "+request);
			while ((tmp=br.readLine())!= null){
				response+=URLDecoder.decode(tmp, "ISO-8859-1");
			}
			reader.close();
			br.close();
			log.info("Respuesta");
			log.info(response);
			
			log.info(conexion.getResponseCode());
			log.info(conexion.getResponseMessage());

		}catch(Exception e){
			log.error("Proceso: "+proceso+" ERROR CONSUMIR SPR DEL LBS: "+e.getMessage());
			e.printStackTrace();
			log.error(e.toString(),e);
			}
		
		return response;
	}

}
