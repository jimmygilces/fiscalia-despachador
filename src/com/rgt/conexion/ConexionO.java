package com.rgt.conexion;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
//import java.util.logging.Level;
//import java.util.logging.Logger;

import org.apache.logging.log4j.Logger;




import com.rgt.utils.Propiedades;
import com.rgt.utils.claves;

public class ConexionO { 
private Connection conexion; 
private static Connection conexionDB;
static Logger log = Propiedades.getMyLogger();
     
public Connection getConexion() { 
    return conexion; 
}    
public void setConexion(Connection conexion) { 
    this.conexion = conexion; 
}  





public ConexionO conectar() throws Exception { 
	String proceso="Conectar";
	int intentoConexion =0;
    boolean flag=true;
    
    while (flag)
    {
    	try {
    	 intentoConexion+=1; 
       	 Properties props= new Propiedades().getPropiedades();
            String driverConexion= props.getProperty("driverConexion");
            String usuarioConexion=props.getProperty("usuario");
            String contrasenaConexion=props.getProperty("contrasena");
            //String contrasenaConexion=claves.contrasenaConexionOper;
            String cadenaConexion=props.getProperty("cadenaConexion");
           Class.forName(driverConexion); 

             
           conexion = DriverManager.getConnection(cadenaConexion, usuarioConexion,contrasenaConexion);             
           flag = false;
       } catch (Exception e) {
    	   if (intentoConexion==3)
    	   {
    		   flag = false;
    		   log.error("Proceso: "+proceso+": Conexion DB local fallida: "+e.toString(),e);
    		   //e.printStackTrace();
    		   //log.error(e.toString(),e);
    		   throw new Exception ("Conexion DB local fallida!: "+e.toString());
    	   }
           
       }
    }
            return this; 
} 

public  boolean escribir(String sql) throws Exception { 
        try { 
            Statement sentencia; 
            //conectarDB();
            sentencia = getConexion().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            //sentencia = conexionDB.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            sentencia.executeUpdate(sql); 
            //getConexion().commit(); 
            sentencia.close(); 
             
        } catch (SQLException e) { 
            e.printStackTrace(); 
            log.error("ERROR SQL: "+e.getMessage()); 
            log.error(e.toString(),e);
            throw new SQLException(e.getMessage());
        }      
        return true; 
    } 

public  ResultSet consultar(String sql) throws Exception { 
        ResultSet resultado = null; 
        try { 
            Statement sentencia;
            //conectarDB();
            sentencia = getConexion().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            //sentencia = conexionDB.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY); 
            resultado = sentencia.executeQuery(sql); 
            
        } catch (SQLException e) { 
        	log.error(e.toString(),e);
            throw new SQLException(e.getMessage()); 
        }        return resultado; 
    }


public static void closeConexionDB()
{
    //System.out.println("conexion actual: "+conexionOpr.isClosed());
	log.info("Cerrando conexi�n...");
        try {
            if (conexionDB==null ||conexionDB.isClosed())
    {
        //System.out.println("Conexion Cerrada?:"+ conexionOpr.isClosed());
        return;
    }
            conexionDB.close();
            conexionDB=null;
        } catch (SQLException ex) {
            //ex.printStackTrace();
            //Logger.getLogger(ConexionO.class.getName()).log(Level.SEVERE, null, ex);
            log.error(ConexionO.class.getName()+" "+ ex);
            log.error(ex.toString(),ex);
        }
}

}