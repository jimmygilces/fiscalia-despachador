package com.rgt.conexion;

import java.sql.*; 
import java.util.Properties;

import com.rgt.utils.Propiedades;
import com.rgt.utils.claves;

import org.apache.logging.log4j.Logger;

public class ConexionN { 	
private Connection conexion;
static Logger log = Propiedades.getMyLogger();
     
public Connection getConexion() { 
    return conexion; 
}    
public void setConexion(Connection conexion) { 
    this.conexion = conexion; 
}  

public ConexionN conectar() throws Exception {
	String proceso="Conectar";
	int intentoConexion =0;
    boolean flag=true;
    
    while (flag)
    {   	
    	try { 
    		intentoConexion+=1; 
    		Properties props= new Propiedades().getPropiedades();
    		
    		//Nuevo parametro por Contingencia de Netezza - SUD JGilces - 19/10/2023
    		log.info("Carga de Parametros por contingencia de Netezza");
    		final String bandContingenciaNetezza = props.getProperty("ContingenciaNTZ");
    		
    		String driverConexion= props.getProperty("nDriverConexion");
    		String user=props.getProperty((bandContingenciaNetezza.equals("S"))?"user_c":"user");
    		String pwd=props.getProperty((bandContingenciaNetezza.equals("S"))?"pwd_c":"pwd");
    		String url=props.getProperty((bandContingenciaNetezza.equals("S"))?"url_c":"url");
    		
            Class.forName(driverConexion);         
            conexion = DriverManager.getConnection(url, user, pwd);
            flag=false;
        } catch (Exception e) {
        	if (intentoConexion==2)
     	   {
     		   flag = false;	   
     		  log.error("Proceso: "+proceso+": Conexion fallida: "+e.toString(),e);
     		  throw new Exception ("Conexion Netezza fallida!: "+e.toString());
     	   }
        }
    }
            return this; 
} 

public boolean escribir(String sql) throws SQLException { 
	String proceso="Escribir";
        try { 
            Statement sentencia; 
            sentencia = getConexion().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY); 
            sentencia.executeUpdate(sql); 
            //getConexion().commit(); 
            sentencia.close(); 
             
        } catch (SQLException e) { 
        	log.error("Proceso: "+proceso+": "+e.toString(),e);
        	//e.printStackTrace();
        	//log.error(e.toString(),e);
            throw new SQLException(e.getMessage());
        }         
        return true; 
    } 

public ResultSet consultar(String sql) throws SQLException { 
	String proceso="Consultar";
	
        ResultSet resultado = null; 
        try { 
            Statement sentencia; 
            sentencia = getConexion().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY); 
            resultado = sentencia.executeQuery(sql); 

        } catch (SQLException e) { 
        	log.error("Proceso: "+proceso+": "+e.toString(),e);
        	e.printStackTrace();
        	log.error(e.toString(),e);
            throw new SQLException(e.getMessage());
        }        return resultado; 
    }
} 

