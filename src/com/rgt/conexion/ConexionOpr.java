package com.rgt.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import com.rgt.utils.Propiedades;
import org.apache.logging.log4j.Logger;

public class ConexionOpr {
	static Logger log = Propiedades.getMyLogger();
	
	private Connection conexion; 
	     
	public Connection getConexion() { 
	    return conexion; 
	}    
	public void setConexion(Connection conexion) { 
	    this.conexion = conexion; 
	}  

	public ConexionOpr conectar() throws Exception { 
		String proceso="Conexion Opr";
		int intentoConexion =0;
	    boolean flag=true;
	    
	    while (flag)
	    {
	    	try {
	    	 intentoConexion+=1; 
	       	 Properties props= new Propiedades().getPropiedades();
	            String driverConexion= props.getProperty("driverConexionOpr");
	            String usuarioConexion=props.getProperty("usuarioOpr");
	            String contrasenaConexion=props.getProperty("contrasenaOpr");
	            //String contrasenaConexion=claves.contrasenaConexionOper;
	            String cadenaConexion=props.getProperty("cadenaConexionOpr");
	           Class.forName(driverConexion); 

	             
	           conexion = DriverManager.getConnection(cadenaConexion, usuarioConexion,contrasenaConexion);             
	           flag = false;
	       } catch (Exception e) {
	    	   if (intentoConexion==3)
	    	   {
	    		   flag = false;
	    		   log.error("Proceso: "+proceso+": Conexion Operaciones fallida: "+e.toString(),e);
	    		   //e.printStackTrace();
	    		   //log.error(e.toString(),e);
	    		   throw new Exception ("Conexion Operaciones fallida!: "+e.toString());
	    	   }
	           
	       }
	    }
	            return this; 
	} 
}
