package com.rgt.conexion;

import java.sql.*; 
import java.util.Properties;

import com.rgt.utils.Propiedades;
import com.rgt.utils.claves;

import org.apache.logging.log4j.Logger;

public class ConexionE { 
private Connection conexion;
public static String usuarioConexion;
public static String contrasenaConexion;

static Logger log = Propiedades.getMyLogger();

     
public Connection getConexion() { 
    return conexion; 
}    
public void setConexion(Connection conexion) { 
    this.conexion = conexion; 
}  



public ConexionE conectar() throws Exception { 
	String proceso="Conectar";
    	try { 
       	 Properties props= new Propiedades().getPropiedades();
       	String usuarioConexionEsp = props.getProperty("usuarioEsp");
            String driverConexion= props.getProperty("driverConexion");
            String cadenaConexion=props.getProperty("cadenaConexionEsp");
            String clave = props.getProperty("contrasenaEsp");
           Class.forName(driverConexion);           
           conexion = DriverManager.getConnection(cadenaConexion, usuarioConexionEsp,clave);             
       } catch (Exception e) {	  
    	   	log.error("Proceso: "+proceso+": Conexion fallida: "+e.toString(),e);
    	   	e.printStackTrace();
    	   	log.error(e.toString(),e);
    		   throw new Exception ("Conexion Espejo fallida!: "+e.getMessage()); 
       }
    
            return this; 
} 

public boolean escribir(String sql) throws SQLException { 
	String proceso="Escribir";
        try { 
            Statement sentencia; 
            sentencia = getConexion().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY); 
            sentencia.executeUpdate(sql); 
            //getConexion().commit(); 
            sentencia.close(); 
             
        } catch (SQLException e) { 
        	log.error("Proceso: "+proceso+": "+e.toString(),e);
        	e.printStackTrace();
        	log.error(e.toString(),e);
            throw new SQLException(e.getMessage());
        }         
        return true; 
    } 

public ResultSet consultar(String sql) throws SQLException { 
	String proceso="Consultar";
        ResultSet resultado = null; 
        try { 
            Statement sentencia; 
            sentencia = getConexion().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY); 
            resultado = sentencia.executeQuery(sql); 

        } catch (SQLException e) { 
        	log.error("Proceso: "+proceso+": "+e.toString(),e);
        	e.printStackTrace();
        	log.error(e.toString(),e);
            throw new SQLException(e.getMessage());
        }        return resultado; 
    } 

} 

