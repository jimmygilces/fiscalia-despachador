package com.rgt.procesos;

import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.Connection;
import java.util.Properties;
import java.sql.PreparedStatement;
import java.sql.CallableStatement;
import java.io.File;
import com.rgt.utils.LogErrores;
import java.util.Date;
import java.util.Calendar;
import java.util.Locale;
import java.text.SimpleDateFormat;
import com.rgt.utils.otrasValidaciones;
import com.rgt.conexion.ConexionD;
import com.rgt.conexion.ConexionO;
import com.rgt.conexion.ConexionN;
import com.rgt.utils.Propiedades;
import org.apache.logging.log4j.Logger;

public class Solicitud extends Thread {
	String idSolicitud;
	String oficio;
	String servicio;
	String tipoReporte;
	String fecha_inicio;
	String fecha_fin;
	String anio;
	String estado;
	String tipoProceso;
	String descServicio;
	String hilo;
	String usuarioIngreso;
	String IdServicioHW;
	static Logger log;
	private long initialTime;

	static {
		Solicitud.log = Propiedades.getMyLogger();
	}

	public Solicitud(final String idSolicitud, final String oficio, final String servicio, final String tipoReporte,
			final String fecha_inicio, final String fecha_fin, final String anio, final String estado,
			final String tipoProceso, final String descServicio) {
		this.idSolicitud = idSolicitud;
		this.oficio = oficio;
		this.servicio = servicio;
		this.tipoReporte = tipoReporte;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.anio = anio;
		this.estado = estado;
		this.tipoProceso = tipoProceso;
		this.descServicio = descServicio;
		this.IdServicioHW = servicio;
	}

	public Solicitud(final String idSolicitud, final String oficio, final String servicio, final String tipoReporte,
			final String fecha_inicio, final String fecha_fin, final String anio, final String estado,
			final String tipoProceso, final String descServicio, final String hilo, final String usuarioIngreso) {
		this.idSolicitud = idSolicitud;
		this.oficio = oficio;
		this.servicio = servicio;
		this.tipoReporte = tipoReporte;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.anio = anio;
		this.estado = estado;
		this.tipoProceso = tipoProceso;
		this.descServicio = descServicio;
		this.hilo = hilo;
		this.usuarioIngreso = usuarioIngreso;
		this.IdServicioHW = servicio;
	}

	public Solicitud(final String idSolicitud, final String oficio, final String servicio, final String tipoReporte,
			final String fecha_inicio, final String fecha_fin, final String anio, final String estado,
			final String tipoProceso, final String descServicio, final String hilo, final String usuarioIngreso,
			final long initialTime) {
		this.idSolicitud = idSolicitud;
		this.oficio = oficio;
		this.servicio = servicio;
		this.tipoReporte = tipoReporte;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.anio = anio;
		this.estado = estado;
		this.tipoProceso = tipoProceso;
		this.descServicio = descServicio;
		this.hilo = hilo;
		this.usuarioIngreso = usuarioIngreso;
		this.initialTime = initialTime;
		this.IdServicioHW = servicio;
	}

	public Solicitud() {
	}

	public String getUsuarioIngreso() {
		return this.usuarioIngreso;
	}

	public void setUsuarioIngreso(final String usuarioIngreso) {
		this.usuarioIngreso = usuarioIngreso;
	}

	public String getIdSolicitud() {
		return this.idSolicitud;
	}

	public void setIdSolicitud(final String idSolicitud) {
		this.idSolicitud = idSolicitud;
	}

	public String getOficio() {
		return this.oficio;
	}

	public void setOficio(final String oficio) {
		this.oficio = oficio;
	}

	public String getServicio() {
		return this.servicio;
	}

	public void setServicio(final String servicio) {
		this.servicio = servicio;
	}

	public String getTipoReporte() {
		return this.tipoReporte;
	}

	public void setTipoReporte(final String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}

	public String getFecha_inicio() {
		return this.fecha_inicio;
	}

	public void setFecha_inicio(final String fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public String getFecha_fin() {
		return this.fecha_fin;
	}

	public void setFecha_fin(final String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	public String getAnio() {
		return this.anio;
	}

	public void setAnio(final String anio) {
		this.anio = anio;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(final String estado) {
		this.estado = estado;
	}

	public String getTipoProceso() {
		return this.tipoProceso;
	}

	public void setTipoProceso(final String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	public String getDescServicio() {
		return this.descServicio;
	}

	public void setDescServicio(final String descServicio) {
		this.descServicio = descServicio;
	}

	public String getHilo() {
		return this.hilo;
	}

	public void setHilo(final String hilo) {
		this.hilo = hilo;
	}

	public long getInitialTime() {
		return this.initialTime;
	}

	public void setInitialTime(final long initialTime) {
		this.initialTime = initialTime;
	}

	@Override
	public void run() {
		
		System.setProperty("java.awt.headless", "true");
		
		final Properties props = new Propiedades().getPropiedades();
		// Nuevo parametro por Contingencia de Netezza - SUD JGilces - 19/10/2023
		final String bandContingenciaNetezza = props.getProperty("ContingenciaNTZ");

		if (bandContingenciaNetezza.equals("S")) {
			Solicitud.log.info("Bandera activa por Contingencia de Netezza");
		}

		String result = "0";
		int contador = 0;
		final ConexionN conNetezza = new ConexionN();
		final ConexionO conoper = new ConexionO();
		final ConexionD conDWH = new ConexionD();
		CallableStatement lcs_cs = null;
		PreparedStatement lcs_cs_oper = null;
		PreparedStatement lcs_cs_operDWH = null;
		Solicitud.log.info((Object) ("CRG-FECHA-INICIO: " + this.fecha_inicio + " CRG-FECHA-FIN: " + this.fecha_fin));
		String sqlInsert = "INSERT INTO COLECTOR." + (bandContingenciaNetezza.equals("S") ? "COLECTOR." : "")
				+ "RP_DATOS_SOLICITUD(ID_SOLICITUD,OFICIO,SERVICIO,TIPO_REPORTE,FECHA_INI,FECHA_FIN,ANIO,ESTADO,TIPO_PROCESO) VALUES("
				+ this.idSolicitud + ",'" + this.oficio + "','" + this.servicio + "','" + this.tipoReporte + "','"
				+ this.fecha_inicio + "','" + this.fecha_fin + "','" + this.anio + "','" + this.estado + "','"
				+ this.tipoProceso + "')";
		final String paquete = "{? = call COLECTOR." + (bandContingenciaNetezza.equals("S") ? "COLECTOR." : "")
				+ "SP_MED_CAUT_TRAFICO(" + this.idSolicitud + ")}";
		final String FIS_PCK = "{ call FISK_API_PORTAL_SEGURIDAD.FISP_INGRESO_DATOS_SOLICITUDES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		final String paqueteHW = "{? = call COLECTOR." + (bandContingenciaNetezza.equals("S") ? "COLECTOR." : "")
				+ "SP_MED_CAUT_TRAFICO_HW(" + this.idSolicitud + ")}";
		final String paqueteHW_RNA = "{? = call COLECTOR." + (bandContingenciaNetezza.equals("S") ? "COLECTOR." : "")
				+ "SP_MED_CAUT_TRAFICO_HW_RNA(" + this.idSolicitud + ")}";
		final String insertDatos = "INSERT INTO FIS_DATOS_SOLICITUD (ID_DATO_SOLICITUD,ID_SOLICITUD,CAMPO_1,CAMPO_2,CAMPO_3,CAMPO_4,CAMPO_5,CAMPO_6,CAMPO_7,CAMPO_8,CAMPO_9,CAMPO_10,CAMPO_11,CAMPO_12,CAMPO_13,CAMPO_14,CAMPO_15,CAMPO_16,CAMPO_17,CAMPO_18,CAMPO_19,CAMPO_20,CAMPO_21,CAMPO_22,CAMPO_23,CAMPO_24,CAMPO_25,CAMPO_26,CAMPO_27,CAMPO_28,CAMPO_29,CAMPO_30) VALUES (SEQ_FIS_DATOS_SOLICITUD.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		final String tabla3 = "COLECTOR." + (bandContingenciaNetezza.equals("S") ? "COLECTOR." : "") + "\"DT_CDR_"
				+ this.oficio + '_' + this.idSolicitud + "\"";// REVISION_JULIAN
		final String tabla4 = "COLECTOR." + (bandContingenciaNetezza.equals("S") ? "COLECTOR." : "") + "\"CDR_SMS_"
				+ this.oficio + '_' + this.idSolicitud + "\"";
		final String tabla5 = "COLECTOR." + (bandContingenciaNetezza.equals("S") ? "COLECTOR." : "")
				+ "\"DT_CDR_RADIOBASE_" + this.oficio + '_' + this.idSolicitud + "\"";
		final String sqlCDRTmp = "select * from " + tabla3;// REVISION_JULIAN
		final String sqlSMSTmp = "select * from " + tabla4;
		final String sqlRADIOBASETmp = "select * from " + tabla5;
		final String nEstado = "select estado from COLECTOR." + (bandContingenciaNetezza.equals("S") ? "COLECTOR." : "")
				+ "RP_DATOS_SOLICITUD where id_solicitud=" + this.idSolicitud;
		final String sqlDWH = " SELECT ID_SOLICITUD, NOMBRE_COMPLETO, NO_IMEI, NO_IMSI, NO_ICCID, IDENTIFICACION, DIRECCION_REFERENCIA, TELEFONO_REFERENCIA, NUMERO_CELULAR, FECHA_INICIO, FECHA_FIN, TIPO_ABONADO, CIUDAD  FROM FIS_REP_ABONADO  WHERE ID_SOLICITUD ="
				+ this.idSolicitud + " AND ORIGEN = 'F' "
				+ " AND TO_DATE(FECHA_FIN, 'DD/MM/YYYY HH24:MI:SS') <= SYSDATE " + " AND FECHA_FIN IS NOT NULL\t"
				+ " ORDER BY FECHA_INICIO ";
		final String insertDatosDWH = "INSERT INTO FIS_REP_ABONADO_DWH (ID_SOLICITUD, NOMBRE_COMPLETO, NO_IMEI, NO_IMSI, NO_ICCID, IDENTIFICACION, DIRECCION_REFERENCIA, TELEFONO_REFERENCIA, NUMERO_CELULAR, FECHA_INICIO, FECHA_FIN, TIPO_ABONADO, CIUDAD )  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ";
		String estadoGenError = this.estado;
		final String estadosPermitidos = "ABIQRUWX";
		String proceso = "Hilo Procesar solicitudes";
		boolean flagProceso = true;
		int intentosProceso = 0;
		int maxIntentosProceso = 0;
		boolean exist = false;
		String bandSMSNetezzaHW = "";
		int eisIdServInf = 0;
		int nCommit = 0;
		String rangoFechas = "";
		String descAdicional = "";
		String descAdicionalHW = "";
		String rangoFechasHW = "";
		final String tiposReporte345 = "345";

		Solicitud.log.info("::: [SUD-Temporal] - Consultas para netezza :::");
		Solicitud.log.info("::: [SUD-Temporal] - " + sqlInsert);
		Solicitud.log.info("::: [SUD-Temporal] - " + paqueteHW);
		Solicitud.log.info("::: [SUD-Temporal] - " + paqueteHW_RNA);
		Solicitud.log.info("::: [SUD-Temporal] - " + sqlCDRTmp);

		try {
			Solicitud.log
					.info((Object) ("solicitud: " + this.idSolicitud + " Procesando solicitud " + this.idSolicitud));
			if (this.tipoProceso.equals("H")) {
				enviaMail.enviar(this);
			}

			maxIntentosProceso = Integer.parseInt(props.getProperty("maxIntentosProceso"));
			final String dirPrincipal = props.getProperty("dirPrincipal");
			final String javaPath = props.getProperty("javaPath");
			final String javaPath2 = props.getProperty("javaPath8");
			final String procesoEjecuta = String.valueOf(String.valueOf(String.valueOf(javaPath2))) + "java -jar "
					+ dirPrincipal + "ArchivosTransferencia.jar " + this.idSolicitud;
			Solicitud.log.info((Object) ("CRG-ESTADO: " + this.estado));
			if ("ABIQRUWX".indexOf(this.estado) < 0) {
				ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
				this.liberaPila();
				return;
			}
			if (!this.estado.equalsIgnoreCase("Q")) {
				cambiarEstado("Q", this.idSolicitud);
				estadoGenError = "Q";
			}
			while (flagProceso) {
				++intentosProceso;
				try {
					Connection connoper = conoper.conectar().getConexion();
					if (!this.estado.equalsIgnoreCase("X")) {
						final String queryDatosSolicitud = "select id_dato_solicitud from fis_datos_solicitud where id_solicitud="
								+ this.idSolicitud + " and rownum=1";
						final ResultSet rso = conoper.consultar(queryDatosSolicitud);
						if (rso.next()) {
							Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
									+ " Contiene registros incompletos en la BD"));
							Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
									+ " Eliminando registros incompletos de la BD..."));
							if (!conoper.escribir(
									"delete from fis_datos_solicitud where id_solicitud=" + this.idSolicitud)) {
								throw new Exception(
										"Error en BD local,No se pudo eliminar los datos existentes para lo solicitud: "
												+ this.idSolicitud);
							}
							Solicitud.log.info(
									(Object) ("solicitud: " + this.idSolicitud + " Registros incompletos Eliminados!"));
						}
						rso.close();
					}
					connoper.close();
					Label_13373: {
						if (this.tipoReporte.equals("3") || this.tipoReporte.equals("4")
								|| this.tipoReporte.equals("5")) {
							final int vlongitud = this.IdServicioHW.length();
							try {
								if (!this.tipoReporte.equals("5")) {
									proceso = "Verificaci\u00f3n numero VoiceMail";
									Solicitud.log
											.info((Object) ("solicitud: " + this.idSolicitud + " verificando numero "
													+ this.servicio + " en lista de numeros VoiceMail..!"));
									if (otrasValidaciones.numVoiceMail(this.servicio)) {
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
												+ " El n\u00famero consultado " + this.servicio
												+ " corresponde a un numero de Casillero de Voz"));
										ConsumoWs.grabarMensajeSinDatos(this.idSolicitud,
												"El n\u00famero consultado corresponde a un n\u00famero de Casillero de Voz",
												(String) null);
										cambiarEstado("P", this.idSolicitud);
										estadoGenError = "P";
										ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
										this.liberaPila();
										return;
									}
									proceso = "Verificaci\u00f3n estado de linea en Axis";
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " verificando estado de numero " + this.servicio
											+ " en axis para las fechas: " + this.fecha_inicio + " ; "
											+ this.fecha_fin));
									final int lenght = this.servicio.length();
									final String parametros = "LINEA||FECHA_INICIAL||FECHA_FINAL";
									final String tmpFechaIni = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(
											new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this.fecha_inicio));
									final String tmpFechaFin = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
											.format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this.fecha_fin));
									final String valorParametros = String.valueOf(
											String.valueOf(String.valueOf(this.servicio.substring(lenght - 8)))) + "||"
											+ tmpFechaIni + "||" + tmpFechaFin;
									final String tagsParametros = ConsumoWs
											.setearTags("LINEA||FECHA_INICIAL||FECHA_FINAL", valorParametros);
									final String[] respSRE = ConsumoWs.consumeSreAxis(tagsParametros,
											props.getProperty("sreTipoTransaccionEstadoLinea"));
									if (estadoGenError != "R") {
										cambiarEstado("R", this.idSolicitud);
										estadoGenError = "R";
									}
									InsertaValidaServicioHw(this.IdServicioHW.substring(vlongitud - 9),
											this.idSolicitud, this.fecha_inicio, this.fecha_fin, this.estado);
									final String parametrosHW = "SOLICITUD||LINEA||FECHA_INICIAL||FECHA_FINAL";
									final String valorParametrosHW = String
											.valueOf(String.valueOf(String.valueOf(this.idSolicitud))) + "||"
											+ this.servicio.substring(lenght - 9) + "||" + tmpFechaIni + "||"
											+ tmpFechaFin;
									Solicitud.log.info((Object) ("valorParametrosHW...:" + valorParametrosHW));
									final String tagsParametrosHW = ConsumoWs.setearTags(
											"SOLICITUD||LINEA||FECHA_INICIAL||FECHA_FINAL", valorParametrosHW);
									Solicitud.log.info((Object) ("tagsParametrosHW...:" + tagsParametrosHW));
									final String[] respSREHW = ConsumoWs.consumeSreHuawei(tagsParametrosHW,
											props.getProperty("sreTipoTransaccionEstadoLineaHW"));
									if (!respSRE[0].equals("0")) {
										Solicitud.log.warn((Object) ("solicitud: " + this.idSolicitud
												+ " error al verificar estado de numero " + this.servicio));
									} else {
										if (!respSREHW[0].equals("0")) {
											Solicitud.log.warn((Object) ("solicitud: " + this.idSolicitud
													+ " error al verificar estado de numero " + this.servicio));
										} else {
											descAdicionalHW = respSREHW[2];
											rangoFechasHW = respSREHW[3];
										}
										descAdicional = respSRE[2];
										rangoFechas = respSRE[3];
										if ((rangoFechas == null || rangoFechas.isEmpty()) && rangoFechasHW != null
												&& !rangoFechasHW.isEmpty()) {
											descAdicional = descAdicionalHW;
											rangoFechas = rangoFechasHW;
										} else if (rangoFechas != null && !rangoFechas.isEmpty()
												&& rangoFechasHW != null && !rangoFechasHW.isEmpty()) {
											descAdicional = descAdicionalHW;
											rangoFechas = rangoFechasHW;
										}
										insertarDescAdicional(
												"{ call FISK_API_PORTAL_SEGURIDAD.FISP_INGRESO_DATOS_SOLICITUDES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}",
												this.idSolicitud, descAdicional, " ");
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
												+ " SRE:descAdicional: " + descAdicional));
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
												+ " SRE:rangoFechas: " + rangoFechas));
										if (rangoFechas == null || rangoFechas.isEmpty()) {
											InsertarNull(
													"{ call FISK_API_PORTAL_SEGURIDAD.FISP_INGRESO_DATOS_SOLICITUDES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}",
													this.idSolicitud);
											if (this.tipoReporte.equalsIgnoreCase("3")) {
												cambiarEstado("X", this.idSolicitud);
												estadoGenError = "X";
												Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
														+ " actualizando informacion de celdas null.."));
												actualizaInfoCelda(this.idSolicitud);
												Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
														+ " celdas Actualizadas!"));
											}
											cambiarEstado("P", this.idSolicitud);
											estadoGenError = "P";
											ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
											this.liberaPila();
											return;
										}
									}
								}
								proceso = "Extracci\u00f3n de Informaci\u00f3n en Netezza";
								if (!this.estado.equalsIgnoreCase("X")) {
									if (estadoGenError != "U") {
										cambiarEstado("U", this.idSolicitud);
										estadoGenError = "U";
									}
									final Connection connNz = conNetezza.conectar().getConexion();
									ResultSet rse = null;
									rse = conNetezza.consultar(nEstado);
									if (rse.next()) {
										exist = true;
									}
									rse.close();
									rse = null;
									if (!exist) {
										if (rangoFechas != null && !rangoFechas.isEmpty()) {
											sqlInsert = "INSERT INTO COLECTOR."
													+ (bandContingenciaNetezza.equals("S") ? "COLECTOR." : "")
													+ "RP_DATOS_SOLICITUD(ID_SOLICITUD,OFICIO,SERVICIO,TIPO_REPORTE,FECHA_INI,FECHA_FIN,ANIO,ESTADO,TIPO_PROCESO,FECHAS) VALUES("
													+ this.idSolicitud + ",'" + this.oficio + "','" + this.servicio
													+ "','" + this.tipoReporte + "','" + this.fecha_inicio + "','"
													+ this.fecha_fin + "','" + this.anio + "','" + this.estado + "','"
													+ this.tipoProceso + "','" + rangoFechas + "')";
										}
										conNetezza.escribir(sqlInsert);
										Solicitud.log.info(
												(Object) ("solicitud: " + this.idSolicitud + " insertada en Netezza"));
									}
									final String banWSRoam = props.getProperty("bandRoaming");
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Ejecutando SP en Netezza..."));
									bandSMSNetezzaHW = parametrosGenerales("BAND_SMS_NETEZZA_HW");
									if (banWSRoam.equals("N")) {
										lcs_cs = connNz.prepareCall(paqueteHW);
									} else {
										lcs_cs = connNz.prepareCall(paqueteHW_RNA);
									}
									lcs_cs.registerOutParameter(1, 12);
									lcs_cs.execute();
									result = lcs_cs.getString(1);
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Resultado SP Netezza: " + result));
									lcs_cs.close();
									if (!result.equals("0")) {
										throw new Exception("No se proceso la solicitud: " + this.idSolicitud
												+ " Error netezza: " + result);
									}
									ResultSet rs = null;
									proceso = "Insertar Datos en BD Local";
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " insertando Datos en Bd Local.."));
									connoper = conoper.conectar().getConexion();
									if (this.tipoReporte.equals("3")) {
										connoper.setAutoCommit(false);
										rs = conNetezza.consultar(sqlCDRTmp);
										String NUMERO_ORIGEN = null;
										String NUMERO_DESTINO = null;
										String NUMERO_MARCADO = null;
										String IMEI_SALIENTE = null;
										String IMEI_ENTRANTE = null;
										String IMSI_SALIENTE = null;
										String IMSI_ENTRANTE = null;
										String CELDA_ORIGEN = null;
										String LATITUD_ENTRANTE = null;
										String LONGITUD_ENTRANTE = null;
										String CANTON_ENTRANTE = null;
										String PROVINCIA_ENTRANTE = null;
										String DIRECCION_ENTRANTE = null;
										String CELDA_DESTINO = null;
										String LATITUD_SALIENTE = null;
										String LONGITUD_SALIENTE = null;
										String CANTON_SALIENTE = null;
										String PROVINCIA_SALIENTE = null;
										String DIRECCION_SALIENTE = null;
										String FECHA = null;
										String TIEMPO_INI = null;
										String DURACION = null;
										String TIEMPO_FIN = null;
										String TIPO_CDR = null;
										String TIPO = null;
										long SOLICITUD = 0L;
										nCommit = 0;
										while (rs.next()) {
											++contador;
											++nCommit;
											NUMERO_ORIGEN = rs.getString("NUMERO_ORIGEN");
											NUMERO_DESTINO = rs.getString("NUMERO_DESTINO");
											NUMERO_MARCADO = rs.getString("NUMERO_MARCADO");
											IMEI_SALIENTE = rs.getString("IMEI_SALIENTE");
											IMEI_ENTRANTE = rs.getString("IMEI_ENTRANTE");
											IMSI_SALIENTE = rs.getString("IMSI_SALIENTE");
											IMSI_ENTRANTE = rs.getString("IMSI_ENTRANTE");
											CELDA_ORIGEN = rs.getString("CELDA_ORIGEN");
											LATITUD_ENTRANTE = rs.getString("LATITUD_ENTRANTE");
											LONGITUD_ENTRANTE = rs.getString("LONGITUD_ENTRANTE");
											CANTON_ENTRANTE = rs.getString("CANTON_ENTRANTE");
											PROVINCIA_ENTRANTE = rs.getString("PROVINCIA_ENTRANTE");
											DIRECCION_ENTRANTE = rs.getString("DIRECCION_ENTRANTE");
											CELDA_DESTINO = rs.getString("CELDA_DESTINO");
											LATITUD_SALIENTE = rs.getString("LATITUD_SALIENTE");
											LONGITUD_SALIENTE = rs.getString("LONGITUD_SALIENTE");
											CANTON_SALIENTE = rs.getString("CANTON_SALIENTE");
											PROVINCIA_SALIENTE = rs.getString("PROVINCIA_SALIENTE");
											DIRECCION_SALIENTE = rs.getString("DIRECCION_SALIENTE");
											FECHA = rs.getString("FECHA");
											TIEMPO_INI = rs.getString("TIEMPO_INI");
											DURACION = rs.getString("DURACION");
											TIEMPO_FIN = rs.getString("TIEMPO_FIN");
											TIPO_CDR = rs.getString("TIPO_CDR");
											TIPO = rs.getString("TIPO");
											SOLICITUD = Long.valueOf(rs.getString("SOLICITUD"));
											String ls_rna = "";
											if (banWSRoam.equals("S")) {
												if (TIPO_CDR == "50") {
													ls_rna = "MOVISTAR";
												} else {
													ls_rna = "CLARO";
												}
											}
											lcs_cs_oper = connoper.prepareStatement(
													"INSERT INTO FIS_DATOS_SOLICITUD (ID_DATO_SOLICITUD,ID_SOLICITUD,CAMPO_1,CAMPO_2,CAMPO_3,CAMPO_4,CAMPO_5,CAMPO_6,CAMPO_7,CAMPO_8,CAMPO_9,CAMPO_10,CAMPO_11,CAMPO_12,CAMPO_13,CAMPO_14,CAMPO_15,CAMPO_16,CAMPO_17,CAMPO_18,CAMPO_19,CAMPO_20,CAMPO_21,CAMPO_22,CAMPO_23,CAMPO_24,CAMPO_25,CAMPO_26,CAMPO_27,CAMPO_28,CAMPO_29,CAMPO_30) VALUES (SEQ_FIS_DATOS_SOLICITUD.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
											lcs_cs_oper.setLong(1, SOLICITUD);
											lcs_cs_oper.setString(2, NUMERO_ORIGEN);
											lcs_cs_oper.setString(3, NUMERO_DESTINO);
											lcs_cs_oper.setString(4, NUMERO_MARCADO);
											lcs_cs_oper.setString(5, IMEI_ENTRANTE);
											lcs_cs_oper.setString(6, IMEI_SALIENTE);
											lcs_cs_oper.setString(7, IMSI_ENTRANTE);
											lcs_cs_oper.setString(8, IMSI_SALIENTE);
											lcs_cs_oper.setString(9, CELDA_ORIGEN);
											lcs_cs_oper.setString(10, LATITUD_ENTRANTE);
											lcs_cs_oper.setString(11, LONGITUD_ENTRANTE);
											lcs_cs_oper.setString(12, CANTON_ENTRANTE);
											lcs_cs_oper.setString(13, PROVINCIA_ENTRANTE);
											lcs_cs_oper.setString(14, DIRECCION_ENTRANTE);
											lcs_cs_oper.setString(15, CELDA_DESTINO);
											lcs_cs_oper.setString(16, LATITUD_SALIENTE);
											lcs_cs_oper.setString(17, LONGITUD_SALIENTE);
											lcs_cs_oper.setString(18, CANTON_SALIENTE);
											lcs_cs_oper.setString(19, PROVINCIA_SALIENTE);
											lcs_cs_oper.setString(20, DIRECCION_SALIENTE);
											lcs_cs_oper.setString(21, FECHA);
											lcs_cs_oper.setString(22, TIEMPO_INI);
											lcs_cs_oper.setString(23, DURACION);
											lcs_cs_oper.setString(24, TIEMPO_FIN);
											lcs_cs_oper.setString(25, TIPO_CDR);
											lcs_cs_oper.setString(26, TIPO);
											lcs_cs_oper.setString(27, ls_rna);
											lcs_cs_oper.setString(28, null);
											lcs_cs_oper.setString(29, null);
											lcs_cs_oper.setString(30, null);
											lcs_cs_oper.setString(31, null);
											lcs_cs_oper.executeUpdate();
											lcs_cs_oper.close();
											if (nCommit == 100) {
												nCommit = 0;
												connoper.commit();
											}
										}
										
										connoper.commit();
										connoper.setAutoCommit(true);
										if (contador == 0) {
											InsertarNull(
													"{ call FISK_API_PORTAL_SEGURIDAD.FISP_INGRESO_DATOS_SOLICITUDES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}",
													this.idSolicitud);
										}
										conNetezza.escribir("drop table " + tabla3);
									} else if (this.tipoReporte.equals("5")) {
										connoper.setAutoCommit(false);
										rs = conNetezza.consultar(sqlRADIOBASETmp);
										String NUMERO_ORIGEN = null;
										String NUMERO_DESTINO = null;
										String NUMERO_MARCADO = null;
										String IMEI_SALIENTE = null;
										String IMEI_ENTRANTE = null;
										String IMSI_SALIENTE = null;
										String IMSI_ENTRANTE = null;
										String CELDA = null;
										String LATITUD = null;
										String LONGITUD = null;
										String CANTON = null;
										String PROVINCIA = null;
										String DIRECCION = null;
										String FECHA2 = null;
										String TIEMPO_INI2 = null;
										String DURACION2 = null;
										String TIEMPO_FIN2 = null;
										String TIPO_CDR2 = null;
										long SOLICITUD2 = 0L;
										nCommit = 0;
										while (rs.next()) {
											++contador;
											++nCommit;
											NUMERO_ORIGEN = rs.getString("NUMERO_ORIGEN");
											NUMERO_DESTINO = rs.getString("NUMERO_DESTINO");
											NUMERO_MARCADO = rs.getString("NUMERO_MARCADO");
											IMEI_SALIENTE = rs.getString("IMEI_SALIENTE");
											IMEI_ENTRANTE = rs.getString("IMEI_ENTRANTE");
											IMSI_SALIENTE = rs.getString("IMSI_SALIENTE");
											IMSI_ENTRANTE = rs.getString("IMSI_ENTRANTE");
											CELDA = rs.getString("CELDA");
											LATITUD = rs.getString("LATITUD");
											LONGITUD = rs.getString("LONGITUD");
											CANTON = rs.getString("CANTON");
											PROVINCIA = rs.getString("PROVINCIA");
											DIRECCION = rs.getString("DIRECCION");
											FECHA2 = rs.getString("FECHA");
											TIEMPO_INI2 = rs.getString("TIEMPO_INI");
											DURACION2 = rs.getString("DURACION");
											TIEMPO_FIN2 = rs.getString("TIEMPO_FIN");
											TIPO_CDR2 = rs.getString("TIPO_CDR");
											SOLICITUD2 = Long.valueOf(rs.getString("SOLICITUD"));
											lcs_cs_oper = connoper.prepareStatement(
													"INSERT INTO FIS_DATOS_SOLICITUD (ID_DATO_SOLICITUD,ID_SOLICITUD,CAMPO_1,CAMPO_2,CAMPO_3,CAMPO_4,CAMPO_5,CAMPO_6,CAMPO_7,CAMPO_8,CAMPO_9,CAMPO_10,CAMPO_11,CAMPO_12,CAMPO_13,CAMPO_14,CAMPO_15,CAMPO_16,CAMPO_17,CAMPO_18,CAMPO_19,CAMPO_20,CAMPO_21,CAMPO_22,CAMPO_23,CAMPO_24,CAMPO_25,CAMPO_26,CAMPO_27,CAMPO_28,CAMPO_29,CAMPO_30) VALUES (SEQ_FIS_DATOS_SOLICITUD.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
											lcs_cs_oper.setLong(1, SOLICITUD2);
											lcs_cs_oper.setString(2, NUMERO_ORIGEN);
											lcs_cs_oper.setString(3, NUMERO_DESTINO);
											lcs_cs_oper.setString(4, NUMERO_MARCADO);
											lcs_cs_oper.setString(5, IMEI_ENTRANTE);
											lcs_cs_oper.setString(6, IMEI_SALIENTE);
											lcs_cs_oper.setString(7, IMSI_ENTRANTE);
											lcs_cs_oper.setString(8, IMSI_SALIENTE);
											lcs_cs_oper.setString(9, CELDA);
											lcs_cs_oper.setString(10, LATITUD);
											lcs_cs_oper.setString(11, LONGITUD);
											lcs_cs_oper.setString(12, CANTON);
											lcs_cs_oper.setString(13, PROVINCIA);
											lcs_cs_oper.setString(14, DIRECCION);
											lcs_cs_oper.setString(15, FECHA2);
											lcs_cs_oper.setString(16, TIEMPO_INI2);
											lcs_cs_oper.setString(17, DURACION2);
											lcs_cs_oper.setString(18, TIEMPO_FIN2);
											lcs_cs_oper.setString(19, TIPO_CDR2);
											lcs_cs_oper.setString(20, null);
											lcs_cs_oper.setString(21, null);
											lcs_cs_oper.setString(22, null);
											lcs_cs_oper.setString(23, null);
											lcs_cs_oper.setString(24, null);
											lcs_cs_oper.setString(25, null);
											lcs_cs_oper.setString(26, null);
											lcs_cs_oper.setString(27, null);
											lcs_cs_oper.setString(28, null);
											lcs_cs_oper.setString(29, null);
											lcs_cs_oper.setString(30, null);
											lcs_cs_oper.setString(31, null);
											lcs_cs_oper.executeUpdate();
											lcs_cs_oper.close();
											if (nCommit == 100) {
												nCommit = 0;
												connoper.commit();
											}
										}
										connoper.commit();
										connoper.setAutoCommit(true);
										if (contador == 0) {
											InsertarNull(
													"{ call FISK_API_PORTAL_SEGURIDAD.FISP_INGRESO_DATOS_SOLICITUDES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}",
													this.idSolicitud);
										}
										conNetezza.escribir("drop table " + tabla5);
									} else if (this.tipoReporte.equals("4")) {
										connoper.setAutoCommit(false);
										rs = conNetezza.consultar(sqlSMSTmp);
										String NUMBER_CALLING = null;
										String NUMBER_CALLED = null;
										String DIA = null;
										String HORA = null;
										String TIPO2 = null;
										long SOLICITUD3 = 0L;
										nCommit = 0;
										while (rs.next()) {
											++contador;
											++nCommit;
											NUMBER_CALLING = rs.getString(1);
											NUMBER_CALLED = rs.getString(2);
											DIA = rs.getString(3);
											HORA = rs.getString(4);
											TIPO2 = rs.getString(5);
											SOLICITUD3 = Long.valueOf(rs.getString(6));
											lcs_cs_oper = connoper.prepareStatement(
													"INSERT INTO FIS_DATOS_SOLICITUD (ID_DATO_SOLICITUD,ID_SOLICITUD,CAMPO_1,CAMPO_2,CAMPO_3,CAMPO_4,CAMPO_5,CAMPO_6,CAMPO_7,CAMPO_8,CAMPO_9,CAMPO_10,CAMPO_11,CAMPO_12,CAMPO_13,CAMPO_14,CAMPO_15,CAMPO_16,CAMPO_17,CAMPO_18,CAMPO_19,CAMPO_20,CAMPO_21,CAMPO_22,CAMPO_23,CAMPO_24,CAMPO_25,CAMPO_26,CAMPO_27,CAMPO_28,CAMPO_29,CAMPO_30) VALUES (SEQ_FIS_DATOS_SOLICITUD.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
											lcs_cs_oper.setLong(1, SOLICITUD3);
											lcs_cs_oper.setString(2, NUMBER_CALLING);
											lcs_cs_oper.setString(3, NUMBER_CALLED);
											lcs_cs_oper.setString(4, DIA);
											lcs_cs_oper.setString(5, HORA);
											lcs_cs_oper.setString(6, TIPO2);
											lcs_cs_oper.setString(7, null);
											lcs_cs_oper.setString(8, null);
											lcs_cs_oper.setString(9, null);
											lcs_cs_oper.setString(10, null);
											lcs_cs_oper.setString(11, null);
											lcs_cs_oper.setString(12, null);
											lcs_cs_oper.setString(13, null);
											lcs_cs_oper.setString(14, null);
											lcs_cs_oper.setString(15, null);
											lcs_cs_oper.setString(16, null);
											lcs_cs_oper.setString(17, null);
											lcs_cs_oper.setString(18, null);
											lcs_cs_oper.setString(19, null);
											lcs_cs_oper.setString(20, null);
											lcs_cs_oper.setString(21, null);
											lcs_cs_oper.setString(22, null);
											lcs_cs_oper.setString(23, null);
											lcs_cs_oper.setString(24, null);
											lcs_cs_oper.setString(25, null);
											lcs_cs_oper.setString(26, null);
											lcs_cs_oper.setString(27, null);
											lcs_cs_oper.setString(28, null);
											lcs_cs_oper.setString(29, null);
											lcs_cs_oper.setString(30, null);
											lcs_cs_oper.setString(31, null);
											lcs_cs_oper.executeUpdate();
											lcs_cs_oper.close();
											if (nCommit == 100) {
												nCommit = 0;
												connoper.commit();
											}
										}
										connoper.commit();
										connoper.setAutoCommit(true);
										if (contador == 0) {
											InsertarNull(
													"{ call FISK_API_PORTAL_SEGURIDAD.FISP_INGRESO_DATOS_SOLICITUDES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}",
													this.idSolicitud);
										}
										conNetezza.escribir("drop table " + tabla4);
									}
									rs.close();
									connNz.close();
									connoper.close();
									contador = 0;
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Datos Insertados en BD local Correctamente!"));
								}
								cambiarEstado("X", this.idSolicitud);
								estadoGenError = "X";
								if (this.tipoReporte.equalsIgnoreCase("3") || this.tipoReporte.equalsIgnoreCase("5")) {
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " actualizando informacion de celdas con fecha.."));
									actualizaInfoCelda(this.idSolicitud);
									Solicitud.log.info(
											(Object) ("solicitud: " + this.idSolicitud + " celdas Actualizadas!"));
								}
								cambiarEstado("P", this.idSolicitud);
								estadoGenError = "P";
								ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
								break Label_13373;
							} catch (Exception e) {
								if (connoper != null && !connoper.isClosed()) {
									connoper.rollback();
									connoper.close();
								}
								throw new Exception(e.toString());
							}
						}
						if (this.tipoReporte.equals("1")) {
							proceso = "Extraccion de informacion en Axis";
							Date auxDate = null;
							String bandAbonadoAxis = props.getProperty("bandAbonadoAxis");
							final String usrsNewAbonado = props.getProperty("usrsNewAbonado")
									.toUpperCase(Locale.ENGLISH);
							final String[] listUsrNewAbonado = usrsNewAbonado.split("\\|");
							String fechaIniVista = null;
							String fechaFinVista = null;
							final int diasDisponibles = Integer.parseInt(props.getProperty("dispAbonadoDias"));
							boolean usrInList = false;
							boolean vlrFechasDisponibles = false;
							if (bandAbonadoAxis.equals("N")) {
								for (int x = 0; x < listUsrNewAbonado.length; ++x) {
									if (listUsrNewAbonado[x].equals(this.usuarioIngreso)
											|| listUsrNewAbonado[x].equals("ALL")) {
										usrInList = true;
										break;
									}
								}
								if (!usrInList) {
									bandAbonadoAxis = "S";
								}
							}
							this.fecha_inicio = "";
							this.fecha_fin = "";
							Calendar calendar = Calendar.getInstance();
							calendar = Calendar.getInstance();
							if (usrInList || this.descServicio.equals("IMEI")) {
								calendar.add(6, -diasDisponibles);
								auxDate = calendar.getTime();
								Date auxDateFF = null;
								Date auxDateFI = null;
								fechaIniVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
								fechaFinVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
								auxDateFI = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaIniVista);
								auxDateFF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaFinVista);
								if (auxDateFI.before(auxDate)) {
									fechaIniVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(auxDate);
								}
								if (auxDateFF.after(auxDate)) {
									vlrFechasDisponibles = true;
								}
								Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud + " Fechas asignadas--> "
										+ this.fecha_inicio + " fecha fin-->" + this.fecha_fin));
								Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
										+ " Fechas para crear la vista: fecha ini-->" + fechaIniVista + " fecha fin-->"
										+ fechaFinVista));
							}
							if (bandAbonadoAxis.equals("S")) {
								if (estadoGenError != "A") {
									cambiarEstado("A", this.idSolicitud);
									estadoGenError = "A";
								}
								if (this.descServicio.equals("NUMERO_CEDULA") || this.descServicio.equals("RUC")
										|| this.descServicio.equals("PASAPORTE")) {
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Extraccion extraer info directamente de axis IDENTIFICACION"));
									eisIdServInf = Integer.parseInt(props.getProperty("eisNUMERO_CEDULA"));
									ConsumoWs.procesaAbonados(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin, (String) null);
								}
								if (this.descServicio.equals("NUMERO_CELULAR")) {
									proceso = "Verificaci\u00f3n numero VoiceMail";
									Solicitud.log
											.info((Object) ("solicitud: " + this.idSolicitud + " verificando numero "
													+ this.servicio + " en lista de numeros VoiceMail..!"));
									if (otrasValidaciones.numVoiceMail(this.servicio)) {
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
												+ " El n\u00famero consultado " + this.servicio
												+ " corresponde a un numero de Casillero de Voz"));
										ConsumoWs.grabarMensajeSinDatos(this.idSolicitud,
												"El n\u00famero consultado corresponde a un n\u00famero de Casillero de Voz",
												(String) null);
										cambiarEstado("P", this.idSolicitud);
										estadoGenError = "P";
										ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
										this.liberaPila();
										return;
									}
									eisIdServInf = Integer.parseInt(props.getProperty("eisNUMERO_CELULARV3"));
									final int lenght2 = this.servicio.length();
									this.servicio = this.servicio.substring(lenght2 - 8);
									this.servicio = "'" + this.servicio + "'";
									ConsumoWs.procesaAbonadosV2(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin, (String) null, (String) null);
								}
								if (this.descServicio.equals("IMEI")) {
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Extraccion extraer info directamente de axis IMEI"));
									eisIdServInf = Integer.parseInt(props.getProperty("eisIMEI"));
									if (!vlrFechasDisponibles) {
										ConsumoWs.grabarMensajeSinDatos(
												this.idSolicitud, "informaci\u00f3n disponible en linea "
														+ diasDisponibles + " d\u00edas hacia atr\u00e1s",
												(String) null);
									} else {
										VistaTmpAbonado.createVistaTmp(this.idSolicitud, fechaIniVista, fechaFinVista);
										String imeiTmp = null;
										imeiTmp = String.valueOf(
												String.valueOf(String.valueOf(this.servicio.substring(0, 14)))) + "0";
										imeiTmp = "'" + imeiTmp + "'";
										ConsumoWs.procesaImeiImsiAbonado(this.idSolicitud, imeiTmp, this.fecha_inicio,
												this.fecha_fin, "MSISDN", "IMEI", (String) null, "I");
										VistaTmpAbonado.eliminaVistaTmp(this.idSolicitud);
									}
								}
								if (this.descServicio.equals("ICCID")) {
									eisIdServInf = Integer.parseInt(props.getProperty("eisNumByIccid"));
									ConsumoWs.procesaICCIDAbonado(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin);
								}
								if (this.descServicio.equals("IMSI")) {
									eisIdServInf = Integer.parseInt(props.getProperty("eisImsiByIccid"));
									ConsumoWs.procesaImsiAbonado(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin);
								}
							} else {
								if (estadoGenError != "B") {
									cambiarEstado("B", this.idSolicitud);
									estadoGenError = "B";
								}
								if (this.descServicio.equals("NUMERO_CEDULA") || this.descServicio.equals("RUC")
										|| this.descServicio.equals("PASAPORTE")) {
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Extraccion extraer info IDENTIFICACION"));
									if (!vlrFechasDisponibles) {
										fechaIniVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
												.format(calendar.getTime());
										fechaFinVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
									}
									VistaTmpAbonado.createVistaTmp(this.idSolicitud, fechaIniVista, fechaFinVista);
									eisIdServInf = Integer.parseInt(props.getProperty("eisNUMERO_CEDULA"));
									ConsumoWs.procesaAbonadosCed(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin, (String) null);
									VistaTmpAbonado.eliminaVistaTmp(this.idSolicitud);
								}
								if (this.descServicio.equals("NUMERO_CELULAR")) {
									proceso = "Verificaci\u00f3n numero VoiceMail";
									Solicitud.log
											.info((Object) ("solicitud: " + this.idSolicitud + " verificando numero "
													+ this.servicio + " en lista de numeros VoiceMail..!"));
									if (otrasValidaciones.numVoiceMail(this.servicio)) {
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
												+ " El n\u00famero consultado " + this.servicio
												+ " corresponde a un numero de Casillero de Voz"));
										ConsumoWs.grabarMensajeSinDatos(this.idSolicitud,
												"El n\u00famero consultado corresponde a un n\u00famero de Casillero de Voz",
												(String) null);
										cambiarEstado("P", this.idSolicitud);
										estadoGenError = "P";
										ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
										this.liberaPila();
										return;
									}
									if (!vlrFechasDisponibles) {
										ConsumoWs.grabarMensajeSinDatos(
												this.idSolicitud, "informaci\u00f3n disponible en linea "
														+ diasDisponibles + " d\u00edas hacia atr\u00e1s",
												(String) null);
									} else {
										VistaTmpAbonado.createVistaTmp(this.idSolicitud, fechaIniVista, fechaFinVista);
										String numTmp = null;
										final int lenght3 = this.servicio.length();
										numTmp = this.servicio.substring(lenght3 - 8);
										numTmp = "'593" + numTmp + "'";
										ConsumoWs.procesaNumServicioAbonado(this.idSolicitud, numTmp, this.fecha_inicio,
												this.fecha_fin, "IMEI", "MSISDN");
										VistaTmpAbonado.eliminaVistaTmp(this.idSolicitud);
									}
								}
								if (this.descServicio.equals("IMEI")) {
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Extraccion extraer info IMEI"));
									if (!vlrFechasDisponibles) {
										ConsumoWs.grabarMensajeSinDatos(
												this.idSolicitud, "informaci\u00f3n disponible en linea "
														+ diasDisponibles + " d\u00edas hacia atr\u00e1s",
												(String) null);
									} else {
										VistaTmpAbonado.createVistaTmp(this.idSolicitud, fechaIniVista, fechaFinVista);
										String imeiTmp = null;
										imeiTmp = String.valueOf(
												String.valueOf(String.valueOf(this.servicio.substring(0, 14)))) + "0";
										imeiTmp = "'" + imeiTmp + "'";
										ConsumoWs.procesaImeiImsiAbonado(this.idSolicitud, imeiTmp, this.fecha_inicio,
												this.fecha_fin, "MSISDN", "IMEI", (String) null, "I");
										VistaTmpAbonado.eliminaVistaTmp(this.idSolicitud);
									}
								}
								if (this.descServicio.equals("ICCID")) {
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Extraccion extraer info ICCID"));
									if (!vlrFechasDisponibles) {
										fechaIniVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
												.format(calendar.getTime());
										fechaFinVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
									}
									VistaTmpAbonado.createVistaTmp(this.idSolicitud, fechaIniVista, fechaFinVista);
									eisIdServInf = Integer.parseInt(props.getProperty("eisNumByIccid"));
									ConsumoWs.procesaICCIDAbonado(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin);
									VistaTmpAbonado.eliminaVistaTmp(this.idSolicitud);
								}
								if (this.descServicio.equals("IMSI")) {
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Extraccion extraer info IMSI"));
									if (!vlrFechasDisponibles) {
										fechaIniVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
												.format(calendar.getTime());
										fechaFinVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
									}
									VistaTmpAbonado.createVistaTmp(this.idSolicitud, fechaIniVista, fechaFinVista);
									eisIdServInf = Integer.parseInt(props.getProperty("eisImsiByIccid"));
									ConsumoWs.procesaImsiAbonado(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin);
									VistaTmpAbonado.eliminaVistaTmp(this.idSolicitud);
								}
							}
							cambiarEstado("P", this.idSolicitud);
							estadoGenError = "P";
							ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
							Solicitud.log.info((Object) ("procesoEjecuta " + procesoEjecuta));
						} else if (this.tipoReporte.equals("7")) {
							Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
									+ " verificando estado de numero " + this.servicio + " en axis para las fechas: "
									+ this.fecha_inicio + " ; " + this.fecha_fin));
							proceso = "Extraccion de informacion en Axis";
							Date auxDate = null;
							String bandAbonadoAxis = props.getProperty("bandAbonadoAxis");
							final String usrsNewAbonado = props.getProperty("usrsNewAbonado")
									.toUpperCase(Locale.ENGLISH);
							final String[] listUsrNewAbonado = usrsNewAbonado.split("\\|");
							String fechaIniVista = null;
							String fechaFinVista = null;
							final int diasDisponibles = Integer.parseInt(props.getProperty("dispAbonadoDias"));
							boolean usrInList = false;
							boolean vlrFechasDisponibles = false;
							if (bandAbonadoAxis.equals("N")) {
								for (int x = 0; x < listUsrNewAbonado.length; ++x) {
									if (listUsrNewAbonado[x].equals(this.usuarioIngreso)
											|| listUsrNewAbonado[x].equals("ALL")) {
										usrInList = true;
										break;
									}
								}
								if (!usrInList) {
									bandAbonadoAxis = "S";
								}
							}
							this.fecha_inicio = "";
							this.fecha_fin = "";
							Calendar calendar = Calendar.getInstance();
							calendar = Calendar.getInstance();
							if (usrInList || this.descServicio.equals("IMEI")) {
								calendar.add(6, -diasDisponibles);
								auxDate = calendar.getTime();
								Date auxDateFF = null;
								Date auxDateFI = null;
								fechaIniVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
								fechaFinVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
								auxDateFI = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaIniVista);
								auxDateFF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaFinVista);
								if (auxDateFI.before(auxDate)) {
									fechaIniVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(auxDate);
								}
								if (auxDateFF.after(auxDate)) {
									vlrFechasDisponibles = true;
								}
								Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud + " Fechas asignadas--> "
										+ this.fecha_inicio + " fecha fin-->" + this.fecha_fin));
								Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
										+ " Fechas para crear la vista: fecha ini-->" + fechaIniVista + " fecha fin-->"
										+ fechaFinVista));
							}
							if (bandAbonadoAxis.equals("S")) {
								if (estadoGenError != "A") {
									cambiarEstado("A", this.idSolicitud);
									estadoGenError = "A";
								}
								if (this.descServicio.equals("NUMERO_CEDULA") || this.descServicio.equals("RUC")
										|| this.descServicio.equals("PASAPORTE")) {
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Extraccion extraer info directamente de axis IDENTIFICACION"));
									eisIdServInf = Integer.parseInt(props.getProperty("eisNUMERO_CEDULA_HW"));
									ConsumoWs.procesaAbonados(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin, (String) null);
								}
								if (this.descServicio.equals("NUMERO_CELULAR")) {
									proceso = "Verificaci\u00f3n numero VoiceMail";
									Solicitud.log
											.info((Object) ("solicitud: " + this.idSolicitud + " verificando numero "
													+ this.servicio + " en lista de numeros VoiceMail..!"));
									if (otrasValidaciones.numVoiceMail(this.servicio)) {
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
												+ " El n\u00famero consultado " + this.servicio
												+ " corresponde a un numero de Casillero de Voz"));
										ConsumoWs.grabarMensajeSinDatos(this.idSolicitud,
												"El n\u00famero consultado corresponde a un numero de Casillero de Voz.",
												(String) null);
										cambiarEstado("P", this.idSolicitud);
										estadoGenError = "P";
										ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
										if ("345".indexOf(this.tipoReporte.toString()) >= 0) {
											--ProcesaSolicitudes.contSolP345;
										} else {
											--ProcesaSolicitudes.contSolP126;
										}
										return;
									}
									eisIdServInf = Integer.parseInt(props.getProperty("eisNUMERO_CELULAR_HW"));
									final int lenght2 = this.servicio.length();
									this.servicio = this.servicio.substring(lenght2 - 8);
									this.servicio = "'" + this.servicio + "'";
									ConsumoWs.procesaAbonadosV2(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin, (String) null, (String) null);
								}
								if (this.descServicio.equals("ICCID")) {
									eisIdServInf = Integer.parseInt(props.getProperty("eisNumByIccid"));
									ConsumoWs.procesaICCIDAbonadoHW(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin);
								}
								if (this.descServicio.equals("IMSI")) {
									eisIdServInf = Integer.parseInt(props.getProperty("eisImsiByIccid"));
									ConsumoWs.procesaImsiAbonadoHW(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin);
								}
							} else {
								if (estadoGenError != "B") {
									cambiarEstado("B", this.idSolicitud);
									estadoGenError = "B";
								}
								if (this.descServicio.equals("NUMERO_CEDULA") || this.descServicio.equals("RUC")
										|| this.descServicio.equals("PASAPORTE")) {
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Extraccion extraer info IDENTIFICACION"));
									if (!vlrFechasDisponibles) {
										fechaIniVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
												.format(calendar.getTime());
										fechaFinVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
									}
									VistaTmpAbonado.createVistaTmp(this.idSolicitud, fechaIniVista, fechaFinVista);
									eisIdServInf = Integer.parseInt(props.getProperty("eisNUMERO_CEDULA"));
									ConsumoWs.procesaAbonadosCed(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin, (String) null);
									VistaTmpAbonado.eliminaVistaTmp(this.idSolicitud);
								}
								if (this.descServicio.equals("NUMERO_CELULAR")) {
									proceso = "Verificaci\u00f3n numero VoiceMail";
									Solicitud.log
											.info((Object) ("solicitud: " + this.idSolicitud + " verificando numero "
													+ this.servicio + " en lista de numeros VoiceMail..!"));
									if (otrasValidaciones.numVoiceMail(this.servicio)) {
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
												+ " El n\u00famero consultado " + this.servicio
												+ " corresponde a un numero de Casillero de Voz"));
										ConsumoWs.grabarMensajeSinDatos(this.idSolicitud,
												"El n\u00famero consultado corresponde a un numero de Casillero de Voz.",
												(String) null);
										cambiarEstado("P", this.idSolicitud);
										estadoGenError = "P";
										ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
										if ("345".indexOf(this.tipoReporte.toString()) >= 0) {
											--ProcesaSolicitudes.contSolP345;
										} else {
											--ProcesaSolicitudes.contSolP126;
										}
										return;
									}
									if (!vlrFechasDisponibles) {
										ConsumoWs.grabarMensajeSinDatos(
												this.idSolicitud, "informaci\u00f3n disponible en linea "
														+ diasDisponibles + " d\u00edas hacia atr\u00e1s",
												(String) null);
									} else {
										VistaTmpAbonado.createVistaTmp(this.idSolicitud, fechaIniVista, fechaFinVista);
										String numTmp = null;
										final int lenght3 = this.servicio.length();
										numTmp = this.servicio.substring(lenght3 - 8);
										numTmp = "'593" + numTmp + "'";
										ConsumoWs.procesaNumServicioAbonado(this.idSolicitud, numTmp, this.fecha_inicio,
												this.fecha_fin, "IMEI", "MSISDN");
										VistaTmpAbonado.eliminaVistaTmp(this.idSolicitud);
									}
								}
								if (this.descServicio.equals("ICCID")) {
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Extraccion extraer info ICCID"));
									if (!vlrFechasDisponibles) {
										fechaIniVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
												.format(calendar.getTime());
										fechaFinVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
									}
									VistaTmpAbonado.createVistaTmp(this.idSolicitud, fechaIniVista, fechaFinVista);
									eisIdServInf = Integer.parseInt(props.getProperty("eisNumByIccid"));
									ConsumoWs.procesaICCIDAbonado(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin);
									VistaTmpAbonado.eliminaVistaTmp(this.idSolicitud);
								}
								if (this.descServicio.equals("IMSI")) {
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Extraccion extraer info IMSI"));
									if (!vlrFechasDisponibles) {
										fechaIniVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
												.format(calendar.getTime());
										fechaFinVista = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
									}
									VistaTmpAbonado.createVistaTmp(this.idSolicitud, fechaIniVista, fechaFinVista);
									eisIdServInf = Integer.parseInt(props.getProperty("eisImsiByIccid"));
									ConsumoWs.procesaImsiAbonado(this.idSolicitud, eisIdServInf, this.servicio,
											this.fecha_inicio, this.fecha_fin);
									VistaTmpAbonado.eliminaVistaTmp(this.idSolicitud);
								}
							}
							final String banderaAboHW = props.getProperty("banderaReporteAbonadoHW");
							proceso = "Verificaci\u00f3n estado de linea HW";
							Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
									+ "Extraccion de informacion Huawei Online"));
							Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud + " verificando estado de HW "
									+ this.servicio + " en axis para las fechas: " + this.fecha_inicio + " ; "
									+ this.fecha_fin));
							if (banderaAboHW.equals("S")) {
								if (estadoGenError != "W") {
									cambiarEstado("W", this.idSolicitud);
									estadoGenError = "W";
								}
								final String parametros2 = "ID_SOLICITUD";
								final String valorParametros2 = this.idSolicitud;
								final String tagsParametros2 = ConsumoWs.setearTags("ID_SOLICITUD", valorParametros2);
								final String sre_valor = props.getProperty("sreTipoTransaccionExtraeInfoHW");
								Solicitud.log
										.info((Object) ("solicitud: " + this.idSolicitud + " sre_valor: " + sre_valor));
								final String[] respSRE2 = ConsumoWs.consumeSreHW(tagsParametros2,
										props.getProperty("sreTipoTransaccionExtraeInfoHW"));
								Solicitud.log
										.info((Object) ("solicitud: " + this.idSolicitud + " XXX1: " + respSRE2[0]));
								Solicitud.log
										.info((Object) ("solicitud: " + this.idSolicitud + " XXX2: " + respSRE2[1]));
								Solicitud.log
										.info((Object) ("solicitud: " + this.idSolicitud + " XXX3: " + respSRE2[2]));
								Solicitud.log
										.info((Object) ("solicitud: " + this.idSolicitud + " XXX4: " + respSRE2[3]));
								try {
									proceso = "Extraccion de informacion Huawei Historico";
									eisIdServInf = Integer.parseInt(props.getProperty("eisID_TRANSANCCION"));
									final String psTipo = String
											.valueOf(String.valueOf(String.valueOf(this.descServicio))) + "-F";
									ConsumoWs.procesaAbonadosHW(this.idSolicitud, eisIdServInf);
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Extraccion de informacion Huawei Historico"));
									final Connection connDWH = conDWH.conectar().getConexion();
									connDWH.setAutoCommit(false);
									final ResultSet rsDWHdelete = conDWH.consultar(sqlDWH);
									if (rsDWHdelete.next()) {
										conDWH.escribir(" DELETE FROM FIS_REP_ABONADO  WHERE ID_SOLICITUD ="
												+ this.idSolicitud + "AND ORIGEN ='F'");
										connDWH.commit();
									}
									connDWH.setAutoCommit(true);
									rsDWHdelete.close();
									final String paquetemcDWH = "{call pck_dwh_medidas_cautelares.SP_CONSULTA_ABONADO_HIST(?,?,?,?,?,?)}";
									try {
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
												+ " 2 Extraccion de informacion Huawei Historico"));
										final CallableStatement cst = connDWH.prepareCall(
												"{call pck_dwh_medidas_cautelares.SP_CONSULTA_ABONADO_HIST(?,?,?,?,?,?)}");
										Solicitud.log.info((Object) ("idSolicitud " + this.idSolicitud));
										cst.setString(1, this.idSolicitud);
										Solicitud.log.info(
												(Object) ("solicitud: " + this.idSolicitud + " descServicio" + psTipo));
										cst.setString(2, psTipo);
										final int vlongitud2 = this.IdServicioHW.length();
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud + " tipo_consulta"
												+ this.IdServicioHW));
										if (this.descServicio.equals("NUMERO_CELULAR")) {
											cst.setString(3, this.IdServicioHW.substring(vlongitud2 - 9));
											Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
													+ " tipo_consulta" + this.IdServicioHW.substring(vlongitud2 - 9)));
										} else {
											cst.setString(3, this.IdServicioHW);
											Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
													+ " tipo_consulta" + this.IdServicioHW));
										}
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud + " fecha_inicio"
												+ this.fecha_inicio));
										cst.setString(4, this.fecha_inicio);
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud + " fecha_fin"
												+ this.fecha_fin));
										cst.setString(5, this.fecha_fin);
										cst.registerOutParameter(6, 12);
										cst.execute();
										final String codError = cst.getString(6);
										Solicitud.log.info(
												(Object) ("solicitud: " + this.idSolicitud + " codError.." + codError));
										if (!codError.equals("0")) {
											Solicitud.log.error((Object) ("Solicitud: " + this.idSolicitud
													+ " - pck_dwh_medidas_cautelares.SP_CONSULTA_ABONADO_HIST - Pv_error: "
													+ codError));
											throw new Exception(codError);
										}
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
												+ " Insertando de informacion Huawei"));
									} catch (Exception e2) {
										Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
												+ " ERROR informacion Huawei" + e2.toString()));
									}
									try {
										ResultSet rsDWH = null;
										rsDWH = conDWH.consultar(sqlDWH);
										connoper = conoper.conectar().getConexion();
										connoper.setAutoCommit(false);
										String NOMBRE_COMPLETO = null;
										String NO_IMEI = null;
										String NO_IMSI = null;
										String NO_ICCID = null;
										String IDENTIFICACION = null;
										String DIRECCION_REFERENCIA = null;
										String TELEFONO_REFERENCIA = null;
										String NUMERO_CELULAR = null;
										String FECHA_INICIO = null;
										String FECHA_FIN = null;
										String TIPO_ABONADO = null;
										String CIUDAD = null;
										long SOLICITUD4 = 0L;
										nCommit = 0;
										while (rsDWH.next()) {
											++contador;
											++nCommit;
											Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
													+ " INGRESO :  " + this.IdServicioHW));
											NOMBRE_COMPLETO = rsDWH.getString("NOMBRE_COMPLETO");
											NO_IMEI = rsDWH.getString("NO_IMEI");
											NO_IMSI = rsDWH.getString("NO_IMSI");
											NO_ICCID = rsDWH.getString("NO_ICCID");
											IDENTIFICACION = rsDWH.getString("IDENTIFICACION");
											DIRECCION_REFERENCIA = rsDWH.getString("DIRECCION_REFERENCIA");
											TELEFONO_REFERENCIA = rsDWH.getString("TELEFONO_REFERENCIA");
											NUMERO_CELULAR = rsDWH.getString("NUMERO_CELULAR");
											FECHA_INICIO = rsDWH.getString("FECHA_INICIO");
											FECHA_FIN = rsDWH.getString("FECHA_FIN");
											TIPO_ABONADO = rsDWH.getString("TIPO_ABONADO");
											CIUDAD = rsDWH.getString("CIUDAD");
											SOLICITUD4 = Long.valueOf(rsDWH.getString("ID_SOLICITUD"));
											lcs_cs_operDWH = connoper.prepareStatement(
													"INSERT INTO FIS_REP_ABONADO_DWH (ID_SOLICITUD, NOMBRE_COMPLETO, NO_IMEI, NO_IMSI, NO_ICCID, IDENTIFICACION, DIRECCION_REFERENCIA, TELEFONO_REFERENCIA, NUMERO_CELULAR, FECHA_INICIO, FECHA_FIN, TIPO_ABONADO, CIUDAD )  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ");
											lcs_cs_operDWH.setLong(1, SOLICITUD4);
											lcs_cs_operDWH.setString(2, NOMBRE_COMPLETO);
											lcs_cs_operDWH.setString(3, NO_IMEI);
											lcs_cs_operDWH.setString(4, NO_IMSI);
											lcs_cs_operDWH.setString(5, NO_ICCID);
											lcs_cs_operDWH.setString(6, IDENTIFICACION);
											lcs_cs_operDWH.setString(7, DIRECCION_REFERENCIA);
											lcs_cs_operDWH.setString(8, TELEFONO_REFERENCIA);
											lcs_cs_operDWH.setString(9, NUMERO_CELULAR);
											lcs_cs_operDWH.setString(10, FECHA_INICIO);
											lcs_cs_operDWH.setString(11, FECHA_FIN);
											lcs_cs_operDWH.setString(12, TIPO_ABONADO);
											lcs_cs_operDWH.setString(13, CIUDAD);
											lcs_cs_operDWH.executeUpdate();
											lcs_cs_operDWH.close();
											if (nCommit == 100) {
												nCommit = 0;
												connoper.commit();
											}
										}
										connoper.commit();
										connoper.setAutoCommit(true);
										rsDWH.close();
									} catch (Exception ex) {
										Solicitud.log.error(
												(Object) ("solicitud: " + this.idSolicitud + " " + ex.toString()),
												(Throwable) ex);
										throw new Exception(ex.toString());
									}
									connDWH.close();
									final String parametrosSRE = "ID_SOLICITUD";
									final String valorParametrosSRE = this.idSolicitud;
									final String tagsParametrosSRE = ConsumoWs.setearTags("ID_SOLICITUD",
											valorParametrosSRE);
									final String sre_valorSRE = props.getProperty("sreTipoTransaccionHW_DWH");
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud + " sre_valor: "
											+ sre_valorSRE));
									final String[] respSREHW2 = ConsumoWs.consumeSreHW_DWH(tagsParametrosSRE,
											sre_valorSRE);
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud + " COD_RESPUESTA: "
											+ respSREHW2[0]));
									Solicitud.log.info(
											(Object) ("solicitud: " + this.idSolicitud + " VALOR: " + respSREHW2[1]));
									Solicitud.log.info(
											(Object) ("solicitud: " + this.idSolicitud + " MENSAJE: " + respSREHW2[2]));
									connoper.close();
								} catch (Exception e3) {
									Solicitud.log.error((Object) ("solicitud: " + this.idSolicitud + " Estado: "
											+ estadoGenError + " Proceso: " + proceso + ": " + e3.getMessage()));
									LogErrores.RegistrarError(this.idSolicitud, this.oficio, estadoGenError, proceso,
											e3.toString());
									LogErrores.registrarIntentosErrorSolicitud(this.idSolicitud);
									Solicitud.log.error(
											(Object) ("solicitud: " + this.idSolicitud + " " + e3.toString()),
											(Throwable) e3);
								}
							}
							if (bandAbonadoAxis.equals("S")) {
								if (this.descServicio.equals("IMEI")) {
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
											+ " Extraccion extraer info directamente de axis IMEI"));
									eisIdServInf = Integer.parseInt(props.getProperty("eisIMEI"));
									if (!vlrFechasDisponibles) {
										ConsumoWs.grabarMensajeSinDatos(
												this.idSolicitud, "informaci\u00f3n disponible en linea "
														+ diasDisponibles + " d\u00edas hacia atr\u00e1s",
												(String) null);
									} else {
										VistaTmpAbonado.createVistaTmp(this.idSolicitud, fechaIniVista, fechaFinVista);
										String imeiTmp2 = null;
										imeiTmp2 = String.valueOf(
												String.valueOf(String.valueOf(this.servicio.substring(0, 14)))) + "0";
										imeiTmp2 = "'" + imeiTmp2 + "'";
										ConsumoWs.procesaImeiImsiAbonadoHW(this.idSolicitud, imeiTmp2,
												this.fecha_inicio, this.fecha_fin, "MSISDN", "IMEI", (String) null,
												"I");
										VistaTmpAbonado.eliminaVistaTmp(this.idSolicitud);
									}
								}
							} else if (this.descServicio.equals("IMEI")) {
								Solicitud.log.info(
										(Object) ("solicitud: " + this.idSolicitud + " Extraccion extraer info IMEI"));
								if (!vlrFechasDisponibles) {
									ConsumoWs
											.grabarMensajeSinDatos(
													this.idSolicitud, "informaci\u00f3n disponible en linea "
															+ diasDisponibles + " d\u00edas hacia atr\u00e1s",
													(String) null);
								} else {
									VistaTmpAbonado.createVistaTmp(this.idSolicitud, fechaIniVista, fechaFinVista);
									String imeiTmp2 = null;
									imeiTmp2 = String.valueOf(
											String.valueOf(String.valueOf(this.servicio.substring(0, 14)))) + "0";
									imeiTmp2 = "'" + imeiTmp2 + "'";
									ConsumoWs.procesaImeiImsiAbonadoHW(this.idSolicitud, imeiTmp2, this.fecha_inicio,
											this.fecha_fin, "MSISDN", "IMEI", (String) null, "I");
									VistaTmpAbonado.eliminaVistaTmp(this.idSolicitud);
								}
							}
							cambiarEstado("P", this.idSolicitud);
							estadoGenError = "P";
							ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
						} else if (this.tipoReporte.equals("2")) {
							if (estadoGenError != "B") {
								cambiarEstado("B", this.idSolicitud);
								estadoGenError = "B";
							}
							final String banWSGeo = props.getProperty("bandWSGeoLocali");
							proceso = "Verificaci\u00f3n numero VoiceMail";
							Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud + " verificando numero "
									+ this.servicio + " en lista de numeros VoiceMail..!"));
							if (otrasValidaciones.numVoiceMail(this.servicio)) {
								Solicitud.log
										.info((Object) ("solicitud: " + this.idSolicitud + " El n\u00famero consultado "
												+ this.servicio + " corresponde a un numero de Casillero de Voz"));
								ConsumoWs.grabarMensajeSinDatos(this.idSolicitud,
										"El n\u00famero consultado corresponde a un n\u00famero de Casillero de Voz",
										(String) null);
								cambiarEstado("P", this.idSolicitud);
								estadoGenError = "P";
								ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
								this.liberaPila();
								return;
							}
							final String usrsGeolocalizador = props.getProperty("usrsGeolocalizador")
									.toUpperCase(Locale.ENGLISH);
							final String[] listUsrNewAbonado2 = usrsGeolocalizador.split("\\|");
							boolean usrInList2 = false;
							String bandGeolocalizador = props.getProperty("bandGeolocalizador")
									.toUpperCase(Locale.ENGLISH);
							if (bandGeolocalizador.equals("S")) {
								for (int x2 = 0; x2 < listUsrNewAbonado2.length; ++x2) {
									if (listUsrNewAbonado2[x2].equals(this.usuarioIngreso)
											|| listUsrNewAbonado2[x2].equals("ALL")) {
										usrInList2 = true;
										break;
									}
								}
								if (!usrInList2) {
									bandGeolocalizador = "N";
								}
							}
							if (banWSGeo.equalsIgnoreCase("S")) {
								Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud
										+ "Inicio Proceso GeoLocalizador Intento : " + intentosProceso));
								if (intentosProceso < 3) {
									proceso = "Extraccion de informacion en WS Geolocalizador";
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud + " - " + proceso));
									ConsumoWs.procesaWSGeolocalizador(this.idSolicitud, this.servicio,
											this.usuarioIngreso);
									final File fichero = new File("request.xml");
									System.out.println(" 1 Borrado de Archivo:" + fichero.getAbsolutePath());
									if (fichero.delete()) {
										System.out.println("1 El fichero ha sido borrado satisfactoriamente");
									} else {
										System.out.println("1 El fichero no puede ser borrado");
									}
								} else {
									proceso = "Extraccion de informacion en WS Suase";
									Solicitud.log.info((Object) ("solicitud: " + this.idSolicitud + " - " + proceso));
									ConsumoWs.procesaSuase(this.idSolicitud, this.servicio);
								}
								Solicitud.log.info(
										(Object) ("solicitud: " + this.idSolicitud + " Fin de Proceso GeoLocalizador"));
							} else if (bandGeolocalizador.equalsIgnoreCase("S")) {
								proceso = "Extraccion de informacion Geolocalizador";
								ConsumoWs.procesaGeolocalizador(this.idSolicitud, this.servicio);
							} else {
								proceso = "Extraccion de informacion en WS Suase";
								ConsumoWs.procesaSuase(this.idSolicitud, this.servicio);
							}
							cambiarEstado("P", this.idSolicitud);
							estadoGenError = "P";
							ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
						} else if (this.tipoReporte.equals("6")) {
							if (estadoGenError != "B") {
								cambiarEstado("B", this.idSolicitud);
								estadoGenError = "B";
							}
							proceso = "Extracci\u00f3n de Informaci\u00f3n en BD local";
							eisIdServInf = Integer.parseInt(props.getProperty("eisUBIC_RADIO_BASE"));
							ConsumoWs.procesaRadioBaseV2(this.idSolicitud, eisIdServInf, this.servicio);
							cambiarEstado("P", this.idSolicitud);
							estadoGenError = "P";
							ejecutaJar(procesoEjecuta, this.idSolicitud, this.oficio, estadoGenError);
						}
					}
					flagProceso = false;
					this.liberaPila();
				} catch (Exception e4) {
					Solicitud.log.error((Object) ("solicitud: " + this.idSolicitud + " estadoGenError: "
							+ estadoGenError + " Proceso: " + proceso + ": " + e4.getMessage()));
					LogErrores.RegistrarError(this.idSolicitud, this.oficio, estadoGenError, proceso, e4.toString());
					LogErrores.registrarIntentosErrorSolicitud(this.idSolicitud);
					Solicitud.log.error((Object) e4.toString(), (Throwable) e4);
					if (intentosProceso >= maxIntentosProceso) {
						throw new Exception(e4.toString());
					}
					continue;
				}
			}
		} catch (Exception e5) {
			Solicitud.log.error((Object) ("solicitud: " + this.idSolicitud + " Estado: " + estadoGenError + " Proceso: "
					+ proceso + ": " + e5.toString()));
			Solicitud.log.error((Object) ("solicitud: " + this.idSolicitud + " Cambiando estado a la solicitud..."));
			cambiarEstado("E", this.idSolicitud);
			this.liberaPila();
			LogErrores.RegistrarError(this.idSolicitud, this.oficio, estadoGenError, proceso, e5.toString());
			enviaMail.enviaMailError(this.idSolicitud, proceso, estadoGenError, this.oficio, this.tipoReporte,
					e5.toString());
			Solicitud.log.error((Object) e5.toString(), (Throwable) e5);
			try {
				if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
					conoper.getConexion().close();
				}
				if (conNetezza.getConexion() != null && !conNetezza.getConexion().isClosed()) {
					conNetezza.getConexion().close();
				}
				if (conDWH.getConexion() != null && conDWH.getConexion().isClosed()) {
					conDWH.getConexion().close();
				}
			} catch (Exception e6) {
				Solicitud.log.fatal((Object) ("solicitud: " + this.idSolicitud + ": " + e6.toString()), (Throwable) e6);
			}
			return;
		} finally {
			try {
				if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
					conoper.getConexion().close();
				}
				if (conNetezza.getConexion() != null && !conNetezza.getConexion().isClosed()) {
					conNetezza.getConexion().close();
				}
				if (conDWH.getConexion() != null && conDWH.getConexion().isClosed()) {
					conDWH.getConexion().close();
				}
			} catch (Exception e7) {
				Solicitud.log.fatal((Object) ("solicitud: " + this.idSolicitud + ": " + e7.toString()), (Throwable) e7);
			}
		}
		try {
			if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
				conoper.getConexion().close();
			}
			if (conNetezza.getConexion() != null && !conNetezza.getConexion().isClosed()) {
				conNetezza.getConexion().close();
			}
			if (conDWH.getConexion() != null && conDWH.getConexion().isClosed()) {
				conDWH.getConexion().close();
			}
		} catch (Exception e7) {
			Solicitud.log.fatal((Object) ("solicitud: " + this.idSolicitud + ": " + e7.toString()), (Throwable) e7);
		}
		try {
			if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
				conoper.getConexion().close();
			}
			if (conNetezza.getConexion() != null && !conNetezza.getConexion().isClosed()) {
				conNetezza.getConexion().close();
			}
			if (conDWH.getConexion() != null && conDWH.getConexion().isClosed()) {
				conDWH.getConexion().close();
			}
		} catch (Exception e8) {
			Solicitud.log.fatal((Object) ("solicitud: " + this.idSolicitud + ": " + e8.toString()), (Throwable) e8);
		}
		try {
			if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
				conoper.getConexion().close();
			}
			if (conNetezza.getConexion() != null && !conNetezza.getConexion().isClosed()) {
				conNetezza.getConexion().close();
			}
			if (conDWH.getConexion() != null && conDWH.getConexion().isClosed()) {
				conDWH.getConexion().close();
			}
		} catch (Exception e9) {
			Solicitud.log.fatal((Object) ("solicitud: " + this.idSolicitud + ": " + e9.toString()), (Throwable) e9);
		}
	}

	public static void InsertarNull(final String paquete, final String idSolicitud) throws Exception {
		final ConexionO conoper = new ConexionO();
		try {
			CallableStatement lcs_cs_oper = null;
			final Connection connoper = conoper.conectar().getConexion();
			lcs_cs_oper = connoper.prepareCall(paquete);
			lcs_cs_oper.setLong(1, Long.valueOf(idSolicitud));
			lcs_cs_oper.setString(2, "N/A");
			lcs_cs_oper.setString(3, null);
			lcs_cs_oper.setString(4, null);
			lcs_cs_oper.setString(5, null);
			lcs_cs_oper.setString(6, null);
			lcs_cs_oper.setString(7, null);
			lcs_cs_oper.setString(8, null);
			lcs_cs_oper.setString(9, null);
			lcs_cs_oper.setString(10, null);
			lcs_cs_oper.setString(11, null);
			lcs_cs_oper.setString(12, null);
			lcs_cs_oper.setString(13, null);
			lcs_cs_oper.setString(14, null);
			lcs_cs_oper.setString(15, null);
			lcs_cs_oper.setString(16, null);
			lcs_cs_oper.setString(17, null);
			lcs_cs_oper.setString(18, null);
			lcs_cs_oper.setString(19, null);
			lcs_cs_oper.setString(20, null);
			lcs_cs_oper.setString(21, null);
			lcs_cs_oper.setString(22, null);
			lcs_cs_oper.setString(23, null);
			lcs_cs_oper.setString(24, null);
			lcs_cs_oper.setString(25, null);
			lcs_cs_oper.setString(26, null);
			lcs_cs_oper.setString(27, null);
			lcs_cs_oper.setString(28, null);
			lcs_cs_oper.setString(29, null);
			lcs_cs_oper.setString(30, null);
			lcs_cs_oper.setString(31, null);
			lcs_cs_oper.registerOutParameter(32, 12);
			lcs_cs_oper.registerOutParameter(33, 12);
			lcs_cs_oper.executeUpdate();
			final String ls_cod_error = lcs_cs_oper.getString(32);
			final String ls_msj_error = lcs_cs_oper.getString(33);
			lcs_cs_oper.close();
			connoper.close();
			if (!ls_cod_error.equals("0")) {
				throw new Exception(ls_msj_error);
			}
		} catch (Exception e) {
			Solicitud.log.error((Object) ("solicitud: " + idSolicitud + ": " + e.toString()), (Throwable) e);
			e.printStackTrace();
			Solicitud.log.error((Object) e.toString(), (Throwable) e);
			throw new Exception("solicitud: " + idSolicitud + " Error al insertar n/a: " + e.toString());
		} finally {
			try {
				if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
					conoper.getConexion().close();
				}
			} catch (Exception e2) {
				Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e2.toString()), (Throwable) e2);
			}
		}
		try {
			if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
				conoper.getConexion().close();
			}
		} catch (Exception e2) {
			Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e2.toString()), (Throwable) e2);
		}
		try {
			if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
				conoper.getConexion().close();
			}
		} catch (Exception e3) {
			Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e3.toString()), (Throwable) e3);
		}
		try {
			if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
				conoper.getConexion().close();
			}
		} catch (Exception e4) {
			Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e4.toString()), (Throwable) e4);
		}
	}

	public static void insertarDescAdicional(final String paquete, final String idSolicitud, final String descAdicional,
			final String valorDescAdicional) throws Exception {
		final ConexionO conoper = new ConexionO();
		try {
			CallableStatement lcs_cs_oper = null;
			final Connection connoper = conoper.conectar().getConexion();
			lcs_cs_oper = connoper.prepareCall(paquete);
			lcs_cs_oper.setLong(1, Long.valueOf(idSolicitud));
			lcs_cs_oper.setString(2, null);
			lcs_cs_oper.setString(3, null);
			lcs_cs_oper.setString(4, null);
			lcs_cs_oper.setString(5, null);
			lcs_cs_oper.setString(6, null);
			lcs_cs_oper.setString(7, null);
			lcs_cs_oper.setString(8, null);
			lcs_cs_oper.setString(9, null);
			lcs_cs_oper.setString(10, null);
			lcs_cs_oper.setString(11, null);
			lcs_cs_oper.setString(12, null);
			lcs_cs_oper.setString(13, null);
			lcs_cs_oper.setString(14, null);
			lcs_cs_oper.setString(15, null);
			lcs_cs_oper.setString(16, null);
			lcs_cs_oper.setString(17, null);
			lcs_cs_oper.setString(18, null);
			lcs_cs_oper.setString(19, null);
			lcs_cs_oper.setString(20, null);
			lcs_cs_oper.setString(21, null);
			lcs_cs_oper.setString(22, null);
			lcs_cs_oper.setString(23, null);
			lcs_cs_oper.setString(24, null);
			lcs_cs_oper.setString(25, null);
			lcs_cs_oper.setString(26, null);
			lcs_cs_oper.setString(27, null);
			lcs_cs_oper.setString(28, null);
			lcs_cs_oper.setString(29, descAdicional);
			lcs_cs_oper.setString(30, valorDescAdicional);
			lcs_cs_oper.setString(31, null);
			lcs_cs_oper.registerOutParameter(32, 12);
			lcs_cs_oper.registerOutParameter(33, 12);
			lcs_cs_oper.executeUpdate();
			final String ls_cod_error = lcs_cs_oper.getString(32);
			final String ls_msj_error = lcs_cs_oper.getString(33);
			lcs_cs_oper.close();
			connoper.close();
			Solicitud.log.info((Object) ("solicitud: " + idSolicitud + ": INGRESO - insertarDescAdicional"));
			if (!ls_cod_error.equals("0")) {
				throw new Exception(ls_msj_error);
			}
		} catch (Exception e) {
			Solicitud.log.error((Object) ("solicitud: " + idSolicitud + ": " + e.toString()), (Throwable) e);
			e.printStackTrace();
			Solicitud.log.error((Object) e.toString(), (Throwable) e);
			throw new Exception(
					"solicitud: " + idSolicitud + " Error al insertar descripcion adicional: " + e.toString());
		} finally {
			try {
				if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
					conoper.getConexion().close();
				}
			} catch (Exception e2) {
				Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e2.toString()), (Throwable) e2);
			}
		}
		try {
			if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
				conoper.getConexion().close();
			}
		} catch (Exception e2) {
			Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e2.toString()), (Throwable) e2);
		}
		try {
			if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
				conoper.getConexion().close();
			}
		} catch (Exception e3) {
			Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e3.toString()), (Throwable) e3);
		}
		try {
			if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
				conoper.getConexion().close();
			}
		} catch (Exception e4) {
			Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e4.toString()), (Throwable) e4);
		}
	}

	public static void cambiarEstado(final String estNuevo, final String idSolicitud) {
		final String proceso = "Cambiar Estado";
		final ConexionO conoper = new ConexionO();
		try {
			final Connection con = conoper.conectar().getConexion();
			final CallableStatement cst = con
					.prepareCall("{CALL FISK_API_PORTAL_TRANSACCIONES.FISP_CAMBIA_ESTADO(?,?,?,?)}");
			cst.setLong(1, Long.valueOf(idSolicitud));
			cst.setString(2, estNuevo);
			cst.registerOutParameter(3, 12);
			cst.registerOutParameter(4, 12);
			cst.execute();
			final String codError = cst.getString(3);
			final String msjError = cst.getString(4);
			cst.close();
			con.close();
			if (!codError.equals("0")) {
				throw new Exception(msjError);
			}
			Solicitud.log.info((Object) ("SOLICITUD: " + idSolicitud + " Proceso: " + "Cambiar Estado"
					+ " Se modifico ESTADO: " + estNuevo + " en la BD"));
		} catch (Exception e) {
			Solicitud.log.error((Object) (" Solicitud: " + idSolicitud + " Proceso: " + "Cambiar Estado"
					+ ": No se pudo cambiar el estado a la solicitud; " + e.toString()));
			Solicitud.log.error((Object) e.toString(), (Throwable) e);
			try {
				if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
					conoper.getConexion().close();
				}
			} catch (Exception e2) {
				Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e2.toString()), (Throwable) e2);
			}
			return;
		} finally {
			try {
				if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
					conoper.getConexion().close();
				}
			} catch (Exception e3) {
				Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e3.toString()), (Throwable) e3);
			}
		}
		try {
			if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
				conoper.getConexion().close();
			}
		} catch (Exception e3) {
			Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e3.toString()), (Throwable) e3);
		}
		try {
			if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
				conoper.getConexion().close();
			}
		} catch (Exception e4) {
			Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e4.toString()), (Throwable) e4);
		}
		try {
			if (conoper.getConexion() != null && !conoper.getConexion().isClosed()) {
				conoper.getConexion().close();
			}
		} catch (Exception e5) {
			Solicitud.log.fatal((Object) ("Cerrar Conexion: " + e5.toString()), (Throwable) e5);
		}
	}

	public static void ejecutaJar(final String procesoEjecuta) {
		final String proceso = "Ejecuta Jar";
		try {
			final Runtime r = Runtime.getRuntime();
			final Process p = r.exec(procesoEjecuta);
			final InputStream stderr = p.getErrorStream();
			final InputStream stdie = p.getInputStream();
			final InputStreamReader isrer = new InputStreamReader(stderr);
			final InputStreamReader isrie = new InputStreamReader(stdie);
			final BufferedReader brer = new BufferedReader(isrer);
			final BufferedReader brie = new BufferedReader(isrie);
			String line = "";
			String outResult = "";
			while ((line = brie.readLine()) != null) {
				outResult = line;
			}
			while ((line = brer.readLine()) != null) {
				outResult = String.valueOf(String.valueOf(String.valueOf(outResult))) + line + "\n";
			}
			final int exitVal = p.waitFor();
			Solicitud.log.info((Object) ("Process Ejecuta Jar exitValue: " + exitVal));
			p.destroy();
			if (exitVal != 0) {
				throw new Exception("Error: " + outResult);
			}
		} catch (Exception e) {
			Solicitud.log.error((Object) ("Proceso: Ejecuta Jar: " + e.toString()), (Throwable) e);
		}
	}

	public static void ejecutaJar(final String procesoEjecuta, final String idSolicitud, final String oficio,
			final String estadoGenError) {
		final String proceso = "Ejecuta Jar";
		try {
			final Runtime r = Runtime.getRuntime();
			final Process p = r.exec(procesoEjecuta);
			final InputStream stderr = p.getErrorStream();
			final InputStream stdie = p.getInputStream();
			final InputStreamReader isrer = new InputStreamReader(stderr);
			final InputStreamReader isrie = new InputStreamReader(stdie);
			final BufferedReader brer = new BufferedReader(isrer);
			final BufferedReader brie = new BufferedReader(isrie);
			String line = "";
			String outResult = "";
			while ((line = brie.readLine()) != null) {
				outResult = line;
			}
			while ((line = brer.readLine()) != null) {
				outResult = String.valueOf(String.valueOf(String.valueOf(outResult))) + line + "\n";
			}
			final int exitVal = p.waitFor();
			Solicitud.log.info((Object) ("Process Ejecuta Jar exitValue: " + exitVal));
			p.destroy();
			if (exitVal != 0) {
				throw new Exception("Error: " + outResult);
			}
		} catch (Exception e) {
			Solicitud.log.error((Object) ("Proceso: Ejecuta Jar: " + e.toString()), (Throwable) e);
			Solicitud.log.error((Object) ("solicitud: " + idSolicitud + " Estado: " + estadoGenError + " Proceso: "
					+ "Ejecuta Jar" + ": " + e.toString()));
			LogErrores.RegistrarError(idSolicitud, oficio, estadoGenError, "Ejecuta Jar", e.toString());
			Solicitud.log.error((Object) ("solicitud: " + idSolicitud + " Cambiando estado a la solicitud..."));
			cambiarEstado("E", idSolicitud);
		}
	}

	public static void actualizaInfoCelda(final String idSolicitud) throws Exception {
		final ConexionO cnx = new ConexionO();
		final Connection con = cnx.conectar().getConexion();
		Solicitud.log.info((Object) ("Solicitud: " + idSolicitud
				+ " Actualiza celdas - FISK_API_PORTAL_TRANSACCIONES.FISP_ACTUALIZA_CELDAS"));
		final CallableStatement cst = con
				.prepareCall("{call FISK_API_PORTAL_TRANSACCIONES.FISP_ACTUALIZA_CELDAS(?,?,?)}");
		cst.setLong(1, Long.valueOf(idSolicitud));
		cst.registerOutParameter(2, 12);
		cst.registerOutParameter(3, 12);
		cst.execute();
		final String codError = cst.getString(2);
		final String msjError = cst.getString(3);
		Solicitud.log.info((Object) ("Solicitud: " + idSolicitud
				+ " - FISK_API_PORTAL_TRANSACCIONES.FISP_ACTUALIZA_CELDAS - Pv_error: " + msjError));
		cst.close();
		con.close();
		if (!codError.equals("0")) {
			Solicitud.log.error((Object) ("Solicitud: " + idSolicitud
					+ " - FISK_API_PORTAL_TRANSACCIONES.FISP_ACTUALIZA_CELDAS - Pv_error: " + msjError));
			throw new Exception(msjError);
		}
	}

	private void liberaPila() {
		final String tiposReporte345 = "345";
		if ("345".indexOf(this.tipoReporte.toString()) >= 0) {
			if (this.tipoProceso.equals("O")) {
				--ProcesaSolicitudes.contSolP345;
			} else {
				--ProcesaSolicitudes.contSolHisP345;
			}
		} else {
			--ProcesaSolicitudes.contSolP126;
		}
	}

	public static String parametrosGenerales(final String idParametro) throws Exception {
		String valor = "";
		final ConexionO cnx = new ConexionO();
		final Connection con = cnx.conectar().getConexion();
		try {
			final String query = "select valor from fis_parametros_generales_hw where id_parametro='" + idParametro
					+ "'";
			final PreparedStatement pst = con.prepareStatement(query);
			final ResultSet rs = pst.executeQuery();
			if (rs.next()) {
				valor = rs.getString(1);
				rs.close();
				pst.close();
				con.close();
				return valor;
			}
			rs.close();
			pst.close();
			con.close();
			throw new Exception("Bandera invalida o no existe");
		} catch (Exception ex) {
			ex.printStackTrace();
			Solicitud.log.error((Object) ex.toString(), (Throwable) ex);
			throw new Exception(ex);
		}
	}

	public static void InsertaValidaServicioHw(final String ServicioHw, final String idSolicitud,
			final String FInicioHW, final String FFinHW, final String EstadoHW) {
		try {
			Solicitud.log.info((Object) "Inicia -InsertaValidaServicioHw");
			final String sqlDWH = " SELECT ID_SOLICITUD, NOMBRE_COMPLETO, NO_IMEI, NO_IMSI, NO_ICCID, IDENTIFICACION, DIRECCION_REFERENCIA, TELEFONO_REFERENCIA, NUMERO_CELULAR, FECHA_INICIO, FECHA_FIN, TIPO_ABONADO, CIUDAD  FROM FIS_REP_ABONADO  WHERE ID_SOLICITUD ="
					+ idSolicitud + " AND ORIGEN = 'F' "
					+ " AND TO_DATE(FECHA_FIN, 'DD/MM/YYYY HH24:MI:SS') <= SYSDATE " + " AND FECHA_FIN IS NOT NULL\t"
					+ " ORDER BY FECHA_INICIO";
			final String insertDatosDWH = "INSERT INTO FIS_REP_ABONADO_DWH (ID_SOLICITUD, NOMBRE_COMPLETO, NO_IMEI, NO_IMSI, NO_ICCID, IDENTIFICACION, DIRECCION_REFERENCIA, TELEFONO_REFERENCIA, NUMERO_CELULAR, FECHA_INICIO, FECHA_FIN, TIPO_ABONADO, CIUDAD ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
			final String psTipo = "NUMERO_CELULAR-F";
			final String estadoGenError = EstadoHW;
			final String result = "0";
			int contador = 0;
			final ConexionO conoper = new ConexionO();
			final ConexionD conDWH = new ConexionD();
			Connection connoper = conoper.conectar().getConexion();
			PreparedStatement lcs_cs_operDWH = null;
			String proceso = "Hilo Procesar solicitudes ";
			int eisIdServInf = 0;
			int nCommit = 0;
			final Properties props = new Propiedades().getPropiedades();
			final String banderaAboHW = props.getProperty("banderaReporteAbonadoHW");
			proceso = "Verificaci\u00f3n estado de linea HW";
			Solicitud.log.info((Object) "Extraccion de informacion Huawei Online");
			Solicitud.log.info((Object) ("solicitud: " + ServicioHw + " verificando estado de HW " + ServicioHw
					+ " en axis para las fechas: " + FInicioHW + " ; " + FFinHW));
			if (banderaAboHW.equals("S")) {
				final String parametros = "ID_SOLICITUD";
				final String valorParametros = idSolicitud;
				final String tagsParametros = ConsumoWs.setearTags("ID_SOLICITUD", valorParametros);
				final String sre_valor = props.getProperty("sreTipoTransaccionExtraeInfoHW");
				System.out.println("sre_valor: " + sre_valor);
				final String[] respSRE = ConsumoWs.consumeSreHW(tagsParametros,
						props.getProperty("sreTipoTransaccionExtraeInfoHW"));
				System.out.println("InsXXX1: " + respSRE[0]);
				System.out.println("InsXXX2: " + respSRE[1]);
				System.out.println("InsXXX3: " + respSRE[2]);
				System.out.println("InsXXX4: " + respSRE[3]);
				if (!respSRE[0].equals("0")) {
					Solicitud.log.warn((Object) ("solicitud: " + idSolicitud
							+ " Error en Consumo SRE proceso HW Online " + ServicioHw));
					throw new Exception("Error en Consumo SRE proceso HW Online ");
				}
				try {
					proceso = "Extraccion de informacion Huawei Historico REP 3,4,5";
					eisIdServInf = Integer.parseInt(props.getProperty("eisID_TRANSANCCION"));
					Solicitud.log.info((Object) "Extraccion de informacion Huawei Historico REP 3,4,5");
					final Connection connDWH = conDWH.conectar().getConexion();
					connDWH.setAutoCommit(false);
					final ResultSet rsDWHdelete = conDWH.consultar(sqlDWH);
					if (rsDWHdelete.next()) {
						conDWH.escribir(
								" DELETE FROM FIS_REP_ABONADO  WHERE ID_SOLICITUD =" + idSolicitud + "AND ORIGEN ='F'");
						connDWH.commit();
					}
					connDWH.setAutoCommit(true);
					rsDWHdelete.close();
					final String paquetemcDWH = "{call pck_dwh_medidas_cautelares.SP_CONSULTA_ABONADO_HIST(?,?,?,?,?,?)}";
					try {
						Solicitud.log.info((Object) "2 Extraccion de informacion Huawei Historico REP 3,4,5");
						final CallableStatement cst = connDWH
								.prepareCall("{call pck_dwh_medidas_cautelares.SP_CONSULTA_ABONADO_HIST(?,?,?,?,?,?)}");
						Solicitud.log.info((Object) ("idSolicitud" + idSolicitud));
						cst.setString(1, idSolicitud);
						Solicitud.log.info((Object) "descServicioNUMERO_CELULAR-F");
						cst.setString(2, "NUMERO_CELULAR-F");
						final int vlongitud = ServicioHw.length();
						Solicitud.log.info((Object) ("tipo_consulta" + ServicioHw));
						cst.setString(3, ServicioHw.substring(vlongitud - 9));
						Solicitud.log.info((Object) ("tipo_consulta" + ServicioHw));
						Solicitud.log.info((Object) ("fecha_inicio" + FInicioHW));
						cst.setString(4, FInicioHW);
						Solicitud.log.info((Object) ("fecha_fin" + FFinHW));
						cst.setString(5, FFinHW);
						cst.registerOutParameter(6, 12);
						cst.execute();
						final String codError = cst.getString(6);
						Solicitud.log.info((Object) ("codError.." + codError));
						if (!codError.equals("0")) {
							Solicitud.log.error((Object) ("Solicitud: " + idSolicitud
									+ " - pck_dwh_medidas_cautelares.SP_CONSULTA_ABONADO_HIST - Pv_error: "
									+ codError));
							throw new Exception(codError);
						}
						Solicitud.log.info((Object) "Insertando de informacion Huawei");
					} catch (Exception e) {
						Solicitud.log.info((Object) ("ERROR informacion Huawei REP 3,4,5 " + e.toString()));
					}
					try {
						ResultSet rsDWH = null;
						rsDWH = conDWH.consultar(sqlDWH);
						connoper = conoper.conectar().getConexion();
						connoper.setAutoCommit(false);
						String NOMBRE_COMPLETO = null;
						String NO_IMEI = null;
						String NO_IMSI = null;
						String NO_ICCID = null;
						String IDENTIFICACION = null;
						String DIRECCION_REFERENCIA = null;
						String TELEFONO_REFERENCIA = null;
						String NUMERO_CELULAR = null;
						String FECHA_INICIO = null;
						String FECHA_FIN = null;
						String TIPO_ABONADO = null;
						String CIUDAD = null;
						long SOLICITUD = 0L;
						nCommit = 0;
						while (rsDWH.next()) {
							++contador;
							++nCommit;
							Solicitud.log.info((Object) ("INGRESO :  " + ServicioHw));
							NOMBRE_COMPLETO = rsDWH.getString("NOMBRE_COMPLETO");
							NO_IMEI = rsDWH.getString("NO_IMEI");
							NO_IMSI = rsDWH.getString("NO_IMSI");
							NO_ICCID = rsDWH.getString("NO_ICCID");
							IDENTIFICACION = rsDWH.getString("IDENTIFICACION");
							DIRECCION_REFERENCIA = rsDWH.getString("DIRECCION_REFERENCIA");
							TELEFONO_REFERENCIA = rsDWH.getString("TELEFONO_REFERENCIA");
							NUMERO_CELULAR = rsDWH.getString("NUMERO_CELULAR");
							FECHA_INICIO = rsDWH.getString("FECHA_INICIO");
							FECHA_FIN = rsDWH.getString("FECHA_FIN");
							TIPO_ABONADO = rsDWH.getString("TIPO_ABONADO");
							CIUDAD = rsDWH.getString("CIUDAD");
							SOLICITUD = Long.valueOf(rsDWH.getString("ID_SOLICITUD"));
							lcs_cs_operDWH = connoper.prepareStatement(
									"INSERT INTO FIS_REP_ABONADO_DWH (ID_SOLICITUD, NOMBRE_COMPLETO, NO_IMEI, NO_IMSI, NO_ICCID, IDENTIFICACION, DIRECCION_REFERENCIA, TELEFONO_REFERENCIA, NUMERO_CELULAR, FECHA_INICIO, FECHA_FIN, TIPO_ABONADO, CIUDAD ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
							lcs_cs_operDWH.setLong(1, SOLICITUD);
							lcs_cs_operDWH.setString(2, NOMBRE_COMPLETO);
							lcs_cs_operDWH.setString(3, NO_IMEI);
							lcs_cs_operDWH.setString(4, NO_IMSI);
							lcs_cs_operDWH.setString(5, NO_ICCID);
							lcs_cs_operDWH.setString(6, IDENTIFICACION);
							lcs_cs_operDWH.setString(7, DIRECCION_REFERENCIA);
							lcs_cs_operDWH.setString(8, TELEFONO_REFERENCIA);
							lcs_cs_operDWH.setString(9, NUMERO_CELULAR);
							lcs_cs_operDWH.setString(10, FECHA_INICIO);
							lcs_cs_operDWH.setString(11, FECHA_FIN);
							lcs_cs_operDWH.setString(12, TIPO_ABONADO);
							lcs_cs_operDWH.setString(13, CIUDAD);
							lcs_cs_operDWH.executeUpdate();
							lcs_cs_operDWH.close();
							if (nCommit == 100) {
								nCommit = 0;
								connoper.commit();
							}
						}
						connoper.commit();
						connoper.setAutoCommit(true);
						rsDWH.close();
					} catch (Exception ex) {
						Solicitud.log.error((Object) ex.toString(), (Throwable) ex);
						throw new Exception(ex.toString());
					}
					connDWH.close();
					connoper.close();
				} catch (Exception e2) {
					LogErrores.RegistrarError(idSolicitud, "Consulta HW Reporte 3-4-5", estadoGenError, proceso,
							e2.toString());
					LogErrores.registrarIntentosErrorSolicitud(idSolicitud);
					Solicitud.log.error((Object) e2.toString(), (Throwable) e2);
				}
			}
			Solicitud.log.info((Object) "FIN InsertaValidaServicioHw");
		} catch (Exception ex2) {
		}
	}
}