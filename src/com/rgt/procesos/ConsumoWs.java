package com.rgt.procesos;

import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import java.io.PrintWriter;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import java.io.FileInputStream;
import org.apache.commons.httpclient.methods.PostMethod;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.axis.message.MessageElement;
import servicioweb.SreReceptaTransaccionResponseElement;
import servicioweb.SreReceptaTransaccionElement;
import servicioweb.SreReceptaTransaccionElementParametros_Transaccion;
import servicioweb.SreSoapHttpStub;
import com.rgt.objetos.RadioBase;
import com.rgt.utils.ConsumeServlet;
import java.sql.CallableStatement;
import com.rgt.objetos.Suase;
import claro.datos.WS_SUASE_WEBPortBindingStub;
import com.rgt.conexion.ConexionE;
import java.io.File;
import java.sql.PreparedStatement;
import com.rgt.conexion.ConexionO;
import java.util.Iterator;
import java.util.Date;
import java.text.SimpleDateFormat;
import org.jdom2.Document;
import java.io.Reader;
import axis.EISApiOnlineWS_wsdl.types.EipConsumeServicioResponseElement;
import org.jdom2.Element;
import java.io.StringReader;
import org.jdom2.input.SAXBuilder;
import axis.EISApiOnlineWS_wsdl.types.EipConsumeServicio_Out;
import axis.EISApiOnlineWS_wsdl.types.EipConsumeServicioElement;
import javax.xml.rpc.Service;
import axis.EISApiOnlineWS_wsdl.types.EisSoapHttpStub;
import java.net.URL;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.util.Properties;
import com.rgt.utils.DigitoVerificador;
import com.rgt.conexion.ConexionOpr;
import java.util.List;
import com.rgt.objetos.Abonado;
import java.util.ArrayList;
import com.rgt.utils.Propiedades; 

public class ConsumoWs
{
    static Logger log;
    
    static {
        ConsumoWs.log = Propiedades.getMyLogger();
    }
    
    public static void main(final String[] args) {
        try {
            String latitud = "02� 07 20.100S";
            System.out.println("latitud original --> " + latitud);
            latitud = caracteresLatitudlongitud(latitud);
            System.out.println("latitud despues --> " + latitud);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
        }
    }
    
    public static void grabarMensajeSinDatos(final String id_solicitud, final String descripDatosVacios, final String hoja) throws Exception {
        final List<Abonado> datos = new ArrayList<Abonado>();
        final Abonado abonado = new Abonado(id_solicitud, descripDatosVacios, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, hoja);
        datos.add(abonado);
        insertaAbonado(datos);
    }
    
    public static void procesaNumServicioAbonado(final String id_solicitud, String tramaValor, final String fechaIni, final String fechaFin, final String select, final String where) throws Exception {
        final String proceso = "Procesa Numero de Servicio Abonado";
        try {
            final Properties props = new Propiedades().getPropiedades();
            final String nombreVista = "FIS_DATOS_AIC_" + id_solicitud;
            final String queryDatos = "select distinct(SUBSTR (" + select + ",1,14)) valores from " + nombreVista + " where " + where + " in (" + tramaValor + ")";
            String tramaValores = null;
            String tramaValoresToAxis = null;
            String sAux = "";
            String descripDatosVacios = "";
            final List<String> numImei = new ArrayList<String>();
            boolean flag = true;
            final ConexionOpr cnx = new ConexionOpr();
            final Connection con = cnx.conectar().getConexion();
            final Statement stq = con.createStatement();
            final ResultSet rs = stq.executeQuery(queryDatos);
            while (rs.next()) {
                if (flag) {
                    tramaValores = "'" + rs.getString("valores") + "0" + "'";
                    tramaValoresToAxis = "'" + rs.getString("valores") + DigitoVerificador.obtieneDigitoVerificador(rs.getString("valores")) + "'";
                    flag = false;
                }
                else {
                    tramaValores = String.valueOf(tramaValores) + "," + "'" + rs.getString("valores") + "0" + "'";
                    sAux = "'" + rs.getString("valores") + DigitoVerificador.obtieneDigitoVerificador(rs.getString("valores")) + "'";
                    tramaValoresToAxis = String.valueOf(tramaValoresToAxis) + "," + sAux;
                }
            }
            stq.close();
            rs.close();
            con.close();
            if (tramaValores == null || tramaValores.isEmpty()) {
                tramaValor = tramaValor.replace("'", "");
                tramaValor = tramaValor.substring(tramaValor.length() - 8);
                if (props.getProperty("vlrNullToAxis").equalsIgnoreCase("S")) {
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + ", Celular 5939" + tramaValor + " no devolvi\u00f3 imei's.. consulta directo hacia axis"));
                    tramaValor = "'" + tramaValor + "'";
                    procesaAbonadosV3(id_solicitud, Integer.parseInt(props.getProperty("eisNUMERO_CELULARV3")), tramaValor, fechaIni, fechaFin, null, null);
                }
                else {
                    descripDatosVacios = "El(Los) N\u00famero(s) " + tramaValor + " no registra(n) tr\u00e1fica en la red desde el rango de fechas ingresado";
                    grabarMensajeSinDatos(id_solicitud, descripDatosVacios, null);
                }
                return;
            }
            procesaImeiImsiAbonado(id_solicitud, tramaValores, fechaIni, fechaFin, "MSISDN", "IMEI", null, "I");
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void procesaImeiImsiAbonado(final String id_solicitud, String tramaValor, final String fechaIni, final String fechaFin, final String select, final String where, final String hoja, final String ImeiImsi) throws Exception {
        final String proceso = "Procesa Imei e Imsi de Abonado";
        try {
            final Properties props = new Propiedades().getPropiedades();
            final String nombreVista = "FIS_DATOS_AIC_" + id_solicitud;
            final String queryDatos = "select distinct (obtiene_telefono_dnc(" + select + ")) valor from " + nombreVista + " where " + where + " in (" + tramaValor + ")";
            String numTmp = null;
            String tramaValores = null;
            boolean flag = true;
            final List<String> valores = new ArrayList<String>();
            final ConexionOpr cnx = new ConexionOpr();
            final Connection con = cnx.conectar().getConexion();
            final Statement st = con.createStatement();
            final ResultSet rs = st.executeQuery(queryDatos);
            int eisIdServInf = 0;
            String descripDatosVacios = "";
            while (rs.next()) {
                numTmp = rs.getString("valor");
                numTmp = numTmp.substring(numTmp.length() - 8);
                if (flag) {
                    tramaValores = "'" + numTmp + "'";
                    flag = false;
                }
                else {
                    tramaValores = String.valueOf(tramaValores) + "," + "'" + numTmp + "'";
                }
            }
            st.close();
            rs.close();
            con.close();
            if (tramaValores == null || tramaValores.isEmpty() || tramaValores.equals("")) {
                tramaValor = tramaValor.replace("'", "");
                String printE = "";
                if (ImeiImsi.equals("I")) {
                    eisIdServInf = Integer.parseInt(props.getProperty("eisIMEI"));
                    printE = "solicitud " + id_solicitud + ", Imei " + tramaValor + " no devolvi\u00f3 numeros celulares.. consulta directo hacia axis";
                    descripDatosVacios = "El(Los) imei('s) no registra(n) tr\u00e1fica en la red desde el rango de fechas ingresado";
                }
                else {
                    eisIdServInf = Integer.parseInt(props.getProperty("eisIMSI"));
                    printE = "solicitud " + id_solicitud + ", Imsi " + tramaValor + " no devolvi\u00f3 numeros celulares.. consulta directo hacia axis";
                    descripDatosVacios = "El(Los) imsi('s) " + tramaValor + " no registra(n) tr\u00e1fica en la red desde el rango de fechas ingresado";
                }
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + printE));
                grabarMensajeSinDatos(id_solicitud, descripDatosVacios, hoja);
                return;
            }
            procesaAbonadosV3(id_solicitud, Integer.parseInt(props.getProperty("eisNUMERO_CELULARV3")), tramaValores, fechaIni, fechaFin, "N\u00fameros Resultantes:", hoja);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void procesaICCIDAbonado(final String id_solicitud, final int id_Transaccion, final String valor, final String fechaIni, final String fechaFin) throws Exception {
        final String proceso = "Procesa ICCID Abonado";
        try {
            final Properties props = new Propiedades().getPropiedades();
            final String dataSource = props.getProperty("DataSourceEISAXIS");
            final String eisWsUrl = props.getProperty("eisWsUrl");
            final String userBDAxis = props.getProperty("userBDAxis");
            boolean flag = true;
            final String descripDatosVacios = "";
            final BigDecimal idTransaccion = new BigDecimal(String.valueOf(id_Transaccion));
            final URL url = new URL(eisWsUrl);
            final EisSoapHttpStub ws = new EisSoapHttpStub(url, (Service)null);
            final EipConsumeServicioElement parameters = new EipConsumeServicioElement();
            EipConsumeServicioResponseElement response = null;
            EipConsumeServicio_Out resultado = new EipConsumeServicio_Out();
            parameters.setDsId(dataSource);
            parameters.setPnIdServicioInformacion(idTransaccion);
            parameters.setPvParametroBind1(valor);
            parameters.setPvParametroBind2("?");
            parameters.setPvParametroBind3("?");
            parameters.setPvParametroBind4("?");
            parameters.setPvParametroBind5("?");
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros EISNumServicioByIccid:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " idTransaccion -->" + idTransaccion));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " valor -->" + valor));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind2 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind3 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind4 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind5 --> ?"));
            response = ws.eipConsumeServicio(parameters);
            resultado = response.getResult();
            resultado = response.getResult();
            if (resultado.getPnerrorOut() != 0) {
                throw new Exception(resultado.getPverrorOut());
            }
            String result = resultado.getPvresultadoOut();
            System.out.print("Resultado EISNumServicioByIccid:");
            System.out.print(result);
            if (result == null || result.isEmpty() || result == "") {
                final List<Abonado> datos = new ArrayList<Abonado>();
                final Abonado abonado = new Abonado(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null);
                datos.add(abonado);
                insertaAbonado(datos);
                return;
            }
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            String tramaValor = "";
            for (int i = 0; i < list.size(); ++i) {
                final Element tabla = (Element) list.get(i);
                if (flag) {
                    tramaValor = "'" + tabla.getChildTextTrim("ID_SERVICIO") + "'";
                    flag = false;
                }
                else {
                    tramaValor = String.valueOf(tramaValor) + "," + "'" + tabla.getChildTextTrim("ID_SERVICIO") + "'";
                }
            }
            procesaAbonadosV2(id_solicitud, Integer.parseInt(props.getProperty("eisNUMERO_CELULARV3")), tramaValor, "", "", "N\u00fameros Resultantes:", null);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void procesaImsiAbonado(final String id_solicitud, final int id_Transaccion, final String valor, final String fechaIni, final String fechaFin) throws Exception {
        final String proceso = "Procesa Imsi de Abonado";
        try {
            final Properties props = new Propiedades().getPropiedades();
            final String dataSource = props.getProperty("DataSourceEISAXIS");
            final String eisWsUrl = props.getProperty("eisWsUrl");
            final String userBDAxis = props.getProperty("userBDAxis");
            final boolean flag = true;
            final String descripDatosVacios = "";
            final BigDecimal idTransaccion = new BigDecimal(String.valueOf(id_Transaccion));
            final URL url = new URL(eisWsUrl);
            final EisSoapHttpStub ws = new EisSoapHttpStub(url, (Service)null);
            final EipConsumeServicioElement parameters = new EipConsumeServicioElement();
            EipConsumeServicioResponseElement response = null;
            EipConsumeServicio_Out resultado = new EipConsumeServicio_Out();
            parameters.setDsId(dataSource);
            parameters.setPnIdServicioInformacion(idTransaccion);
            parameters.setPvParametroBind1(valor);
            parameters.setPvParametroBind2("?");
            parameters.setPvParametroBind3("IMSI");
            parameters.setPvParametroBind4(String.valueOf(userBDAxis) + "CL_SIMCARD");
            parameters.setPvParametroBind5("DISTINCT(ICCID)");
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros EISIccidByImsi:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " idTransaccion -->" + idTransaccion));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " valor -->" + valor));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind2 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind3 --> ICCID"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind4 --> " + userBDAxis + "CL_SIMCARD"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind5 --> DISTINCT(IMSI)"));
            response = ws.eipConsumeServicio(parameters);
            resultado = response.getResult();
            if (resultado.getPnerrorOut() != 0) {
                throw new Exception(resultado.getPverrorOut());
            }
            String result = resultado.getPvresultadoOut();
            System.out.print("Resultado EISIccidByImsi:");
            System.out.print(result);
            if (result == null || result.isEmpty() || result == "") {
                final List<Abonado> datos = new ArrayList<Abonado>();
                final Abonado abonado = new Abonado(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null);
                datos.add(abonado);
                insertaAbonado(datos);
                return;
            }
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            String tramaValor = "";
            for (int i = 0; i < list.size(); ++i) {
                final Element tabla = (Element) list.get(i);
                tramaValor = tabla.getChildTextTrim("NUMERO");
            }
            procesaICCIDAbonado(id_solicitud, Integer.parseInt(props.getProperty("eisNumByIccid")), tramaValor, fechaIni, fechaFin);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void procesaAbonados(final String id_solicitud, final int id_Transaccion, final String valor, String fechaIni, String fechaFin, final String hoja) throws Exception {
        final String proceso = "Procesa Abonados";
        Date auxDate = null;
        try {
            if (fechaIni == null || fechaIni.equals("0")) {
                fechaIni = "";
            }
            if (fechaFin == null || fechaFin.equals("0")) {
                fechaFin = "";
            }
            if (!fechaIni.equals("")) {
                auxDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaIni);
                fechaIni = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(auxDate);
            }
            if (!fechaFin.equals("")) {
                auxDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaFin);
                fechaFin = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(auxDate);
            }
            final Properties props = new Propiedades().getPropiedades();
            final String dataSource = props.getProperty("DataSourceEISAXIS");
            final String eisWsUrl = props.getProperty("eisWsUrl");
            final BigDecimal idTransaccion = new BigDecimal(String.valueOf(id_Transaccion));
            final URL url = new URL(eisWsUrl);
            final EisSoapHttpStub ws = new EisSoapHttpStub(url, (Service)null);
            final EipConsumeServicioElement parameters = new EipConsumeServicioElement();
            EipConsumeServicioResponseElement response = null;
            EipConsumeServicio_Out resultado = new EipConsumeServicio_Out();
            parameters.setDsId(dataSource);
            parameters.setPnIdServicioInformacion(idTransaccion);
            parameters.setPvParametroBind1(valor);
            parameters.setPvParametroBind2(fechaIni);
            parameters.setPvParametroBind3(fechaFin);
            parameters.setPvParametroBind4("?");
            parameters.setPvParametroBind5("?");
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros EIS Abonado:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " idTransaccion -->" + idTransaccion));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " valor -->" + valor));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " fechaIni -->" + fechaIni));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " fechaFin -->" + fechaFin));
            response = ws.eipConsumeServicio(parameters);
            resultado = response.getResult();
            resultado = response.getResult();
            if (resultado.getPnerrorOut() != 0) {
                throw new Exception(resultado.getPverrorOut());
            }
            String result = resultado.getPvresultadoOut();
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Resultado EISAbonado:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + result));
            final List<Abonado> datos = new ArrayList<Abonado>();
            if (result == null || result.isEmpty() || result == "") {
                final Abonado abonado = new Abonado(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, hoja);
                datos.add(abonado);
                insertaAbonado(datos);
                return;
            }
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            for (int i = 0; i < list.size(); ++i) {
                final Element tabla = (Element) list.get(i);
                final String NOMBRE_COMPLETO = tabla.getChildTextTrim("NOMBRE_COMPLETO");
                final String IMEI = tabla.getChildTextTrim("IMEI");
                final String IMSI = tabla.getChildTextTrim("IMSI");
                final String ICCID = tabla.getChildTextTrim("ICCID");
                final String IDENTIFICACION = tabla.getChildTextTrim("IDENTIFICACION");
                final String DIRECCION_COMPLETA = tabla.getChildTextTrim("DIRECCION_COMPLETA");
                final String TELEFONO1 = tabla.getChildTextTrim("TELEFONO1");
                final String FECHA_INICIO = tabla.getChildTextTrim("FECHA_INICIO");
                final String FECHA_FIN = tabla.getChildTextTrim("FECHA_FIN");
                final String ID_SERVICIO = tabla.getChildTextTrim("ID_SERVICIO");
                final String SUBPRODUCTO = tabla.getChildTextTrim("SUBPRODUCTO");
                final String CIUDAD = tabla.getChildTextTrim("CIUDAD");
                final Abonado abonado = new Abonado(id_solicitud, NOMBRE_COMPLETO, IMEI, IMSI, ICCID, IDENTIFICACION, DIRECCION_COMPLETA, TELEFONO1, FECHA_INICIO, FECHA_FIN, ID_SERVICIO, SUBPRODUCTO, CIUDAD, (String)null, (String)null, hoja);
                datos.add(abonado);
            }
            insertaAbonado(datos);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void procesaAbonadosCed(final String id_solicitud, final int id_Transaccion, final String valor, String fechaIni, String fechaFin, final String hoja) throws Exception {
        final String proceso = "Procesa Abonados Ced";
        Date auxDate = null;
        try {
            if (fechaIni == null || fechaIni.equals("0")) {
                fechaIni = "";
            }
            if (fechaFin == null || fechaFin.equals("0")) {
                fechaFin = "";
            }
            if (!fechaIni.equals("")) {
                auxDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaIni);
                fechaIni = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(auxDate);
            }
            if (!fechaFin.equals("")) {
                auxDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaFin);
                fechaFin = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(auxDate);
            }
            final Properties props = new Propiedades().getPropiedades();
            final String dataSource = props.getProperty("DataSourceEISAXIS");
            final String eisWsUrl = props.getProperty("eisWsUrl");
            final BigDecimal idTransaccion = new BigDecimal(String.valueOf(id_Transaccion));
            final URL url = new URL(eisWsUrl);
            final EisSoapHttpStub ws = new EisSoapHttpStub(url, (Service)null);
            final EipConsumeServicioElement parameters = new EipConsumeServicioElement();
            EipConsumeServicioResponseElement response = null;
            EipConsumeServicio_Out resultado = new EipConsumeServicio_Out();
            parameters.setDsId(dataSource);
            parameters.setPnIdServicioInformacion(idTransaccion);
            parameters.setPvParametroBind1(valor);
            parameters.setPvParametroBind2(fechaIni);
            parameters.setPvParametroBind3(fechaFin);
            parameters.setPvParametroBind4("?");
            parameters.setPvParametroBind5("?");
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros EIS Abonado:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " idTransaccion -->" + idTransaccion));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " valor -->" + valor));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " fechaIni -->" + fechaIni));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " fechaFin -->" + fechaFin));
            response = ws.eipConsumeServicio(parameters);
            resultado = response.getResult();
            resultado = response.getResult();
            if (resultado.getPnerrorOut() != 0) {
                throw new Exception(resultado.getPverrorOut());
            }
            String result = resultado.getPvresultadoOut();
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Resultado EISAbonado:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + result));
            final List<Abonado> datos = new ArrayList<Abonado>();
            if (result == null || result.isEmpty() || result == "") {
                final Abonado abonado = new Abonado(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, hoja);
                datos.add(abonado);
                insertaAbonado(datos);
                return;
            }
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            final List<String> numActivos = new ArrayList<String>();
            for (int i = 0; i < list.size(); ++i) {
                final Element tabla = (Element) list.get(i);
                final String NOMBRE_COMPLETO = tabla.getChildTextTrim("NOMBRE_COMPLETO");
                final String IMEI = tabla.getChildTextTrim("IMEI");
                final String IMSI = tabla.getChildTextTrim("IMSI");
                final String ICCID = tabla.getChildTextTrim("ICCID");
                final String IDENTIFICACION = tabla.getChildTextTrim("IDENTIFICACION");
                final String DIRECCION_COMPLETA = tabla.getChildTextTrim("DIRECCION_COMPLETA");
                final String TELEFONO1 = tabla.getChildTextTrim("TELEFONO1");
                final String FECHA_INICIO = tabla.getChildTextTrim("FECHA_INICIO");
                final String FECHA_FIN = tabla.getChildTextTrim("FECHA_FIN");
                final String ID_SERVICIO = tabla.getChildTextTrim("ID_SERVICIO");
                final String SUBPRODUCTO = tabla.getChildTextTrim("SUBPRODUCTO");
                final String CIUDAD = tabla.getChildTextTrim("CIUDAD");
                final Abonado abonado = new Abonado(id_solicitud, NOMBRE_COMPLETO, IMEI, IMSI, ICCID, IDENTIFICACION, DIRECCION_COMPLETA, TELEFONO1, FECHA_INICIO, FECHA_FIN, ID_SERVICIO, SUBPRODUCTO, CIUDAD, (String)null, (String)null, hoja);
                datos.add(abonado);
                if (FECHA_FIN == null || FECHA_FIN.isEmpty()) {
                    numActivos.add(ID_SERVICIO);
                }
            }
            insertaAbonado(datos);
            if (numActivos != null && numActivos.size() != 0 && !numActivos.isEmpty()) {
                actualizaImeiBdLocal(id_solicitud, numActivos);
            }
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void procesaAbonadosV2(final String id_solicitud, final int id_Transaccion, final String tramaValor, String fechaIni, String fechaFin, final String descTramaValor, final String hoja) throws Exception {
        final String proceso = "Procesa Abonados";
        Date auxDate = null;
        try {
            if (fechaIni == null || fechaIni.equals("0")) {
                fechaIni = "";
            }
            if (fechaFin == null || fechaFin.equals("0")) {
                fechaFin = "";
            }
            if (!fechaIni.equals("")) {
                auxDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaIni);
                fechaIni = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(auxDate);
            }
            if (!fechaFin.equals("")) {
                auxDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaFin);
                fechaFin = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(auxDate);
            }
            final Properties props = new Propiedades().getPropiedades();
            final String dataSource = props.getProperty("DataSourceEISAXIS");
            final String eisWsUrl = props.getProperty("eisWsUrl");
            final BigDecimal idTransaccion = new BigDecimal(String.valueOf(id_Transaccion));
            final URL url = new URL(eisWsUrl);
            final EisSoapHttpStub ws = new EisSoapHttpStub(url, (Service)null);
            final EipConsumeServicioElement parameters = new EipConsumeServicioElement();
            EipConsumeServicioResponseElement response = null;
            EipConsumeServicio_Out resultado = new EipConsumeServicio_Out();
            parameters.setDsId(dataSource);
            parameters.setPnIdServicioInformacion(idTransaccion);
            parameters.setPvParametroBind1(fechaIni);
            parameters.setPvParametroBind2(fechaFin);
            parameters.setPvParametroBind3("?");
            parameters.setPvParametroBind4("?");
            parameters.setPvParametroBind5(tramaValor);
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros EIS AbonadoV2:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " idTransaccion -->" + idTransaccion));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " fechaIni -->" + fechaIni));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " fechaFin -->" + fechaFin));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " valorIn -->" + tramaValor));
            response = ws.eipConsumeServicio(parameters);
            resultado = response.getResult();
            if (resultado.getPnerrorOut() != 0) {
                throw new Exception(resultado.getPverrorOut());
            }
            final List<Abonado> datos = new ArrayList<Abonado>();
            if (descTramaValor != null) {
                final Abonado abonado = new Abonado(id_solicitud, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, descTramaValor, tramaValor.replace("'", ""), hoja);
                datos.add(abonado);
                insertaAbonado(datos);
                datos.clear();
            }
            String result = resultado.getPvresultadoOut();
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Resultado EISAbonado:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + result));
            if (result == null || result.isEmpty() || result == "") {
                final Abonado abonado = new Abonado(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, hoja);
                datos.add(abonado);
                insertaAbonado(datos);
                return;
            }
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            for (int i = 0; i < list.size(); ++i) {
                final Element tabla = (Element) list.get(i);
                final String NOMBRE_COMPLETO = tabla.getChildTextTrim("NOMBRE_COMPLETO");
                final String IMEI = tabla.getChildTextTrim("IMEI");
                final String IMSI = tabla.getChildTextTrim("IMSI");
                final String ICCID = tabla.getChildTextTrim("ICCID");
                final String IDENTIFICACION = tabla.getChildTextTrim("IDENTIFICACION");
                final String DIRECCION_COMPLETA = tabla.getChildTextTrim("DIRECCION_COMPLETA");
                final String TELEFONO1 = tabla.getChildTextTrim("TELEFONO1");
                final String FECHA_INICIO = tabla.getChildTextTrim("FECHA_INICIO");
                final String FECHA_FIN = tabla.getChildTextTrim("FECHA_FIN");
                final String ID_SERVICIO = tabla.getChildTextTrim("ID_SERVICIO");
                final String SUBPRODUCTO = tabla.getChildTextTrim("SUBPRODUCTO");
                final String CIUDAD = tabla.getChildTextTrim("CIUDAD");
                final String MIGRACION = tabla.getChildTextTrim("MIGRACION");
                final Abonado abonado = new Abonado(id_solicitud, NOMBRE_COMPLETO, IMEI, IMSI, ICCID, IDENTIFICACION, DIRECCION_COMPLETA, TELEFONO1, FECHA_INICIO, FECHA_FIN, ID_SERVICIO, SUBPRODUCTO, CIUDAD, (String)null, (String)null, hoja, MIGRACION);
                datos.add(abonado);
            }
            insertaAbonadoAx(datos);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void procesaAbonadosV3(final String id_solicitud, final int id_Transaccion, final String tramaValor, String fechaIni, String fechaFin, final String descTramaValor, final String hoja) throws Exception {
        final String proceso = "Procesa Abonados";
        Date auxDate = null;
        try {
            if (fechaIni == null || fechaIni.equals("0")) {
                fechaIni = "";
            }
            if (fechaFin == null || fechaFin.equals("0")) {
                fechaFin = "";
            }
            if (!fechaIni.equals("")) {
                auxDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaIni);
                fechaIni = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(auxDate);
            }
            if (!fechaFin.equals("")) {
                auxDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaFin);
                fechaFin = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(auxDate);
            }
            final Properties props = new Propiedades().getPropiedades();
            final String dataSource = props.getProperty("DataSourceEISAXIS");
            final String eisWsUrl = props.getProperty("eisWsUrl");
            final BigDecimal idTransaccion = new BigDecimal(String.valueOf(id_Transaccion));
            final URL url = new URL(eisWsUrl);
            final EisSoapHttpStub ws = new EisSoapHttpStub(url, (Service)null);
            final EipConsumeServicioElement parameters = new EipConsumeServicioElement();
            EipConsumeServicioResponseElement response = null;
            EipConsumeServicio_Out resultado = new EipConsumeServicio_Out();
            parameters.setDsId(dataSource);
            parameters.setPnIdServicioInformacion(idTransaccion);
            parameters.setPvParametroBind1(fechaIni);
            parameters.setPvParametroBind2(fechaFin);
            parameters.setPvParametroBind3("?");
            parameters.setPvParametroBind4("?");
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros EIS AbonadoV3:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " idTransaccion -->" + idTransaccion));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " fechaIni -->" + fechaIni));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " fechaFin -->" + fechaFin));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " valorIn -->" + tramaValor));
            response = ws.eipConsumeServicio(parameters);
            final int cantidadCaracteres = 5000;
            String result = "";
            String resultadoAux = "";
            if (tramaValor.length() >= cantidadCaracteres) {
                List<String> list = new ArrayList<String>();
                list = Cadena(tramaValor, cantidadCaracteres);
                for (final String bloqueCaracteres : list) {
                    parameters.setPvParametroBind5(bloqueCaracteres);
                    response = ws.eipConsumeServicio(parameters);
                    resultado = response.getResult();
                    if (resultado.getPnerrorOut() != 0) {
                        throw new Exception(resultado.getPverrorOut());
                    }
                    resultadoAux = resultado.getPvresultadoOut();
                    result = String.valueOf(result) + resultadoAux;
                }
            }
            else {
                parameters.setPvParametroBind5(tramaValor);
                response = ws.eipConsumeServicio(parameters);
                resultado = response.getResult();
                if (resultado.getPnerrorOut() != 0) {
                    throw new Exception(resultado.getPverrorOut());
                }
                result = resultado.getPvresultadoOut();
            }
            final List<Abonado> datos = new ArrayList<Abonado>();
            if (descTramaValor != null) {
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " longitud trama valor: " + tramaValor.length()));
                final int valorPorExtraer = 3000;
                if (tramaValor.length() > valorPorExtraer) {
                    List<String> list2 = new ArrayList<String>();
                    list2 = Cadena(tramaValor, valorPorExtraer);
                    for (final String caracteres : list2) {
                        System.out.println("cadena: " + caracteres);
                        final Abonado abonado = new Abonado(id_solicitud, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, descTramaValor, caracteres.replace("'", ""), hoja);
                        datos.add(abonado);
                    }
                }
                else {
                    final Abonado abonado = new Abonado(id_solicitud, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, descTramaValor, tramaValor.replace("'", ""), hoja);
                    datos.add(abonado);
                }
                insertaAbonado(datos);
                datos.clear();
            }
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Resultado EISAbonado:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + result));
            if (result == null || result.isEmpty() || result == "") {
                final Abonado abonado = new Abonado(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, hoja);
                datos.add(abonado);
                insertaAbonado(datos);
                return;
            }
            result = result.replace("<DATOS>", "");
            result = result.replace("</DATOS>", "");
            result = "<DATOS>" + result + "</DATOS>";
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list3 = docu.getRootElement().getChildren();
            final List<String> numActivos = new ArrayList<String>();
            for (int i = 0; i < list3.size(); ++i) {
                final Element tabla = (Element) list3.get(i);
                final String NOMBRE_COMPLETO = tabla.getChildTextTrim("NOMBRE_COMPLETO");
                final String IMEI = tabla.getChildTextTrim("IMEI");
                final String IMSI = tabla.getChildTextTrim("IMSI");
                final String ICCID = tabla.getChildTextTrim("ICCID");
                final String IDENTIFICACION = tabla.getChildTextTrim("IDENTIFICACION");
                final String DIRECCION_COMPLETA = tabla.getChildTextTrim("DIRECCION_COMPLETA");
                final String TELEFONO1 = tabla.getChildTextTrim("TELEFONO1");
                final String FECHA_INICIO = tabla.getChildTextTrim("FECHA_INICIO");
                final String FECHA_FIN = tabla.getChildTextTrim("FECHA_FIN");
                final String ID_SERVICIO = tabla.getChildTextTrim("ID_SERVICIO");
                final String SUBPRODUCTO = tabla.getChildTextTrim("SUBPRODUCTO");
                final String CIUDAD = tabla.getChildTextTrim("CIUDAD");
                final Abonado abonado = new Abonado(id_solicitud, NOMBRE_COMPLETO, IMEI, IMSI, ICCID, IDENTIFICACION, DIRECCION_COMPLETA, TELEFONO1, FECHA_INICIO, FECHA_FIN, ID_SERVICIO, SUBPRODUCTO, CIUDAD, (String)null, (String)null, hoja);
                datos.add(abonado);
                if (FECHA_FIN == null || FECHA_FIN.isEmpty()) {
                    numActivos.add(ID_SERVICIO);
                }
            }
            insertaAbonado(datos);
            if (numActivos != null && numActivos.size() != 0 && !numActivos.isEmpty()) {
                actualizaImeiBdLocal(id_solicitud, numActivos);
            }
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void actualizaImeiBdLocal(final String idSolicitud, final List<String> numActivos) throws Exception {
        final String proceso = "Actualiza Imei Bd Local";
        ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Actualizar BD local con imei del vlr"));
        final String in = "";
        boolean flag = true;
        final String nombreVista = "FIS_DATOS_AIC_" + idSolicitud;
        final String queryDatos = "select distinct(SUBSTR (imei,1,14)) valor from " + nombreVista + " where msisdn =";
        final String queryPeriodoimei = "select SUBSTR (periodo,1,4)||'-'||SUBSTR (periodo,5,2) imei_periodo from(select DISTINCT(SUBSTR (TABLA_DIARIA,11,6)) periodo from " + nombreVista + " WHERE IMEI = ? AND MSISDN = ?)";
        final String queryUpdateImeiReal = "update fis_datos_solicitud set campo_2=? where id_solicitud=? and campo_8=? and campo_10 is null";
        final String queryUpdatePeriodoImei = "update fis_datos_solicitud set campo_13=? where id_solicitud=? and campo_8=? and campo_10 is null";
        final ConexionOpr cnx = new ConexionOpr();
        final ConexionO cnxo = new ConexionO();
        final Connection conVlr = cnx.conectar().getConexion();
        final Connection conBdlocal = cnxo.conectar().getConexion();
        int n = 0;
        try {
            String imeisVlr = "";
            String imeisToAxis = "";
            String sAux = "";
            conBdlocal.setAutoCommit(false);
            for (final String num : numActivos) {
                ++n;
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Extrayendo imeis desde vlr del numero:" + num));
                String periodoImei = "";
                String numTmp = num;
                final int lenght = numTmp.length();
                numTmp = numTmp.substring(lenght - 8);
                numTmp = "'593" + numTmp + "'";
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " numero tmp:" + numTmp));
                PreparedStatement pst = conVlr.prepareStatement(String.valueOf(queryDatos) + numTmp);
                ResultSet rs = pst.executeQuery();
                flag = true;
                while (rs.next()) {
                    if (flag) {
                        imeisToAxis = String.valueOf(rs.getString("valor")) + DigitoVerificador.obtieneDigitoVerificador(rs.getString("valor"));
                        imeisVlr = rs.getString("valor");
                        flag = false;
                    }
                    else {
                        sAux = String.valueOf(rs.getString("valor")) + DigitoVerificador.obtieneDigitoVerificador(rs.getString("valor"));
                        imeisToAxis = String.valueOf(imeisToAxis) + ",\n" + sAux;
                        imeisVlr = String.valueOf(imeisVlr) + "," + rs.getString("valor");
                    }
                }
                rs.close();
                pst.close();
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Imeis resultantes vlr del numero:" + num));
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " " + imeisToAxis));
                if (imeisVlr != null && !imeisVlr.isEmpty()) {
                    ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Obteniendo Periodo de ocurrencia por cada imei resultante del numero " + num + "..."));
                    boolean tmpflag = true;
                    final String[] imei = imeisVlr.split(",");
                    String[] array;
                    for (int length = (array = imei).length, i = 0; i < length; ++i) {
                        final String im = array[i];
                        pst = conVlr.prepareStatement(queryPeriodoimei);
                        pst.setString(1, String.valueOf(im) + "0");
                        pst.setString(2, numTmp.replace("'", ""));
                        rs = pst.executeQuery();
                        while (rs.next()) {
                            if (tmpflag) {
                                periodoImei = String.valueOf(im) + DigitoVerificador.obtieneDigitoVerificador(im) + " --> " + rs.getString("imei_periodo");
                                tmpflag = false;
                            }
                            else {
                                periodoImei = String.valueOf(periodoImei) + "\n" + im + DigitoVerificador.obtieneDigitoVerificador(im) + " --> " + rs.getString("imei_periodo");
                            }
                        }
                        ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Imei - Periodo"));
                        ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " " + periodoImei));
                        rs.close();
                        pst.close();
                    }
                }
                else {
                    imeisToAxis = (periodoImei = "Imei no registra trafica en la Red");
                }
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Numero " + num + " Actualizando BD local con imeis devueltos desde VLR.."));
                pst = conBdlocal.prepareStatement(queryUpdateImeiReal);
                pst.setString(1, imeisToAxis);
                pst.setString(2, idSolicitud);
                pst.setString(3, num);
                pst.executeUpdate();
                pst.close();
                pst = conBdlocal.prepareStatement(queryUpdatePeriodoImei);
                pst.setString(1, periodoImei);
                pst.setString(2, idSolicitud);
                pst.setString(3, num);
                pst.executeUpdate();
                pst.close();
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Numero " + num + " actualizado!"));
                if (n == 100) {
                    n = 0;
                    conBdlocal.commit();
                }
            }
            conBdlocal.commit();
            ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " imeis actualizados correctamente!"));
        }
        catch (Exception e) {
            conBdlocal.rollback();
            ConsumoWs.log.error((Object)("solicitud: " + idSolicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
        finally {
            try {
                if (cnxo.getConexion() != null && !cnxo.getConexion().isClosed()) {
                    cnxo.getConexion().close();
                }
                if (cnx.getConexion() != null && !cnx.getConexion().isClosed()) {
                    cnx.getConexion().close();
                }
            }
            catch (Exception e2) {
                ConsumoWs.log.fatal((Object)("Cerrar Conexion: " + e2.toString()), (Throwable)e2);
            }
        }
        try {
            if (cnxo.getConexion() != null && !cnxo.getConexion().isClosed()) {
                cnxo.getConexion().close();
            }
            if (cnx.getConexion() != null && !cnx.getConexion().isClosed()) {
                cnx.getConexion().close();
            }
        }
        catch (Exception e2) {
            ConsumoWs.log.fatal((Object)("Cerrar Conexion: " + e2.toString()), (Throwable)e2);
        }
    }
    
    public static void insertaAbonado(final List<Abonado> listaAbonado) throws Exception {
        final String proceso = "Inserta Abonado";
        final ConexionO cnx = new ConexionO();
        final Connection con = cnx.conectar().getConexion();
        PreparedStatement cst = null;
        int n = 0;
        try {
            con.setAutoCommit(false);
            for (int i = 0; i < listaAbonado.size(); ++i) {
                ++n;
                cst = con.prepareStatement("INSERT INTO FIS_DATOS_SOLICITUD(ID_DATO_SOLICITUD,ID_SOLICITUD,CAMPO_1,CAMPO_2,CAMPO_3,CAMPO_4,CAMPO_5,CAMPO_6,CAMPO_7,CAMPO_8,CAMPO_9,CAMPO_10,CAMPO_11,CAMPO_12,CAMPO_13,CAMPO_14,CAMPO_15,CAMPO_16,CAMPO_17,CAMPO_18,CAMPO_19,CAMPO_20,CAMPO_21,CAMPO_22,CAMPO_23,CAMPO_24,CAMPO_25,CAMPO_26,CAMPO_27,CAMPO_28,CAMPO_29,CAMPO_30) VALUES (SEQ_FIS_DATOS_SOLICITUD.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                cst.setLong(1, Long.valueOf(listaAbonado.get(i).getID_SOLICITUD()));
                cst.setString(2, listaAbonado.get(i).getNOMBRE_COMPLETO());
                cst.setString(3, listaAbonado.get(i).getIMEI());
                cst.setString(4, listaAbonado.get(i).getIMSI());
                cst.setString(5, listaAbonado.get(i).getICCID());
                cst.setString(6, listaAbonado.get(i).getIDENTIFICACION());
                cst.setString(7, listaAbonado.get(i).getDIRECCION_COMPLETA());
                cst.setString(8, listaAbonado.get(i).getTELEFONO1());
                cst.setString(9, listaAbonado.get(i).getID_SERVICIO());
                cst.setString(10, listaAbonado.get(i).getFECHA_INICIO());
                cst.setString(11, listaAbonado.get(i).getFECHA_FIN());
                cst.setString(12, listaAbonado.get(i).getSUBPRODUCTO());
                cst.setString(13, listaAbonado.get(i).getCIUDAD());
                cst.setString(14, null);
                cst.setString(15, null);
                cst.setString(16, null);
                cst.setString(17, null);
                cst.setString(18, null);
                cst.setString(19, null);
                cst.setString(20, null);
                cst.setString(21, null);
                cst.setString(22, null);
                cst.setString(23, null);
                cst.setString(24, null);
                cst.setString(25, null);
                cst.setString(26, null);
                cst.setString(27, null);
                cst.setString(28, null);
                cst.setString(29, listaAbonado.get(i).getDESCRIP_ADICIONAL());
                cst.setString(30, listaAbonado.get(i).getDESCRIP_VALOR());
                cst.setString(31, listaAbonado.get(i).getHOJAEXCEL());
                cst.executeUpdate();
                cst.close();
                if (n == 100) {
                    n = 0;
                    con.commit();
                }
            }
            con.commit();
            con.close();
        }
        catch (Exception ex) {
            con.rollback();
            ConsumoWs.log.error((Object)("Proceso: " + proceso + ": " + ex.getMessage()));
            ConsumoWs.log.error((Object)ex.toString(), (Throwable)ex);
            throw new Exception(ex.toString());
        }
        finally {
            try {
                if (cnx.getConexion() != null && !cnx.getConexion().isClosed()) {
                    cnx.getConexion().close();
                }
            }
            catch (Exception e2) {
                ConsumoWs.log.fatal((Object)("Cerrar Conexion: " + e2.toString()), (Throwable)e2);
            }
        }
        try {
            if (cnx.getConexion() != null && !cnx.getConexion().isClosed()) {
                cnx.getConexion().close();
            }
        }
        catch (Exception e2) {
            ConsumoWs.log.fatal((Object)("Cerrar Conexion: " + e2.toString()), (Throwable)e2);
        }
    }
    
    public static void procesaSuase(final String id_solicitud, final String valor) throws Exception {
        final String proceso = "Procesa Suase";
        try {
            final Properties props = new Propiedades().getPropiedades();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            final Date fechaActual = new Date();
            final String fecha = sdf.format(fechaActual);
            sdf = new SimpleDateFormat("HH:mm");
            final String hora = sdf.format(fechaActual);
            final String bandXmlSuase = props.getProperty("bandXmlSuase");
            String result = "";
            final SAXBuilder builder = new SAXBuilder();
            Document docu = null;
            if (bandXmlSuase.equals("l")) {
                final String rutaXml = props.getProperty("xml");
                final File file = new File(rutaXml);
                docu = builder.build(file);
            }
            if (bandXmlSuase.equals("w")) {
                final String user_app = props.getProperty("user_app");
                final String clave_app = props.getProperty("clave_app");
                final String canal_app = props.getProperty("canal_app");
                final String entidad = props.getProperty("entidad");
                final String Motivo = props.getProperty("Motivo");
                final String Concesionario = props.getProperty("Concesionario");
                final String Cod_Referencia = props.getProperty("Cod_Referencia");
                final ConexionE cnx = new ConexionE();
                final Connection con = cnx.conectar().getConexion();
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + fecha + " " + hora + " Conexion Espejo establecida!"));
                final CallableStatement cstmt = con.prepareCall("{? = call Pck_Api_Suase_Adm.Pcf_Ingresa_Solicitud_Web(?,?,?,?,?,?,?,?,?,?,?,?)}");
                cstmt.registerOutParameter(1, 12);
                cstmt.setString(2, user_app);
                cstmt.setString(3, clave_app);
                cstmt.setString(4, canal_app);
                cstmt.setString(5, entidad);
                cstmt.setString(6, Motivo);
                cstmt.setString(7, Concesionario);
                cstmt.setString(8, valor);
                cstmt.setString(9, fecha);
                cstmt.setString(10, hora);
                cstmt.setString(11, Cod_Referencia);
                cstmt.setString(12, null);
                cstmt.setString(13, null);
                cstmt.executeUpdate();
                result = cstmt.getString(1);
                cstmt.close();
                con.close();
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Resultado Pck_Api_Suase_Adm.Pcf_Ingresa_Solicitud_Web:"));
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + result));
                final Reader stringReader = new StringReader(result);
                docu = builder.build(stringReader);
            }
            if (bandXmlSuase.equals("ws")) {
                final String user_app = props.getProperty("user_app");
                final String clave_app = props.getProperty("clave_app");
                final String canal_app = props.getProperty("canal_app");
                final String entidad = props.getProperty("entidad");
                final String Motivo = props.getProperty("Motivo");
                final String Concesionario = props.getProperty("Concesionario");
                final String Cod_Referencia = props.getProperty("Cod_Referencia");
                final String suaseUrl = props.getProperty("suaseUrl");
                final URL url = new URL(suaseUrl);
                final WS_SUASE_WEBPortBindingStub ws = new WS_SUASE_WEBPortBindingStub(url, (Service)null);
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros de ingreso Ws Suase:"));
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " user_app --> " + user_app));
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " clave_app --> " + clave_app));
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " canal_app --> " + canal_app));
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " entidad --> " + entidad));
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Motivo --> " + Motivo));
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Concesionario --> " + Concesionario));
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " NumeroCelular --> " + valor));
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Fecha --> " + fecha));
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " hora --> " + hora));
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Cod_Referencia --> " + Cod_Referencia));
                result = ws.ingresarSolicitudWeb(user_app, clave_app, canal_app, entidad, Motivo, Concesionario, valor, fecha, hora, Cod_Referencia);
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Resultado Ws Suase:"));
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + result));
                result = result.replaceAll("&", "&amp;");
                final Reader stringReader2 = new StringReader(result);
                docu = builder.build(stringReader2);
            }
            final List<Suase> datos = new ArrayList<Suase>();
            final Element tabla = docu.getRootElement();
            final String idRespuesta = tabla.getChildTextTrim("id_respuesta");
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " IDRespuesta: " + idRespuesta));
            if (idRespuesta != null) {
                final Suase suase = new Suase(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, "S");
                datos.add(suase);
                insertaSuase(datos);
                return;
            }
            final String nombre_completo = tabla.getChildTextTrim("nombre_completo");
            final String provincia = tabla.getChildTextTrim("provincia");
            final String cantonCiudad = tabla.getChildTextTrim("cantonCiudad");
            final String direccion = tabla.getChildTextTrim("direccion");
            final String nombreCelda = tabla.getChildTextTrim("celda");
            final String celda = tabla.getChildTextTrim("idCelda");
            String longitud = tabla.getChildTextTrim("longitud");
            String latitud = tabla.getChildTextTrim("latitud");
            final String azimut = tabla.getChildTextTrim("azimut");
            longitud = caracteresLatitudlongitud(longitud);
            latitud = caracteresLatitudlongitud(latitud);
            final Suase suase = new Suase(id_solicitud, nombre_completo, provincia, cantonCiudad, direccion, celda, nombreCelda, longitud, latitud, azimut, "S");
            datos.add(suase);
            insertaSuase(datos);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    private static String caracteresLatitudlongitud(final String coordenadas) {
        String valor = null;
        if (coordenadas == null || coordenadas.isEmpty()) {
            return coordenadas;
        }
        try {
            final String[] vector = coordenadas.split(" ");
            if (vector.length < 3 || vector.length > 4) {
                return coordenadas;
            }
            String grado = vector[0];
            String comillaSimple = vector[1];
            String comillaDoble = vector[2];
            String posicion = "";
            if (vector.length > 3) {
                posicion = vector[3];
            }
            String ultimoCaracter = grado.substring(grado.length() - 1);
            if (ultimoCaracter.matches("^[0-9]$")) {
                grado = String.valueOf(grado) + "�";
            }
            ultimoCaracter = comillaSimple.substring(comillaSimple.length() - 1);
            if (ultimoCaracter.matches("^[0-9]$")) {
                comillaSimple = String.valueOf(comillaSimple) + "'";
            }
            if (posicion == null || posicion.isEmpty()) {
                ultimoCaracter = comillaDoble.substring(comillaDoble.length() - 1);
                if (ultimoCaracter.matches("^[a-zA-Z0-9]$")) {
                    comillaDoble = String.valueOf(comillaDoble.substring(0, comillaDoble.length() - 1)) + "\"" + comillaDoble.substring(comillaDoble.length() - 1);
                }
            }
            else {
                ultimoCaracter = comillaDoble.substring(comillaDoble.length() - 1, comillaDoble.length());
                if (ultimoCaracter.matches("^[0-9]$")) {
                    comillaDoble = String.valueOf(comillaDoble) + "\"";
                }
            }
            valor = String.valueOf(grado) + " " + comillaSimple + " " + comillaDoble + " " + posicion;
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            return coordenadas;
        }
        return valor;
    }
    
    public static void procesaGeolocalizador(final String id_solicitud, final String valor) throws Exception {
        final String proceso = "Procesa Geolocalizador";
        try {
            final Properties props = new Propiedades().getPropiedades();
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            final Date fechaActual = new Date();
            final String fecha = sdf.format(fechaActual);
            String codRespGL = "";
            final ConexionO cnx = new ConexionO();
            String result = "";
            final SAXBuilder builder = new SAXBuilder();
            Document docu = null;
            final String institucionGl = props.getProperty("institucionGl");
            final String usuarioGl = props.getProperty("usuarioGl");
            final String passGl = props.getProperty("passGl");
            final String motivoGl = props.getProperty("motivoGl");
            final String descripcionGl = props.getProperty("descripcionGl");
            final String UrlGeolocalizador = props.getProperty("urlGelocalizador");
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros de ingreso Geolocalizador:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " usuarioGl --> " + usuarioGl));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " passGl --> " + passGl));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " motivoGl --> " + motivoGl));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " descripcionGl --> " + descripcionGl));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " institucionGl --> " + institucionGl));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " NumeroCelular --> " + valor));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Fecha --> " + fecha));
            final String xmlParametros = "<Numero>" + valor + "</Numero>" + "<Institucion>" + institucionGl + "</Institucion>" + "<Usuario>" + usuarioGl + "</Usuario>" + "<Contrasenia>" + passGl + "</Contrasenia>" + "<Fecha>" + fecha + "</Fecha>" + "<Motivo>" + motivoGl + "</Motivo>" + "<Descripcion>" + descripcionGl + "</Descripcion>";
            final String xmlRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mom=\"http://mom/\"><soapenv:Header/><soapenv:Body><dat:ConsultaLocalizacionWeb>" + xmlParametros + "</dat:ConsultaLocalizacionWeb>" + "</soapenv:Body>" + "</soapenv:Envelope>";
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Xml de entrada-->" + xmlRequest));
            result = ConsumeServlet.consumePost(UrlGeolocalizador, xmlRequest);
            result = result.replaceAll("&", "&amp;");
            final Reader stringReader = new StringReader(result);
            docu = builder.build(stringReader);
            final List<Suase> datos = new ArrayList<Suase>();
            final Element tabla = docu.getRootElement();
            final String CodigoResultado = tabla.getChildTextTrim("CodigoResultado");
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " CodigoResultado: " + CodigoResultado));
            final Connection con = cnx.conectar().getConexion();
            final String usrDB = props.getProperty("userRbaseTmp");
            final PreparedStatement pst = con.prepareStatement("select descripcion from " + usrDB + ".fis_respuesta_geolocalizador where cod_respuesta=" + CodigoResultado);
            final ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                codRespGL = rs.getString("descripcion");
            }
            rs.close();
            pst.close();
            con.close();
            if (CodigoResultado.equals("8") || CodigoResultado.equals("9") || CodigoResultado.equals("-1")) {
                throw new Exception("Error Geolocalizador --> " + codRespGL);
            }
            if (CodigoResultado.equals("1") || CodigoResultado.equals("2")) {
                final Suase suase = new Suase(id_solicitud, codRespGL, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null);
                datos.add(suase);
                insertaSuase(datos);
                return;
            }
            if (CodigoResultado.equals("3") || CodigoResultado.equals("5")) {
                final Suase suase = new Suase(id_solicitud, (String)null, (String)null, (String)null, codRespGL, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null);
                datos.add(suase);
                insertaSuase(datos);
                return;
            }
            if (CodigoResultado.equals("6")) {
                final Suase suase = new Suase(id_solicitud, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null);
                datos.add(suase);
                insertaSuase(datos);
                return;
            }
            final String nombre_completo = tabla.getChildTextTrim("propietario");
            final String provincia = tabla.getChildTextTrim("rbsProvincia");
            final String cantonCiudad = tabla.getChildTextTrim("rbsCiudad");
            final String direccion = tabla.getChildTextTrim("rbsDireccion");
            final String nombreCelda = tabla.getChildTextTrim("rbsCelda");
            final String celda;
            final String id_celda = celda = "";
            final String longitud = tabla.getChildTextTrim("rbsLongitud");
            final String latitud = tabla.getChildTextTrim("rbsLatitud");
            final String azimut = tabla.getChildTextTrim("rbsAzimut");
            final Suase suase = new Suase(id_solicitud, nombre_completo, provincia, cantonCiudad, direccion, celda, nombreCelda, longitud, latitud, azimut, (String)null);
            datos.add(suase);
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Nombre --->" + nombre_completo));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " provincia --->" + provincia));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " cantonCiudad --->" + cantonCiudad));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " direccion --->" + direccion));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " nombreCelda --->" + nombreCelda));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " celda --->" + celda));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " longitud --->" + longitud));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " latitud --->" + latitud));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " azimut --->" + azimut));
            insertaSuase(datos);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void insertaSuase(final List<Suase> lista) throws Exception {
        final String proceso = "Inserta Suase";
        final ConexionO cnx = new ConexionO();
        final Connection con = cnx.conectar().getConexion();
        final Properties props = new Propiedades().getPropiedades();
        try {
            for (int i = 0; i < lista.size(); ++i) {
                final Suase suase = lista.get(i);
                boolean lb_rna = false;
                String ls_rna = "";
                lb_rna = isNumeric(suase.getCelda());
                final String banWSGeo = props.getProperty("bandWSGeoLocali");
                if (banWSGeo.equalsIgnoreCase("S") && !suase.getNombre_completo().equals("N/A")) {
                    if (lb_rna) {
                        ls_rna = "MOVISTAR";
                    }
                    else {
                        ls_rna = "CLARO";
                    }
                }
                else {
                    ls_rna = "";
                }
                final CallableStatement cst = con.prepareCall("{call FISK_API_PORTAL_SEGURIDAD.FISP_INGRESO_DATOS_SOLICITUDES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                cst.setLong(1, Long.valueOf(suase.getId_solicitud()));
                cst.setString(2, suase.getNombre_completo());
                cst.setString(3, suase.getProvincia());
                cst.setString(4, suase.getCantonCiudad());
                cst.setString(5, suase.getDireccion());
                cst.setString(6, suase.getCelda());
                cst.setString(7, suase.getNombreCelda());
                cst.setString(8, suase.getLongitud());
                cst.setString(9, suase.getLatitud());
                cst.setString(10, suase.getAzimut());
                cst.setString(11, ls_rna);
                cst.setString(12, null);
                cst.setString(13, null);
                cst.setString(14, null);
                cst.setString(15, null);
                cst.setString(16, null);
                cst.setString(17, null);
                cst.setString(18, null);
                cst.setString(19, null);
                cst.setString(20, null);
                cst.setString(21, null);
                cst.setString(22, null);
                cst.setString(23, null);
                cst.setString(24, null);
                cst.setString(25, null);
                cst.setString(26, null);
                cst.setString(27, null);
                cst.setString(28, null);
                cst.setString(29, null);
                cst.setString(30, null);
                cst.setString(31, null);
                cst.registerOutParameter(32, 12);
                cst.registerOutParameter(33, 12);
                cst.executeUpdate();
                final String codError = cst.getString(32);
                final String msjError = cst.getString(33);
                cst.close();
                if (!codError.equals("0")) {
                    throw new Exception(msjError);
                }
            }
            con.close();
        }
        catch (Exception ex) {
            con.rollback();
            ConsumoWs.log.error((Object)("Proceso: " + proceso + ": " + ex.toString()), (Throwable)ex);
            throw new Exception(ex.toString());
        }
    }
    
    public static void procesaRadioBase(final String id_solicitud, final int id_Transaccion, final String valor) throws Exception {
        final String proceso = "Procesa RadioBase";
        try {
            final Properties props = new Propiedades().getPropiedades();
            final String dataSource = props.getProperty("DataSourseEISOPER");
            final String eisWsUrl = props.getProperty("eisWsUrl");
            final String idTipoParametro = props.getProperty("IdTipoParametro");
            String bandRbsa = null;
            final String queryBandgvParam = "select valor from gv_parametros where id_tipo_parametro =" + idTipoParametro + " and id_parametro = 'GV_FIS_BANDERA_CARGA'";
            final ConexionO cnx = new ConexionO();
            final Connection con = cnx.conectar().getConexion();
            final Statement st = con.createStatement();
            final ResultSet rs = st.executeQuery(queryBandgvParam);
            if (!rs.next()) {
                rs.close();
                st.close();
                con.close();
                throw new Exception("gv_parametros no se encuentra o No esta configurada la bandera de carga: GV_FIS_BANDERA_CARGA");
            }
            bandRbsa = rs.getString("valor");
            rs.close();
            st.close();
            con.close();
            String nombreTabla;
            if (bandRbsa.equalsIgnoreCase("N")) {
                final String usertabla = props.getProperty("userRbase");
                nombreTabla = String.valueOf(usertabla) + "Fis_radiobases";
            }
            else {
                final String usertabla = props.getProperty("userRbaseTmp");
                nombreTabla = String.valueOf(usertabla) + "Fis_radiobases_tmp";
            }
            final BigDecimal idTransaccion = new BigDecimal(String.valueOf(id_Transaccion));
            final URL url = new URL(eisWsUrl);
            final EisSoapHttpStub ws = new EisSoapHttpStub(url, (Service)null);
            final EipConsumeServicioElement parameters = new EipConsumeServicioElement();
            EipConsumeServicioResponseElement response = null;
            EipConsumeServicio_Out resultado = new EipConsumeServicio_Out();
            parameters.setDsId(dataSource);
            parameters.setPnIdServicioInformacion(idTransaccion);
            parameters.setPvParametroBind1(valor);
            parameters.setPvParametroBind2("?");
            parameters.setPvParametroBind3("?");
            parameters.setPvParametroBind4("?");
            parameters.setPvParametroBind5(nombreTabla);
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros de ingreso EISUbicacionRadioBase:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " idTransaccion -->" + idTransaccion));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " ParametroBind1 --> " + valor));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " ParametroBind2 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " ParametroBind3 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " ParametroBind4 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " ParametroBind5 --> " + nombreTabla));
            response = ws.eipConsumeServicio(parameters);
            resultado = response.getResult();
            resultado = response.getResult();
            if (resultado.getPnerrorOut() != 0) {
                throw new Exception(resultado.getPverrorOut());
            }
            String result = resultado.getPvresultadoOut();
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Resultado EISUbicacionRadioBase:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + result));
            final List<RadioBase> datos = new ArrayList<RadioBase>();
            if (result == null || result.isEmpty() || result == "") {
                final RadioBase radiobase = new RadioBase(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null);
                datos.add(radiobase);
                insertaRadioBase(datos);
                return;
            }
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            for (int i = 0; i < list.size(); ++i) {
                final Element tabla = (Element) list.get(i);
                final String ID_CELDA = tabla.getChildTextTrim("ID_CELDA");
                final String PROVINCIA = tabla.getChildTextTrim("PROVINCIA");
                final String CANTON = tabla.getChildTextTrim("CANTON");
                final String CIUDAD_PARROQUIA = tabla.getChildTextTrim("CIUDAD_PARROQUIA");
                final String NOMBRE_ESTACION = tabla.getChildTextTrim("NOMBRE_ESTACION");
                final String DIRECCION = tabla.getChildTextTrim("DIRECCION");
                final String LATITUD = tabla.getChildTextTrim("LATITUD");
                final String LONGITUD = tabla.getChildTextTrim("LONGITUD");
                final RadioBase radiobase = new RadioBase(id_solicitud, ID_CELDA, PROVINCIA, CANTON, CIUDAD_PARROQUIA, NOMBRE_ESTACION, DIRECCION, LATITUD, LONGITUD);
                datos.add(radiobase);
            }
            insertaRadioBase(datos);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void procesaRadioBaseV2(final String id_solicitud, final int id_Transaccion, final String valor) throws Exception {
        final String proceso = "Procesa RadioBase";
        final String queryNombreTabla = "select object_name valor from (\t\t\t\t select object_name from user_objects where object_name like 'FIS_RADIOBASES20%' AND object_type = 'TABLE' order by created desc) where rownum = 1";
        String nombreTabla = "";
        try {
            final Properties props = new Propiedades().getPropiedades();
            final String dataSource = props.getProperty("DataSourseEISOPER");
            final String eisWsUrl = props.getProperty("eisWsUrl");
            final String usertabla = props.getProperty("userRbase");
            final ConexionO cnx = new ConexionO();
            final Connection con = cnx.conectar().getConexion();
            final Statement st = con.createStatement();
            final ResultSet rs = st.executeQuery(queryNombreTabla);
            if (!rs.next()) {
                rs.close();
                st.close();
                con.close();
                throw new Exception("No existe tabla de RadioBase!");
            }
            nombreTabla = rs.getString("valor");
            rs.close();
            st.close();
            con.close();
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Tabla radio base a consultar --> " + nombreTabla));
            final BigDecimal idTransaccion = new BigDecimal(String.valueOf(id_Transaccion));
            final URL url = new URL(eisWsUrl);
            final EisSoapHttpStub ws = new EisSoapHttpStub(url, (Service)null);
            final EipConsumeServicioElement parameters = new EipConsumeServicioElement();
            EipConsumeServicioResponseElement response = null;
            EipConsumeServicio_Out resultado = new EipConsumeServicio_Out();
            parameters.setDsId(dataSource);
            parameters.setPnIdServicioInformacion(idTransaccion);
            parameters.setPvParametroBind1(valor);
            parameters.setPvParametroBind2("?");
            parameters.setPvParametroBind3("?");
            parameters.setPvParametroBind4("?");
            parameters.setPvParametroBind5(String.valueOf(usertabla) + nombreTabla);
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros de ingreso EISUbicacionRadioBase:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " idTransaccion -->" + idTransaccion));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " ParametroBind1 --> " + valor));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " ParametroBind2 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " ParametroBind3 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " ParametroBind4 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " ParametroBind5 --> " + usertabla + nombreTabla));
            response = ws.eipConsumeServicio(parameters);
            resultado = response.getResult();
            resultado = response.getResult();
            if (resultado.getPnerrorOut() != 0) {
                throw new Exception(resultado.getPverrorOut());
            }
            String result = resultado.getPvresultadoOut();
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Resultado EISUbicacionRadioBase:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + result));
            final List<RadioBase> datos = new ArrayList<RadioBase>();
            if (result == null || result.isEmpty() || result == "") {
                final RadioBase radiobase = new RadioBase(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null);
                datos.add(radiobase);
                insertaRadioBase(datos);
                return;
            }
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            for (int i = 0; i < list.size(); ++i) {
                final Element tabla = (Element) list.get(i);
                final String ID_CELDA = tabla.getChildTextTrim("ID_CELDA");
                final String PROVINCIA = tabla.getChildTextTrim("PROVINCIA");
                final String CANTON = tabla.getChildTextTrim("CANTON");
                final String CIUDAD_PARROQUIA = tabla.getChildTextTrim("CIUDAD_PARROQUIA");
                final String NOMBRE_ESTACION = tabla.getChildTextTrim("NOMBRE_ESTACION");
                final String DIRECCION = tabla.getChildTextTrim("DIRECCION");
                final String LATITUD = tabla.getChildTextTrim("LATITUD");
                final String LONGITUD = tabla.getChildTextTrim("LONGITUD");
                final RadioBase radiobase = new RadioBase(id_solicitud, ID_CELDA, PROVINCIA, CANTON, CIUDAD_PARROQUIA, NOMBRE_ESTACION, DIRECCION, LATITUD, LONGITUD);
                datos.add(radiobase);
            }
            insertaRadioBase(datos);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void insertaRadioBase(final List<RadioBase> lista) throws Exception {
        final String proceso = "Inserta RadioBase";
        final ConexionO cnx = new ConexionO();
        try {
            final Connection con = cnx.conectar().getConexion();
            for (int i = 0; i < lista.size(); ++i) {
                final RadioBase radiobase = lista.get(i);
                final CallableStatement cst = con.prepareCall("{call FISK_API_PORTAL_SEGURIDAD.FISP_INGRESO_DATOS_SOLICITUDES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                cst.setInt(1, Integer.parseInt(radiobase.getId_solicitud()));
                cst.setString(2, radiobase.getID_CELDA());
                cst.setString(3, radiobase.getPROVINCIA());
                cst.setString(4, radiobase.getCANTON());
                cst.setString(5, radiobase.getCIUDAD_PARROQUIA());
                cst.setString(6, radiobase.getNOMBRE_ESTACION());
                cst.setString(7, radiobase.getDIRECCION());
                cst.setString(8, radiobase.getLATITUD());
                cst.setString(9, radiobase.getLONGITUD());
                cst.setString(10, null);
                cst.setString(11, null);
                cst.setString(12, null);
                cst.setString(13, null);
                cst.setString(14, null);
                cst.setString(15, null);
                cst.setString(16, null);
                cst.setString(17, null);
                cst.setString(18, null);
                cst.setString(19, null);
                cst.setString(20, null);
                cst.setString(21, null);
                cst.setString(22, null);
                cst.setString(23, null);
                cst.setString(24, null);
                cst.setString(25, null);
                cst.setString(26, null);
                cst.setString(27, null);
                cst.setString(28, null);
                cst.setString(29, null);
                cst.setString(30, null);
                cst.setString(31, null);
                cst.registerOutParameter(32, 12);
                cst.registerOutParameter(33, 12);
                cst.execute();
                final String codError = cst.getString(32);
                final String msjError = cst.getString(33);
                cst.close();
                if (!codError.equals("0")) {
                    throw new Exception(msjError);
                }
            }
            con.close();
        }
        catch (Exception ex) {
            ConsumoWs.log.error((Object)("Proceso: " + proceso + ": " + ex.toString()), (Throwable)ex);
            ex.printStackTrace();
            ConsumoWs.log.error((Object)ex.toString(), (Throwable)ex);
            throw new Exception(ex.toString());
        }
    }
    
    public static List<String> Cadena(final String cadena, final int cantidadCaracteresExtraer) {
        final List<String> list = new ArrayList<String>();
        final String sCadena = cadena;
        int acuini = 0;
        int acufin = 0;
        int acuaxu = 0;
        final int valorPorExtraer = cantidadCaracteresExtraer;
        String aux1 = "";
        String cadenaAux = "";
        String ultimoCaracter = "";
        String primerCaracter = "";
        while (acufin < sCadena.length()) {
            if (acufin == sCadena.length() - 1) {
                int resta = 0;
                resta = sCadena.length() - acufin;
                acufin += resta;
                acuini = acufin - resta;
            }
            else {
                acuaxu += valorPorExtraer;
                if (acuaxu > sCadena.length()) {
                    final int resta = 0;
                    acuini = acufin;
                    acufin = sCadena.length();
                }
                else {
                    acufin += valorPorExtraer;
                    acuini = acufin - valorPorExtraer;
                }
            }
            cadenaAux = sCadena.substring(acuini, acufin);
            ultimoCaracter = cadenaAux.substring(cadenaAux.length() - 1, cadenaAux.length());
            for (primerCaracter = cadenaAux.substring(0, 1); primerCaracter.equals(","); cadenaAux = (primerCaracter = sCadena.substring(cadenaAux.length() - 1, cadenaAux.length()))) {
                ++acuini;
                if (acufin < cadenaAux.length() - 1) {
                    ++acufin;
                }
                cadenaAux = sCadena.substring(acuini, acufin);
            }
            while (!ultimoCaracter.equals("'")) {
                ++acufin;
                cadenaAux = sCadena.substring(acuini, acufin);
                cadenaAux = (ultimoCaracter = sCadena.substring(cadenaAux.length() - 1, cadenaAux.length()));
            }
            aux1 = sCadena.substring(acuini, acufin);
            list.add(aux1);
            System.out.println("cadena: " + aux1);
        }
        return list;
    }
    
    public static String[] consumeSreAxis(final String tagsParametros, final String tipoTransaccion) {
        String mensRespuesta = null;
        String codRespuesta = null;
        final String idSolicitud = null;
        final String[] respuesta = new String[4];
        final Properties props = new Propiedades().getPropiedades();
        try {
            final URL url = new URL(props.getProperty("sreUrl"));
            final SreSoapHttpStub ws = new SreSoapHttpStub(url, (Service)null);
            final SreReceptaTransaccionElement recepta = new SreReceptaTransaccionElement(props.getProperty("sreIdIntacia"), tipoTransaccion, props.getProperty("sreIdIntegrador"), props.getProperty("sreCodAlternoDistribuidor"), props.getProperty("sreUsuario"), props.getProperty("sreClave"), props.getProperty("DataSourceEISAXIS"), new SreReceptaTransaccionElementParametros_Transaccion(convertXMLStringtoMessageElement(tagsParametros)));
            ConsumoWs.log.info((Object)("Parametros SRE " + tipoTransaccion));
            ConsumoWs.log.info((Object)("SreId_instancia\t\t\t\t--> " + recepta.getId_Instancia()));
            ConsumoWs.log.info((Object)("SreId_tipo_transaccion\t\t--> " + recepta.getId_Tipo_Transaccion()));
            ConsumoWs.log.info((Object)("SreId_integrador\t\t\t--> " + recepta.getId_Integrador()));
            ConsumoWs.log.info((Object)("SreCod_alterno_distribuidor\t--> " + recepta.getCod_Alterno_Distribuidor()));
            ConsumoWs.log.info((Object)("Usuario\t\t\t\t\t\t--> " + recepta.getUsuario()));
            ConsumoWs.log.info((Object)("Clave\t\t\t\t\t\t--> " + recepta.getClave()));
            ConsumoWs.log.info((Object)("DataSource\t\t\t\t\t--> " + recepta.getData_Source()));
            ConsumoWs.log.info((Object)"Parametros:");
            ConsumoWs.log.info((Object)tagsParametros);
            final SreReceptaTransaccionResponseElement response = ws.sreReceptaTransaccion(recepta);
            String result = response.getResult().get_any()[0].getAsString();
            ConsumoWs.log.info((Object)("REspuesta SRE " + tipoTransaccion + " " + result));
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            final String wsError = docu.getRootElement().getChild("WS_ERROR").getValue();
            final String wsMensError = docu.getRootElement().getChild("WS_MENSAJE_ERROR").getValue();
            if (wsError.equals("true")) {
                mensRespuesta = wsMensError;
                codRespuesta = "1";
                respuesta[0] = codRespuesta;
                respuesta[1] = wsMensError;
                return respuesta;
            }
            final Element tagPackResp = docu.getRootElement().getChild("PCK_RESPUESTA");
            codRespuesta = tagPackResp.getChildTextTrim("COD_RESPUESTA");
            mensRespuesta = tagPackResp.getChildTextTrim("MENSAJE");
            if (mensRespuesta == null || mensRespuesta.equals("")) {
                mensRespuesta = tagPackResp.getChildTextTrim("MSJ_ERROR");
            }
            if (mensRespuesta == null || mensRespuesta.equals("")) {
                mensRespuesta = tagPackResp.getChildTextTrim("MENSAJE_TECNICO");
            }
            if (codRespuesta == null || codRespuesta.isEmpty() || codRespuesta.equals("")) {
                codRespuesta = "1";
            }
            respuesta[0] = codRespuesta;
            respuesta[1] = mensRespuesta;
            respuesta[2] = tagPackResp.getChildTextTrim("RESULTADO");
            respuesta[3] = tagPackResp.getChildTextTrim("FECHA_CADENA");
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)e.getMessage(), (Throwable)e);
            respuesta[0] = "1";
            respuesta[1] = "ERROR SRE: " + e.toString();
            respuesta[3] = (respuesta[2] = null);
        }
        return respuesta;
    }
    
    private static MessageElement[] convertXMLStringtoMessageElement(final String xmlString) throws SAXException, IOException, ParserConfigurationException {
        final MessageElement[] m = { null };
        final org.w3c.dom.Document XMLDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(xmlString)));
        final org.w3c.dom.Element element = XMLDoc.getDocumentElement();
        m[0] = new MessageElement(element);
        return m;
    }
    
    public static String setearTags(final String parametros, final String valorParametros) {
        ConsumoWs.log.info((Object)("consumoSreSeguridad: Valor setearTags: " + valorParametros));
        String tags = "";
        final String[] tagName = parametros.split("\\|\\|");
        final String[] tagValue = valorParametros.split("\\|\\|");
        for (int i = 0; i < tagName.length; ++i) {
            tags = String.valueOf(tags) + "<" + tagName[i] + ">" + tagValue[i] + "</" + tagName[i] + ">";
        }
        tags = "<parametros>" + tags + "</parametros>";
        ConsumoWs.log.info((Object)("consumoSreSeguridad: Forma Tags: " + tags));
        return tags;
    }
    
    public static String[] consumeSreHW(final String tagsParametros, final String tipoTransaccion) {
        String mensRespuesta = null;
        String codRespuesta = null;
        final String idSolicitud = null;
        String codValor = null;
        final String[] respuesta = new String[4];
        final Properties props = new Propiedades().getPropiedades();
        URL url = null;
        try {
            url = new URL(props.getProperty("sreUrl"));
            ConsumoWs.log.info((Object)url.toString());
            final SreSoapHttpStub ws = new SreSoapHttpStub(url, (Service)null);
            final SreReceptaTransaccionElement recepta = new SreReceptaTransaccionElement(props.getProperty("sreIdIntaciadw"), tipoTransaccion, props.getProperty("sreIdIntegradordw"), props.getProperty("sreCodAlternoDistribuidordw"), props.getProperty("sreUsuariodw"), props.getProperty("sreClavedw"), props.getProperty("DataSourseEISOPER"), new SreReceptaTransaccionElementParametros_Transaccion(convertXMLStringtoMessageElement(tagsParametros)));
            ConsumoWs.log.info((Object)("Parametros SRE " + tipoTransaccion));
            ConsumoWs.log.info((Object)("SreId_instancia\t\t\t\t--> " + recepta.getId_Instancia()));
            ConsumoWs.log.info((Object)("SreId_tipo_transaccion\t\t--> " + recepta.getId_Tipo_Transaccion()));
            ConsumoWs.log.info((Object)("SreId_integrador\t\t\t--> " + recepta.getId_Integrador()));
            ConsumoWs.log.info((Object)("SreCod_alterno_distribuidor\t--> " + recepta.getCod_Alterno_Distribuidor()));
            ConsumoWs.log.info((Object)("Usuario\t\t\t\t\t\t--> " + recepta.getUsuario()));
            ConsumoWs.log.info((Object)("Clave\t\t\t\t\t\t--> " + recepta.getClave()));
            ConsumoWs.log.info((Object)("DataSource\t\t\t\t\t--> " + recepta.getData_Source()));
            ConsumoWs.log.info((Object)"Parametros:");
            ConsumoWs.log.info((Object)tagsParametros);
            final SreReceptaTransaccionResponseElement response = ws.sreReceptaTransaccion(recepta);
            String result = response.getResult().get_any()[0].getAsString();
            ConsumoWs.log.info((Object)("REspuesta SRE " + tipoTransaccion + " " + result));
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            final String wsError = docu.getRootElement().getChild("WS_ERROR").getValue();
            final String wsMensError = docu.getRootElement().getChild("WS_MENSAJE_ERROR").getValue();
            ConsumoWs.log.info((Object)("wsError: " + wsError + " wsMensError: " + wsMensError));
            if (wsError.equals("true")) {
                mensRespuesta = wsMensError;
                codRespuesta = "1";
                respuesta[0] = codRespuesta;
                respuesta[1] = wsMensError;
                return respuesta;
            }
            final Element tagPackResp = docu.getRootElement().getChild("PCK_RESPUESTA");
            codRespuesta = tagPackResp.getChildTextTrim("COD_RESPUESTA");
            codValor = tagPackResp.getChildTextTrim("VALOR");
            mensRespuesta = tagPackResp.getChildTextTrim("MENSAJE");
            if (mensRespuesta == null || mensRespuesta.equals("")) {
                mensRespuesta = tagPackResp.getChildTextTrim("MSJ_ERROR");
            }
            if (mensRespuesta == null || mensRespuesta.equals("")) {
                mensRespuesta = tagPackResp.getChildTextTrim("MENSAJE_TECNICO");
            }
            if (codRespuesta == null || codRespuesta.isEmpty() || codRespuesta.equals("")) {
                codRespuesta = "1";
            }
            respuesta[0] = codRespuesta;
            respuesta[1] = codValor;
            respuesta[2] = mensRespuesta;
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)e.getMessage(), (Throwable)e);
            respuesta[0] = null;
            respuesta[1] = "1";
            respuesta[2] = "ERROR SRE: " + e.toString();
        }
        return respuesta;
    }
    
    public static void procesaImeiImsiAbonadoHW(final String id_solicitud, String tramaValor, final String fechaIni, final String fechaFin, final String select, final String where, final String hoja, final String ImeiImsi) throws Exception {
        final String proceso = "Procesa Imei e Imsi de Abonado";
        ConsumoWs.log.info((Object)"[SUD] ConsumeWs.procesImeiImsiAbonadoHW");
        try {
            final Properties props = new Propiedades().getPropiedades();
            final String nombreVista = "FIS_DATOS_AIC_" + id_solicitud;
            final String queryDatos = "select distinct (obtiene_telefono_dnc(" + select + ")) valor from " + nombreVista + " where " + where + " in (" + tramaValor + ")";
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " VISTA:" + queryDatos));
            String numTmp = null;
            String tramaValores = null;
            boolean flag = true;
            final List<String> valores = new ArrayList<String>();
            final ConexionOpr cnx = new ConexionOpr();
            final Connection con = cnx.conectar().getConexion();
            final Statement st = con.createStatement();
            final ResultSet rs = st.executeQuery(queryDatos);
            int eisIdServInf = 0;
            String descripDatosVacios = "";
            ConsumoWs.log.info((Object)"[SUD] Itera sobre cada servicio encontrado.");
            while (rs.next()) {
                numTmp = rs.getString("valor");
                numTmp = numTmp.substring(numTmp.length() - 8);
                if (flag) {
                    tramaValores = "'" + numTmp + "'";
                    flag = false;
                }
                else {
                    tramaValores = String.valueOf(tramaValores) + "," + "'" + numTmp + "'";
                }
            }
            st.close();
            rs.close();
            con.close();
            ConsumoWs.log.info((Object)("[SUD] Los n\u00fameros encontrados son: " + tramaValores));
            if (tramaValores == null || tramaValores.isEmpty() || tramaValores.equals("")) {
                tramaValor = tramaValor.replace("'", "");
                String printE = "";
                if (ImeiImsi.equals("I")) {
                    eisIdServInf = Integer.parseInt(props.getProperty("eisIMEI"));
                    printE = "solicitud " + id_solicitud + ", Imei " + tramaValor + " no devolvi\u00f3 numeros celulares.. consulta directo hacia axis";
                    descripDatosVacios = "El(Los) imei('s) no registra(n) tr\u00e1fica en la red desde el rango de fechas ingresado";
                }
                else {
                    eisIdServInf = Integer.parseInt(props.getProperty("eisIMSI"));
                    printE = "solicitud " + id_solicitud + ", Imsi " + tramaValor + " no devolvi\u00f3 numeros celulares.. consulta directo hacia axis";
                    descripDatosVacios = "El(Los) imsi('s) " + tramaValor + " no registra(n) tr\u00e1fica en la red desde el rango de fechas ingresado";
                }
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + printE));
                grabarMensajeSinDatos(id_solicitud, descripDatosVacios, hoja);
                return;
            }
            ConsumoWs.log.info((Object)("[SUD] Llamado a procesaAbonadosV3HW, eisNumero_Celular_Hw: " + props.getProperty("eisNUMERO_CELULAR_HW")));
            procesaAbonadosV3HW(id_solicitud, Integer.parseInt(props.getProperty("eisNUMERO_CELULAR_HW")), tramaValores, fechaIni, fechaFin, "N\u00fameros Resultantes:", hoja);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("[SUD][" + id_solicitud + "] Se propaga el error Error en Armando del XML fuera de ConsumeWs.procesaImeiImsiAbonadoHW"));
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void procesaICCIDAbonadoHW(final String id_solicitud, final int id_Transaccion, final String valor, final String fechaIni, final String fechaFin) throws Exception {
        final String proceso = "Procesa ICC ID Abonado";
        try {
            final Properties props = new Propiedades().getPropiedades();
            final String dataSource = props.getProperty("DataSourceEISAXIS");
            final String eisWsUrl = props.getProperty("eisWsUrl");
            final String userBDAxis = props.getProperty("userBDAxis");
            boolean flag = true;
            final String descripDatosVacios = "";
            final BigDecimal idTransaccion = new BigDecimal(String.valueOf(id_Transaccion));
            final URL url = new URL(eisWsUrl);
            final EisSoapHttpStub ws = new EisSoapHttpStub(url, (Service)null);
            final EipConsumeServicioElement parameters = new EipConsumeServicioElement();
            EipConsumeServicioResponseElement response = null;
            EipConsumeServicio_Out resultado = new EipConsumeServicio_Out();
            parameters.setDsId(dataSource);
            parameters.setPnIdServicioInformacion(idTransaccion);
            parameters.setPvParametroBind1(valor);
            parameters.setPvParametroBind2("?");
            parameters.setPvParametroBind3("?");
            parameters.setPvParametroBind4("?");
            parameters.setPvParametroBind5("?");
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros EISNumServicioByIccid:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " idTransaccion -->" + idTransaccion));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " valor -->" + valor));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind2 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind3 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind4 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind5 --> ?"));
            response = ws.eipConsumeServicio(parameters);
            resultado = response.getResult();
            resultado = response.getResult();
            if (resultado.getPnerrorOut() != 0) {
                throw new Exception(resultado.getPverrorOut());
            }
            String result = resultado.getPvresultadoOut();
            System.out.print("Resultado EISNumServicioByIccid:");
            System.out.print(result);
            if (result == null || result.isEmpty() || result == "") {
                final List<Abonado> datos = new ArrayList<Abonado>();
                final Abonado abonado = new Abonado(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null);
                datos.add(abonado);
                insertaAbonado(datos);
                return;
            }
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            String tramaValor = "";
            for (int i = 0; i < list.size(); ++i) {
                final Element tabla = (Element) list.get(i);
                if (flag) {
                    tramaValor = "'" + tabla.getChildTextTrim("ID_SERVICIO") + "'";
                    flag = false;
                }
                else {
                    tramaValor = String.valueOf(tramaValor) + "," + "'" + tabla.getChildTextTrim("ID_SERVICIO") + "'";
                }
            }
            procesaAbonadosV2(id_solicitud, Integer.parseInt(props.getProperty("eisNUMERO_CELULAR_HW")), tramaValor, "", "", "N\u00fameros Resultantes:", null);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void procesaImsiAbonadoHW(final String id_solicitud, final int id_Transaccion, final String valor, final String fechaIni, final String fechaFin) throws Exception {
        final String proceso = "Procesa Imsi de Abonado";
        try {
            final Properties props = new Propiedades().getPropiedades();
            final String dataSource = props.getProperty("DataSourceEISAXIS");
            final String eisWsUrl = props.getProperty("eisWsUrl");
            final String userBDAxis = props.getProperty("userBDAxis");
            final boolean flag = true;
            final String descripDatosVacios = "";
            final BigDecimal idTransaccion = new BigDecimal(String.valueOf(id_Transaccion));
            final URL url = new URL(eisWsUrl);
            final EisSoapHttpStub ws = new EisSoapHttpStub(url, (Service)null);
            final EipConsumeServicioElement parameters = new EipConsumeServicioElement();
            EipConsumeServicioResponseElement response = null;
            EipConsumeServicio_Out resultado = new EipConsumeServicio_Out();
            parameters.setDsId(dataSource);
            parameters.setPnIdServicioInformacion(idTransaccion);
            parameters.setPvParametroBind1(valor);
            parameters.setPvParametroBind2("?");
            parameters.setPvParametroBind3("IMSI");
            parameters.setPvParametroBind4(String.valueOf(userBDAxis) + "CL_SIMCARD");
            parameters.setPvParametroBind5("DISTINCT(ICCID)");
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros EISIccidByImsi:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " idTransaccion -->" + idTransaccion));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " valor -->" + valor));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind2 --> ?"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind3 --> ICCID"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind4 --> " + userBDAxis + "CL_SIMCARD"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " PvParametroBind5 --> DISTINCT(IMSI)"));
            response = ws.eipConsumeServicio(parameters);
            resultado = response.getResult();
            if (resultado.getPnerrorOut() != 0) {
                throw new Exception(resultado.getPverrorOut());
            }
            String result = resultado.getPvresultadoOut();
            System.out.print("Resultado EISIccidByImsi:");
            System.out.print(result);
            if (result == null || result.isEmpty() || result == "") {
                final List<Abonado> datos = new ArrayList<Abonado>();
                final Abonado abonado = new Abonado(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null);
                datos.add(abonado);
                insertaAbonado(datos);
                return;
            }
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            String tramaValor = "";
            for (int i = 0; i < list.size(); ++i) {
                final Element tabla = (Element) list.get(i);
                tramaValor = tabla.getChildTextTrim("NUMERO");
            }
            procesaICCIDAbonadoHW(id_solicitud, Integer.parseInt(props.getProperty("eisNumByIccid")), tramaValor, fechaIni, fechaFin);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static void procesaAbonadosHW(final String id_solicitud, final int id_Transaccion) throws Exception {
        ConsumoWs.log.info((Object)"[SUD] Metodo ConsumoWS.procesaAbonadoHW");
        final String proceso = "Procesa Abonados HW";
        final Date auxDate = null;
        try {

        	final Properties props = new Propiedades().getPropiedades();
            final String dataSource = props.getProperty("DataSourseEISOPER");
            final String eisWsUrl = props.getProperty("eisWsUrl");
            final BigDecimal idTransaccion = new BigDecimal(String.valueOf(id_Transaccion));
            final URL url = new URL(eisWsUrl);
            final EisSoapHttpStub ws = new EisSoapHttpStub(url, (Service)null);
            final EipConsumeServicioElement parameters = new EipConsumeServicioElement();
            
            EipConsumeServicioResponseElement response = null;
            EipConsumeServicio_Out resultado = new EipConsumeServicio_Out();
            
            parameters.setDsId(dataSource);
            parameters.setPnIdServicioInformacion(idTransaccion);
            parameters.setPvParametroBind1(id_solicitud);
            
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros EIS Abonado:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " idTransaccion -->" + idTransaccion));
            
            response = ws.eipConsumeServicio(parameters);
            resultado = response.getResult();
            resultado = response.getResult();
            
            ConsumoWs.log.info((Object)("PS procesaAbonadosHW solicitud: " + id_solicitud + " resultado " + resultado));
            
            if (resultado.getPnerrorOut() != 0) {
                throw new Exception(resultado.getPverrorOut());
            }
            String result = resultado.getPvresultadoOut();
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Resultado EISAbonado:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + result));
            final List<Abonado> datos = new ArrayList<Abonado>();
            if (result == null || result.isEmpty() || result == "") {
                final Abonado abonado = new Abonado(id_solicitud, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null);
                datos.add(abonado);
                insertaAbonado(datos);
                return;
            }
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            for (int i = 0; i < list.size(); ++i) {
                final Element tabla = (Element) list.get(i);
                final String NOMBRE_COMPLETO = tabla.getChildTextTrim("NOMBRE_COMPLETO");
                final String IMEI = tabla.getChildTextTrim("IMEI");
                final String IMSI = tabla.getChildTextTrim("IMSI");
                final String ICCID = tabla.getChildTextTrim("ICCID");
                final String IDENTIFICACION = tabla.getChildTextTrim("IDENTIFICACION");
                final String DIRECCION_COMPLETA = tabla.getChildTextTrim("DIRECCION_COMPLETA");
                final String TELEFONO1 = tabla.getChildTextTrim("TELEFONO1");
                final String FECHA_INICIO = tabla.getChildTextTrim("FECHA_INICIO");
                ConsumoWs.log.info((Object)("fecha_ini" + FECHA_INICIO));
                final String FECHA_FIN = tabla.getChildTextTrim("FECHA_FIN");
                ConsumoWs.log.info((Object)("fecha_fin" + FECHA_FIN));
                final String ID_SERVICIO = tabla.getChildTextTrim("ID_SERVICIO");
                final String SUBPRODUCTO = tabla.getChildTextTrim("SUBPRODUCTO");
                final String CIUDAD = tabla.getChildTextTrim("CIUDAD");
                final String MIGRACION = "X";
                final Abonado abonado = new Abonado(id_solicitud, NOMBRE_COMPLETO, IMEI, IMSI, ICCID, IDENTIFICACION, DIRECCION_COMPLETA, TELEFONO1, FECHA_INICIO, FECHA_FIN, ID_SERVICIO, SUBPRODUCTO, CIUDAD, (String)null, (String)null, (String)null, MIGRACION);
                datos.add(abonado);
            }
            insertaAbonadoAx(datos);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            e.printStackTrace();
            ConsumoWs.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.toString());
        }
    }
    
    public static String[] consumeSreHW_DWH(final String tagsParametros, final String tipoTransaccion) {
        String mensRespuesta = null;
        String codRespuesta = null;
        final String idSolicitud = null;
        String codValor = null;
        final String[] respuesta = new String[4];
        final Properties props = new Propiedades().getPropiedades();
        URL url = null;
        try {
            url = new URL(props.getProperty("sreUrl"));
            ConsumoWs.log.info((Object)url.toString());
            final SreSoapHttpStub ws = new SreSoapHttpStub(url, (Service)null);
            final SreReceptaTransaccionElement recepta = new SreReceptaTransaccionElement(props.getProperty("sreIdIntaciadw"), tipoTransaccion, props.getProperty("sreIdIntegradordw"), props.getProperty("sreCodAlternoDistribuidordw"), props.getProperty("sreUsuariodw"), props.getProperty("sreClavedw"), props.getProperty("DataSourseEISOPER"), new SreReceptaTransaccionElementParametros_Transaccion(convertXMLStringtoMessageElement(tagsParametros)));
            ConsumoWs.log.info((Object)("Parametros SRE " + tipoTransaccion));
            ConsumoWs.log.info((Object)("SreId_instancia\t\t\t\t--> " + recepta.getId_Instancia()));
            ConsumoWs.log.info((Object)("SreId_tipo_transaccion\t\t--> " + recepta.getId_Tipo_Transaccion()));
            ConsumoWs.log.info((Object)("SreId_integrador\t\t\t--> " + recepta.getId_Integrador()));
            ConsumoWs.log.info((Object)("SreCod_alterno_distribuidor\t--> " + recepta.getCod_Alterno_Distribuidor()));
            ConsumoWs.log.info((Object)("Usuario\t\t\t\t\t\t--> " + recepta.getUsuario()));
            ConsumoWs.log.info((Object)("Clave\t\t\t\t\t\t--> " + recepta.getClave()));
            ConsumoWs.log.info((Object)("DataSource\t\t\t\t\t--> " + recepta.getData_Source()));
            ConsumoWs.log.info((Object)"Parametros:");
            ConsumoWs.log.info((Object)tagsParametros);
            final SreReceptaTransaccionResponseElement response = ws.sreReceptaTransaccion(recepta);
            String result = response.getResult().get_any()[0].getAsString();
            ConsumoWs.log.info((Object)("REspuesta SRE " + tipoTransaccion + " " + result));
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            final String wsError = docu.getRootElement().getChild("WS_ERROR").getValue();
            final String wsMensError = docu.getRootElement().getChild("WS_MENSAJE_ERROR").getValue();
            if (wsError.equals("true")) {
                mensRespuesta = wsMensError;
                codRespuesta = "1";
                respuesta[0] = codRespuesta;
                respuesta[1] = wsMensError;
                return respuesta;
            }
            final Element tagPackResp = docu.getRootElement().getChild("PCK_RESPUESTA");
            codRespuesta = tagPackResp.getChildTextTrim("COD_RESPUESTA");
            codValor = tagPackResp.getChildTextTrim("VALOR");
            mensRespuesta = tagPackResp.getChildTextTrim("PV_ERROR");
            if (mensRespuesta == null || mensRespuesta.equals("")) {
                mensRespuesta = tagPackResp.getChildTextTrim("MSJ_ERROR");
            }
            if (mensRespuesta == null || mensRespuesta.equals("")) {
                mensRespuesta = tagPackResp.getChildTextTrim("MENSAJE_TECNICO");
            }
            if (codRespuesta == null || codRespuesta.isEmpty() || codRespuesta.equals("")) {
                codRespuesta = "1";
            }
            respuesta[0] = codValor;
            respuesta[1] = codRespuesta;
            respuesta[2] = mensRespuesta;
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)e.getMessage(), (Throwable)e);
            respuesta[0] = null;
            respuesta[1] = "1";
            respuesta[2] = "ERROR SRE: " + e.toString();
        }
        return respuesta;
    }
    
    public static void procesaAbonadosV3HW(final String id_solicitud, final long id_Transaccion, final String tramaValor, String fechaIni, String fechaFin, final String descTramaValor, final String hoja) throws Exception {
        final String proceso = "Procesa Abonados V3 HW";
        ConsumoWs.log.info((Object)"[SUD] ConsumoWs.procesaAbonadosV3HW");
        ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Procesa Abonados HW"));
        Date auxDate = null;
        try {
            if (fechaIni == null || fechaIni.equals("0")) {
                fechaIni = "";
            }
            if (fechaFin == null || fechaFin.equals("0")) {
                fechaFin = "";
            }
            if (!fechaIni.equals("")) {
                auxDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaIni);
                fechaIni = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(auxDate);
            }
            if (!fechaFin.equals("")) {
                auxDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaFin);
                fechaFin = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(auxDate);
            }
            final Properties props = new Propiedades().getPropiedades();
            final String dataSource = props.getProperty("DataSourceEISAXIS");
            final String eisWsUrl = props.getProperty("eisWsUrl");
            final BigDecimal idTransaccion = new BigDecimal(String.valueOf(id_Transaccion));
            final URL url = new URL(eisWsUrl);
            final EisSoapHttpStub ws = new EisSoapHttpStub(url, (Service)null);
            final EipConsumeServicioElement parameters = new EipConsumeServicioElement();
            EipConsumeServicioResponseElement response = null;
            EipConsumeServicio_Out resultado = new EipConsumeServicio_Out();
            parameters.setDsId(dataSource);
            parameters.setPnIdServicioInformacion(idTransaccion);
            parameters.setPvParametroBind1(fechaIni);
            parameters.setPvParametroBind2(fechaFin);
            parameters.setPvParametroBind3("?");
            parameters.setPvParametroBind4("?");
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros EIS AbonadoV3 HW:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " idTransaccion -->" + idTransaccion));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " fechaIni -->" + fechaIni));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " fechaFin -->" + fechaFin));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " valorIn -->" + tramaValor));
            response = ws.eipConsumeServicio(parameters);
            final int cantidadCaracteres = 5000;
            String result = "";
            String resultadoAux = "";
            if (tramaValor.length() >= cantidadCaracteres) {
                List<String> list = new ArrayList<String>();
                list = Cadena(tramaValor, cantidadCaracteres);
                for (final String bloqueCaracteres : list) {
                    parameters.setPvParametroBind5(bloqueCaracteres);
                    response = ws.eipConsumeServicio(parameters);
                    resultado = response.getResult();
                    if (resultado.getPnerrorOut() != 0) {
                        throw new Exception(resultado.getPverrorOut());
                    }
                    resultadoAux = resultado.getPvresultadoOut();
                    result = String.valueOf(result) + resultadoAux;
                }
            }
            else {
                parameters.setPvParametroBind5(tramaValor);
                response = ws.eipConsumeServicio(parameters);
                resultado = response.getResult();
                if (resultado.getPnerrorOut() != 0) {
                    throw new Exception(resultado.getPverrorOut());
                }
                result = resultado.getPvresultadoOut();
            }
            ConsumoWs.log.info((Object)("[SUD] Se consulta informaci\u00f3n en AXIS de los servicios: " + tramaValor));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Resultado EISAbonado HW:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + result));
            final List<Abonado> datos = new ArrayList<Abonado>();
            if (descTramaValor != null) {
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " (HW) longitud trama valor: " + tramaValor.length()));
                final int valorPorExtraer = 3000;
                if (tramaValor.length() > valorPorExtraer) {
                    List<String> list2 = new ArrayList<String>();
                    list2 = Cadena(tramaValor, valorPorExtraer);
                    for (final String caracteres : list2) {
                        ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " cadena HW: " + caracteres));
                        final Abonado abonado = new Abonado(id_solicitud, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, descTramaValor, caracteres.replace("'", ""), hoja);
                        datos.add(abonado);
                    }
                }
                else {
                    final Abonado abonado = new Abonado(id_solicitud, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, descTramaValor, tramaValor.replace("'", ""), hoja);
                    datos.add(abonado);
                }
                insertaAbonado(datos);
                datos.clear();
            }
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Resultado EISAbonado HW:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " " + result));
            if (result == null || result.isEmpty() || result == "") {
                final Abonado abonado = new Abonado(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, hoja);
                datos.add(abonado);
                insertaAbonado(datos);
                final String lsBanderaSerImei = props.getProperty("bandValidaServImei");
                if (lsBanderaSerImei.equalsIgnoreCase("S")) {
                    final String parametros = "DATO";
                    final String valorParametros = id_solicitud;
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Entro x N/A"));
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " valorParametrosHW...:" + valorParametros));
                    final String tagsParametros = setearTags(parametros, valorParametros);
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " tagsParametrosHW...:" + tagsParametros));
                    final String[] respSRE = consumeSreValidaSerImei(tagsParametros, props.getProperty("sreValidaServiciosIMEI"));
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Respuesta SRE Valida Servicios VLR vs HUAWEI"));
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " COD_RESPUESTA: " + respSRE[0]));
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " MENSAJE: " + respSRE[1]));
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " VALOR: " + respSRE[2]));
                    if (!respSRE[2].equals("0")) {
                        ConsumoWs.log.warn((Object)("solicitud: " + id_solicitud + "Abonado N/A Axis- error al verificar Servicios de Imei DEL VLR en Huawei"));
                        throw new Exception("solicitud: " + id_solicitud + "Abonado N/A Axis- error al verificar Servicios de Imei del VLR en Huawei");
                    }
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " INICIA Actualiza IMEI VLR Caso N/A"));
                    actualizaImeiBdLocalHWVLR(id_solicitud);
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " FIN Actualiza IMEI VLR Caso N/A"));
                }
                return;
            }
            result = result.replace("<DATOS>", "");
            result = result.replace("</DATOS>", "");
            result = "<DATOS>" + result + "</DATOS>";
            result = result.replaceAll("&", "&amp;");
            try {
                final SAXBuilder builder = new SAXBuilder();
                final Reader stringReader = new StringReader(result);
                final Document docu = builder.build(stringReader);
                final List list3 = docu.getRootElement().getChildren();
                final List<String> numActivos = new ArrayList<String>();
                for (int i = 0; i < list3.size(); ++i) {
                    final Element tabla = (Element) list3.get(i);
                    final String NOMBRE_COMPLETO = tabla.getChildTextTrim("NOMBRE_COMPLETO");
                    final String IMEI = tabla.getChildTextTrim("IMEI");
                    final String IMSI = tabla.getChildTextTrim("IMSI");
                    final String ICCID = tabla.getChildTextTrim("ICCID");
                    final String IDENTIFICACION = tabla.getChildTextTrim("IDENTIFICACION");
                    final String DIRECCION_COMPLETA = tabla.getChildTextTrim("DIRECCION_COMPLETA");
                    final String TELEFONO1 = tabla.getChildTextTrim("TELEFONO1");
                    final String FECHA_INICIO = tabla.getChildTextTrim("FECHA_INICIO");
                    final String FECHA_FIN = tabla.getChildTextTrim("FECHA_FIN");
                    final String ID_SERVICIO = tabla.getChildTextTrim("ID_SERVICIO");
                    final String SUBPRODUCTO = tabla.getChildTextTrim("SUBPRODUCTO");
                    final String CIUDAD = tabla.getChildTextTrim("CIUDAD");
                    final String MIGRACION = tabla.getChildTextTrim("MIGRACION");
                    final Abonado abonado = new Abonado(id_solicitud, NOMBRE_COMPLETO, IMEI, IMSI, ICCID, IDENTIFICACION, DIRECCION_COMPLETA, TELEFONO1, FECHA_INICIO, FECHA_FIN, ID_SERVICIO, SUBPRODUCTO, CIUDAD, (String)null, (String)null, hoja, MIGRACION);
                    datos.add(abonado);
                    if (FECHA_FIN == null || FECHA_FIN.isEmpty()) {
                        numActivos.add(ID_SERVICIO);
                    }
                }
                insertaAbonadoAx(datos);
                final String lsBanderaSerImei2 = props.getProperty("bandValidaServImei");
                ConsumoWs.log.info((Object)("[SUD] Bandera de Valida Servicio Imei: " + lsBanderaSerImei2));
                if (lsBanderaSerImei2.equalsIgnoreCase("S")) {
                    final String parametros2 = "DATO";
                    final String valorParametros2 = id_solicitud;
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Entro x Normal"));
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " valorParametrosHW...:" + valorParametros2));
                    final String tagsParametros2 = setearTags(parametros2, valorParametros2);
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " tagsParametrosHW...:" + tagsParametros2));
                    ConsumoWs.log.info((Object)"[SUD] Se procede a validar el servicio en axis.");
                    final String[] respSRE2 = consumeSreValidaSerImei(tagsParametros2, props.getProperty("sreValidaServiciosIMEI"));
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Respuesta SRE Valida Servicios VLR vs HUAWEI"));
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " COD_RESPUESTA: " + respSRE2[0]));
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " MENSAJE: " + respSRE2[1]));
                    ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " VALOR: " + respSRE2[2]));
                    if (!respSRE2[2].equals("0")) {
                        ConsumoWs.log.warn((Object)("solicitud: " + id_solicitud + " error al verificar Servicios de Imei DEL VLR en Huawei"));
                        throw new Exception("solicitud: " + id_solicitud + " error al verificar Servicios de Imei del VLR en Huawei");
                    }
                }
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " INICIA Actualiza IMEI Caso normal"));
                ConsumoWs.log.info((Object)"[SUD] Se procede a actualizar la BD Local.");
                actualizaImeiBdLocalHW(id_solicitud, numActivos);
                ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " FIN Actualiza IMEI Caso normal"));
            }
            catch (Exception e) {
                ConsumoWs.log.error((Object)("[SUD][" + id_solicitud + "] Se gener\u00f3 error en ConsumoWs.procesaAbonadosV3HW --> " + e.getMessage() + " --> " + e.toString()));
                throw new Exception("Error al armar el XML para la extracci\u00f3n de IMEI.");
            }
        }
        catch (Exception e2) {
            ConsumoWs.log.error((Object)("[SUD][" + id_solicitud + "] Se propaga error fuera de ConsumoWs.procesaAbonadosV3HW --> " + e2.getMessage() + " --> " + e2.toString()));
            ConsumoWs.log.error((Object)("Solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e2.getMessage()));
            e2.printStackTrace();
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " " + e2.toString()), (Throwable)e2);
            throw new Exception(e2.toString());
        }
    }
    
    public static void actualizaImeiBdLocalHW(final String idSolicitud, final List<String> numActivos) throws Exception {
        final String proceso = "Actualiza Imei Bd Local HW";
        ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] ConsumoWs.actualizaImeiBdLocalHW"));
        ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Actualizar BD local con imei del vlr HW"));
        final String in = "";
        boolean flag = true;
        final String nombreVista = "FIS_DATOS_AIC_" + idSolicitud;
        final String queryDatos = "select distinct(SUBSTR (imei,1,14)) valor from " + nombreVista + " where msisdn =";
        final String queryPeriodoimei = "select SUBSTR (periodo,1,4)||'-'||SUBSTR (periodo,5,2) imei_periodo from(select DISTINCT(SUBSTR (TABLA_DIARIA,11,6)) periodo from " + nombreVista + " WHERE IMEI = ? AND MSISDN = ?)";
        final String queryUpdateImeiReal = "update fis_datos_solicitud set campo_2=? where id_solicitud=? and OBTIENE_TELEFONO_DNC(campo_8)=? and campo_10 is null";
        final String queryUpdatePeriodoImei = "update fis_datos_solicitud set campo_13=? where id_solicitud=? and OBTIENE_TELEFONO_DNC(campo_8)=? and campo_10 is null";
        final String queryfistmpdatoshw = "select OBTIENE_TELEFONO_DNC(servicio)  from fis_tmp_datos_hw\t where id_solicitud =?";
        final List<String> numActivosL = numActivos;
        final ConexionOpr cnx = new ConexionOpr();
        final ConexionO cnxo = new ConexionO();
        final Connection conVlr = cnx.conectar().getConexion();
        Connection conBdlocal = cnxo.conectar().getConexion();
        int n = 0;
        try {
            String imeisVlr = "";
            String imeisToAxis = "";
            String sAux = "";
            conBdlocal.setAutoCommit(false);
            ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Consulta la tabla FIS_TMP_DATOS_HW, " + queryfistmpdatoshw));
            final PreparedStatement pt = conBdlocal.prepareStatement(queryfistmpdatoshw);
            pt.setString(1, idSolicitud);
            final ResultSet rt = pt.executeQuery();
            ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Punto de control, while numActivosL"));
            while (rt.next()) {
                numActivosL.add(rt.getString(1));
            }
            rt.close();
            pt.close();
            ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Se cierra conexi\u00f3n a base Local para consultas a la temporal."));
            conBdlocal.close();
            ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Punto de control, fin while numActivosL"));
            ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Punto de control, for num in numActivosL"));
            for (final String num : numActivosL) {
                ++n;
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] PSL solicitud: " + idSolicitud + " Extrayendo imeis desde vlr del numero HW: " + num));
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Extrayendo imeis desde vlr del numero HW: " + num));
                String periodoImei = "";
                String numTmp = num;
                final int lenght = numTmp.length();
                numTmp = numTmp.substring(lenght - 8);
                numTmp = "'593" + numTmp + "'";
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] PSL solicitud: " + idSolicitud + " numero tmp:" + numTmp));
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " numero tmp:" + numTmp));
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Se ejecuta la extracci\u00f3n de IMEIS desde el VLR --> {query: " + queryDatos + numTmp + "}"));
                PreparedStatement pst = conVlr.prepareStatement(String.valueOf(queryDatos) + numTmp);
                ResultSet rs = pst.executeQuery();
                flag = true;
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Punto de control, while IMEIs desde el VLR-OPER"));
                while (rs.next()) {
                    if (flag) {
                        imeisToAxis = String.valueOf(rs.getString("valor")) + DigitoVerificador.obtieneDigitoVerificador(rs.getString("valor"));
                        imeisVlr = rs.getString("valor");
                        flag = false;
                    }
                    else {
                        sAux = String.valueOf(rs.getString("valor")) + DigitoVerificador.obtieneDigitoVerificador(rs.getString("valor"));
                        imeisToAxis = String.valueOf(imeisToAxis) + ",\n" + sAux;
                        imeisVlr = String.valueOf(imeisVlr) + "," + rs.getString("valor");
                    }
                }
                rs.close();
                pst.close();
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Punto de control, fin while IMEIs desde el VLR-OPER"));
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Imeis resultantes vlr del numero: " + num));
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " " + imeisToAxis));
                if (imeisVlr != null && !imeisVlr.isEmpty()) {
                    ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Obteniendo Periodo de ocurrencia por cada imei resultante del numero " + num + "..."));
                    boolean tmpflag = true;
                    final String[] imei = imeisVlr.split(",");
                    String[] array;
                    for (int length = (array = imei).length, i = 0; i < length; ++i) {
                        final String im = array[i];
                        pst = conVlr.prepareStatement(queryPeriodoimei);
                        pst.setString(1, String.valueOf(im) + "0");
                        pst.setString(2, numTmp.replace("'", ""));
                        rs = pst.executeQuery();
                        while (rs.next()) {
                            if (tmpflag) {
                                periodoImei = String.valueOf(im) + DigitoVerificador.obtieneDigitoVerificador(im) + " --> " + rs.getString("imei_periodo");
                                tmpflag = false;
                            }
                            else {
                                periodoImei = String.valueOf(periodoImei) + "\n" + im + DigitoVerificador.obtieneDigitoVerificador(im) + " --> " + rs.getString("imei_periodo");
                            }
                        }
                        ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Imei - Periodo"));
                        ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " " + periodoImei));
                        rs.close();
                        pst.close();
                    }
                }
                else {
                    imeisToAxis = (periodoImei = "Imei no registra trafica en la Red");
                }
                if (imeisToAxis.length() > 3500) {
                    imeisToAxis = imeisToAxis.substring(1, 3500);
                }
                if (periodoImei.length() > 3500) {
                    periodoImei = periodoImei.substring(1, 3500);
                }
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Numero " + num + " Actualizando BD local con imeis devueltos desde VLR.."));
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Updates a ejecutar:\nImei Real: " + queryUpdateImeiReal + "; Datos: {" + imeisToAxis + "|" + idSolicitud + "}\nPeriodo Imei: " + queryUpdatePeriodoImei + "; Datos:{" + periodoImei + "|" + idSolicitud + "}"));
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Se establece la conexi\u00f3n a la base para updates."));
                conBdlocal = cnxo.conectar().getConexion();
                conBdlocal.setAutoCommit(false);
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] PrepareStatement para queryUpdateImeiReal"));
                pst = conBdlocal.prepareStatement(queryUpdateImeiReal);
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Asignaci\u00f3n de Parametros."));
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] \tAsignando imeisToAxis"));
                pst.setString(1, imeisToAxis);
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] \tAsignando idSolicitud"));
                pst.setString(2, idSolicitud);
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] \tAsignando servicio"));
                pst.setString(3, num);
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Se procede a ejecutar update"));
                pst.executeUpdate();
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Se cierra el ejecutor."));
                pst.close();
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Query Imei Real ejecutado correctamente."));
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] PrepareStatement para queryUpdatePeriodoImei"));
                pst = conBdlocal.prepareStatement(queryUpdatePeriodoImei);
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Asignaci\u00f3n de Parametros."));
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] \tAsignando periodoImei"));
                pst.setString(1, periodoImei);
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] \tAsignando idSolicitud"));
                pst.setString(2, idSolicitud);
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] \tAsignando num"));
                pst.setString(3, num);
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Se procede a ejecutar update"));
                pst.executeUpdate();
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Se cierra el ejecutor."));
                pst.close();
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Query Periodo Imei ejecutado correctamente."));
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Se realiza COMMIT para las transacciones de Updates."));
                ConsumoWs.log.info((Object)("PSL solicitud: " + idSolicitud + " Numero " + num + " actualizado!"));
                conBdlocal.commit();
                ConsumoWs.log.info((Object)("[SUD][" + idSolicitud + "] Se cierra la conexci\u00f3n de Updates."));
                conBdlocal.close();
            }
            ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " imeis actualizados correctamente!"));
        }
        catch (Exception e) {
            conBdlocal.rollback();
            ConsumoWs.log.error((Object)("[SUD][" + idSolicitud + "] Proceso: Error en actualizaImeiBdLocalHW --> " + e.toString()));
            ConsumoWs.log.error((Object)("solicitud: " + idSolicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            ConsumoWs.log.error((Object)("solicitud: " + idSolicitud + " " + e.toString()), (Throwable)e);
            throw new Exception(String.valueOf(e.toString()) + "Error en actualizaImeiBdLocalHW");
        }
        finally {
            try {
                if (cnxo.getConexion() != null && !cnxo.getConexion().isClosed()) {
                    cnxo.getConexion().close();
                }
                if (cnx.getConexion() != null && !cnx.getConexion().isClosed()) {
                    cnx.getConexion().close();
                }
            }
            catch (Exception e2) {
                ConsumoWs.log.fatal((Object)("solicitud: " + idSolicitud + " Cerrar Conexion: " + e2.toString()), (Throwable)e2);
            }
        }
        try {
            if (cnxo.getConexion() != null && !cnxo.getConexion().isClosed()) {
                cnxo.getConexion().close();
            }
            if (cnx.getConexion() != null && !cnx.getConexion().isClosed()) {
                cnx.getConexion().close();
            }
        }
        catch (Exception e2) {
            ConsumoWs.log.fatal((Object)("solicitud: " + idSolicitud + " Cerrar Conexion: " + e2.toString()), (Throwable)e2);
        }
    }
    
    public static void actualizaImeiBdLocalHWVLR(final String idSolicitud) throws Exception {
        final String proceso = "Actualiza Imei Bd Local HW solo VLR";
        ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Actualizar BD local con imei del vlr HW"));
        final String in = "";
        boolean flag = true;
        final String nombreVista = "FIS_DATOS_AIC_" + idSolicitud;
        final String queryDatos = "select distinct(SUBSTR (imei,1,14)) valor from " + nombreVista + " where msisdn =";
        final String queryPeriodoimei = "select SUBSTR (periodo,1,4)||'-'||SUBSTR (periodo,5,2) imei_periodo from(select DISTINCT(SUBSTR (TABLA_DIARIA,11,6)) periodo from " + nombreVista + " WHERE IMEI = ? AND MSISDN = ?)";
        final String queryUpdateImeiReal = "update fis_datos_solicitud set campo_2=? where id_solicitud=? and OBTIENE_TELEFONO_DNC(campo_8)=? and campo_10 is null";
        final String queryUpdatePeriodoImei = "update fis_datos_solicitud set campo_13=? where id_solicitud=? and OBTIENE_TELEFONO_DNC(campo_8)=? and campo_10 is null";
        final String queryfistmpdatoshw = "select OBTIENE_TELEFONO_DNC(campo_8)  from fis_datos_solicitud\t where id_solicitud =? and campo_8 is not null and campo_10 is null";
        final List<String> numActivosL = new ArrayList<String>();
        final ConexionOpr cnx = new ConexionOpr();
        final ConexionO cnxo = new ConexionO();
        final Connection conVlr = cnx.conectar().getConexion();
        final Connection conBdlocal = cnxo.conectar().getConexion();
        int n = 0;
        try {
            String imeisVlr = "";
            String imeisToAxis = "";
            String sAux = "";
            conBdlocal.setAutoCommit(false);
            final PreparedStatement pt = conBdlocal.prepareStatement(queryfistmpdatoshw);
            pt.setString(1, idSolicitud);
            final ResultSet rt = pt.executeQuery();
            while (rt.next()) {
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Servicio: " + rt.getString(1)));
                numActivosL.add(rt.getString(1));
            }
            rt.close();
            pt.close();
            for (final String num : numActivosL) {
                ++n;
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Extrayendo imeis desde vlr del numero HW: " + num));
                String periodoImei = "";
                String numTmp = num;
                final int lenght = numTmp.length();
                numTmp = numTmp.substring(lenght - 8);
                numTmp = "'593" + numTmp + "'";
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " numero tmp:" + numTmp));
                PreparedStatement pst = conVlr.prepareStatement(String.valueOf(queryDatos) + numTmp);
                ResultSet rs = pst.executeQuery();
                flag = true;
                while (rs.next()) {
                    if (flag) {
                        imeisToAxis = String.valueOf(rs.getString("valor")) + DigitoVerificador.obtieneDigitoVerificador(rs.getString("valor"));
                        imeisVlr = rs.getString("valor");
                        flag = false;
                    }
                    else {
                        sAux = String.valueOf(rs.getString("valor")) + DigitoVerificador.obtieneDigitoVerificador(rs.getString("valor"));
                        imeisToAxis = String.valueOf(imeisToAxis) + ",\n" + sAux;
                        imeisVlr = String.valueOf(imeisVlr) + "," + rs.getString("valor");
                    }
                }
                rs.close();
                pst.close();
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Imeis resultantes vlr del numero:" + num));
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " " + imeisToAxis));
                if (imeisVlr != null && !imeisVlr.isEmpty()) {
                    ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Obteniendo Periodo de ocurrencia por cada imei resultante del numero " + num + "..."));
                    boolean tmpflag = true;
                    final String[] imei = imeisVlr.split(",");
                    String[] array;
                    for (int length = (array = imei).length, i = 0; i < length; ++i) {
                        final String im = array[i];
                        pst = conVlr.prepareStatement(queryPeriodoimei);
                        pst.setString(1, String.valueOf(im) + "0");
                        pst.setString(2, numTmp.replace("'", ""));
                        rs = pst.executeQuery();
                        while (rs.next()) {
                            if (tmpflag) {
                                periodoImei = String.valueOf(im) + DigitoVerificador.obtieneDigitoVerificador(im) + " --> " + rs.getString("imei_periodo");
                                tmpflag = false;
                            }
                            else {
                                periodoImei = String.valueOf(periodoImei) + "\n" + im + DigitoVerificador.obtieneDigitoVerificador(im) + " --> " + rs.getString("imei_periodo");
                            }
                        }
                        ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Imei - Periodo"));
                        ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " " + periodoImei));
                        rs.close();
                        pst.close();
                    }
                }
                else {
                    imeisToAxis = (periodoImei = "Imei no registra trafica en la Red");
                }
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Numero " + num + " Actualizando BD local con imeis devueltos desde VLR.."));
                pst = conBdlocal.prepareStatement(queryUpdateImeiReal);
                pst.setString(1, imeisToAxis);
                pst.setString(2, idSolicitud);
                pst.setString(3, num);
                pst.executeUpdate();
                pst.close();
                pst = conBdlocal.prepareStatement(queryUpdatePeriodoImei);
                pst.setString(1, periodoImei);
                pst.setString(2, idSolicitud);
                pst.setString(3, num);
                pst.executeUpdate();
                pst.close();
                ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " Numero " + num + " actualizado!"));
                if (n == 100) {
                    n = 0;
                    conBdlocal.commit();
                }
            }
            conBdlocal.commit();
            ConsumoWs.log.info((Object)("solicitud: " + idSolicitud + " imeis actualizados correctamente!"));
        }
        catch (Exception e) {
            conBdlocal.rollback();
            ConsumoWs.log.error((Object)("solicitud: " + idSolicitud + " Proceso: " + proceso + ": " + e.getMessage()));
            ConsumoWs.log.error((Object)("solicitud: " + idSolicitud + " " + e.toString()), (Throwable)e);
            throw new Exception(e.toString());
        }
        finally {
            try {
                if (cnxo.getConexion() != null && !cnxo.getConexion().isClosed()) {
                    cnxo.getConexion().close();
                }
                if (cnx.getConexion() != null && !cnx.getConexion().isClosed()) {
                    cnx.getConexion().close();
                }
            }
            catch (Exception e2) {
                ConsumoWs.log.fatal((Object)("solicitud: " + idSolicitud + " Cerrar Conexion: " + e2.toString()), (Throwable)e2);
            }
        }
        try {
            if (cnxo.getConexion() != null && !cnxo.getConexion().isClosed()) {
                cnxo.getConexion().close();
            }
            if (cnx.getConexion() != null && !cnx.getConexion().isClosed()) {
                cnx.getConexion().close();
            }
        }
        catch (Exception e2) {
            ConsumoWs.log.fatal((Object)("solicitud: " + idSolicitud + " Cerrar Conexion: " + e2.toString()), (Throwable)e2);
        }
    }
    
    public static String[] consumeSreHuawei(final String tagsParametros, final String tipoTransaccion) {
        String mensRespuesta = null;
        String codRespuesta = null;
        final String[] respuesta = new String[4];
        final Properties props = new Propiedades().getPropiedades();
        try {
            final URL url = new URL(props.getProperty("sreUrl"));
            final SreSoapHttpStub ws = new SreSoapHttpStub(url, (Service)null);
            final SreReceptaTransaccionElement recepta = new SreReceptaTransaccionElement(props.getProperty("sreIdIntaciadw"), tipoTransaccion, props.getProperty("sreIdIntegradordw"), props.getProperty("sreCodAlternoDistribuidordw"), props.getProperty("sreUsuariodw"), props.getProperty("sreClavedw"), props.getProperty("DataSourseEISOPER"), new SreReceptaTransaccionElementParametros_Transaccion(convertXMLStringtoMessageElement(tagsParametros)));
            ConsumoWs.log.info((Object)("Parametros SRE " + tipoTransaccion));
            ConsumoWs.log.info((Object)("SreId_instancia\t\t\t\t--> " + recepta.getId_Instancia()));
            ConsumoWs.log.info((Object)("SreId_tipo_transaccion\t\t--> " + recepta.getId_Tipo_Transaccion()));
            ConsumoWs.log.info((Object)("SreId_integrador\t\t\t--> " + recepta.getId_Integrador()));
            ConsumoWs.log.info((Object)("SreCod_alterno_distribuidor\t--> " + recepta.getCod_Alterno_Distribuidor()));
            ConsumoWs.log.info((Object)("Usuario\t\t\t\t\t\t--> " + recepta.getUsuario()));
            ConsumoWs.log.info((Object)("Clave\t\t\t\t\t\t--> " + recepta.getClave()));
            ConsumoWs.log.info((Object)("DataSource\t\t\t\t\t--> " + recepta.getData_Source()));
            ConsumoWs.log.info((Object)"Parametros:");
            ConsumoWs.log.info((Object)tagsParametros);
            final SreReceptaTransaccionResponseElement response = ws.sreReceptaTransaccion(recepta);
            String result = response.getResult().get_any()[0].getAsString();
            ConsumoWs.log.info((Object)("REspuesta SRE " + tipoTransaccion + " " + result));
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            final String wsError = docu.getRootElement().getChild("WS_ERROR").getValue();
            final String wsMensError = docu.getRootElement().getChild("WS_MENSAJE_ERROR").getValue();
            if (wsError.equals("true")) {
                mensRespuesta = wsMensError;
                codRespuesta = "1";
                respuesta[0] = codRespuesta;
                respuesta[1] = wsMensError;
                return respuesta;
            }
            final Element tagPackResp = docu.getRootElement().getChild("PCK_RESPUESTA");
            codRespuesta = tagPackResp.getChildTextTrim("COD_RESPUESTA");
            mensRespuesta = tagPackResp.getChildTextTrim("MENSAJE");
            System.out.println("codRespuesta   " + codRespuesta);
            System.out.println("mensRespuesta   " + mensRespuesta);
            if (mensRespuesta == null || mensRespuesta.equals("")) {
                mensRespuesta = tagPackResp.getChildTextTrim("MSJ_ERROR");
            }
            if (mensRespuesta == null || mensRespuesta.equals("")) {
                mensRespuesta = tagPackResp.getChildTextTrim("MENSAJE_TECNICO");
            }
            if (codRespuesta == null || codRespuesta.isEmpty() || codRespuesta.equals("")) {
                codRespuesta = "1";
            }
            respuesta[0] = codRespuesta;
            respuesta[1] = mensRespuesta;
            respuesta[2] = tagPackResp.getChildTextTrim("RESULTADO");
            respuesta[3] = tagPackResp.getChildTextTrim("FECHA_CADENA");
            System.out.println("RESULTADO HW respuesta[2] " + respuesta[2]);
            System.out.println("FECHA_CADENA HW respuesta[2] " + respuesta[3]);
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)e.getMessage(), (Throwable)e);
            respuesta[0] = "1";
            respuesta[1] = "ERROR SRE: " + e.toString();
            respuesta[3] = (respuesta[2] = null);
        }
        return respuesta;
    }
    
    public static String[] consumeSreValidaSerImei(final String tagsParametros, final String tipoTransaccion) {
        String mensRespuesta = null;
        ConsumoWs.log.info((Object)"[SUD] ConsumeWs.consumeSreValidaSerImei");
        String codRespuesta = null;
        final String[] respuesta = new String[4];
        final Properties props = new Propiedades().getPropiedades();
        try {
            final URL url = new URL(props.getProperty("sreUrl"));
            final SreSoapHttpStub ws = new SreSoapHttpStub(url, (Service)null);
            final SreReceptaTransaccionElement recepta = new SreReceptaTransaccionElement(props.getProperty("sreIdIntaciadw"), tipoTransaccion, props.getProperty("sreIdIntegradordw"), props.getProperty("sreCodAlternoDistribuidordw"), props.getProperty("sreUsuariodw"), props.getProperty("sreClavedw"), props.getProperty("DataSourseEISOPER"), new SreReceptaTransaccionElementParametros_Transaccion(convertXMLStringtoMessageElement(tagsParametros)));
            ConsumoWs.log.info((Object)("Parametros SRE " + tipoTransaccion));
            ConsumoWs.log.info((Object)("SreId_instancia\t\t\t\t--> " + recepta.getId_Instancia()));
            ConsumoWs.log.info((Object)("SreId_tipo_transaccion\t\t--> " + recepta.getId_Tipo_Transaccion()));
            ConsumoWs.log.info((Object)("SreId_integrador\t\t\t--> " + recepta.getId_Integrador()));
            ConsumoWs.log.info((Object)("SreCod_alterno_distribuidor\t--> " + recepta.getCod_Alterno_Distribuidor()));
            ConsumoWs.log.info((Object)("Usuario\t\t\t\t\t\t--> " + recepta.getUsuario()));
            ConsumoWs.log.info((Object)("Clave\t\t\t\t\t\t--> " + recepta.getClave()));
            ConsumoWs.log.info((Object)("DataSource\t\t\t\t\t--> " + recepta.getData_Source()));
            ConsumoWs.log.info((Object)"Parametros:");
            ConsumoWs.log.info((Object)tagsParametros);
            final SreReceptaTransaccionResponseElement response = ws.sreReceptaTransaccion(recepta);
            ConsumoWs.log.info((Object)"[SUD] Uso de proceso FISK_API_MEDIDAS_CAUTELARES.SP_VALIDA_SERVICIOS_IMEI");
            String result = response.getResult().get_any()[0].getAsString();
            ConsumoWs.log.info((Object)("REspuesta SRE " + tipoTransaccion + " " + result));
            result = result.replaceAll("&", "&amp;");
            final SAXBuilder builder = new SAXBuilder();
            final Reader stringReader = new StringReader(result);
            final Document docu = builder.build(stringReader);
            final List list = docu.getRootElement().getChildren();
            final String wsError = docu.getRootElement().getChild("WS_ERROR").getValue();
            final String wsMensError = docu.getRootElement().getChild("WS_MENSAJE_ERROR").getValue();
            if (wsError.equals("true")) {
                mensRespuesta = wsMensError;
                codRespuesta = "1";
                respuesta[0] = codRespuesta;
                respuesta[1] = wsMensError;
                return respuesta;
            }
            final Element tagPackResp = docu.getRootElement().getChild("PCK_RESPUESTA");
            codRespuesta = tagPackResp.getChildTextTrim("COD_RESPUESTA");
            mensRespuesta = tagPackResp.getChildTextTrim("MENSAJE");
            System.out.println("codRespuesta   " + codRespuesta);
            System.out.println("mensRespuesta   " + mensRespuesta);
            if (mensRespuesta == null || mensRespuesta.equals("")) {
                mensRespuesta = tagPackResp.getChildTextTrim("MSJ_ERROR");
            }
            if (mensRespuesta == null || mensRespuesta.equals("")) {
                mensRespuesta = tagPackResp.getChildTextTrim("MENSAJE_TECNICO");
            }
            if (codRespuesta == null || codRespuesta.isEmpty() || codRespuesta.equals("")) {
                codRespuesta = "1";
            }
            respuesta[0] = codRespuesta;
            respuesta[1] = mensRespuesta;
            respuesta[2] = tagPackResp.getChildTextTrim("VALOR");
        }
        catch (Exception e) {
            ConsumoWs.log.error((Object)e.getMessage(), (Throwable)e);
            respuesta[0] = "1";
            respuesta[1] = "ERROR SRE: " + e.toString();
            respuesta[2] = "";
        }
        return respuesta;
    }
    
    public static void insertaAbonadoAx(final List<Abonado> listaAbonado) throws Exception {
        final String proceso = "Inserta Abonado";
        final ConexionO cnx = new ConexionO();
        final Connection con = cnx.conectar().getConexion();
        PreparedStatement cst = null;
        int n = 0;
        ConsumoWs.log.info((Object)"Insertando datos de abonado FIS_DATOS_SOLICITUDES");
        try {
            con.setAutoCommit(false);
            for (int i = 0; i < listaAbonado.size(); ++i) {
                ++n;
                cst = con.prepareStatement("INSERT INTO FIS_DATOS_SOLICITUD(ID_DATO_SOLICITUD,ID_SOLICITUD,CAMPO_1,CAMPO_2,CAMPO_3,CAMPO_4,CAMPO_5,CAMPO_6,CAMPO_7,CAMPO_8,CAMPO_9,CAMPO_10,CAMPO_11,CAMPO_12,CAMPO_13,CAMPO_14,CAMPO_15,CAMPO_16,CAMPO_17,CAMPO_18,CAMPO_19,CAMPO_20,CAMPO_21,CAMPO_22,CAMPO_23,CAMPO_24,CAMPO_25,CAMPO_26,CAMPO_27,CAMPO_28,CAMPO_29,CAMPO_30) VALUES (SEQ_FIS_DATOS_SOLICITUD.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                cst.setLong(1, Long.valueOf(listaAbonado.get(i).getID_SOLICITUD()));
                cst.setString(2, listaAbonado.get(i).getNOMBRE_COMPLETO());
                cst.setString(3, listaAbonado.get(i).getIMEI());
                cst.setString(4, listaAbonado.get(i).getIMSI());
                cst.setString(5, listaAbonado.get(i).getICCID());
                cst.setString(6, listaAbonado.get(i).getIDENTIFICACION());
                cst.setString(7, listaAbonado.get(i).getDIRECCION_COMPLETA());
                cst.setString(8, listaAbonado.get(i).getTELEFONO1());
                cst.setString(9, listaAbonado.get(i).getID_SERVICIO());
                cst.setString(10, listaAbonado.get(i).getFECHA_INICIO());
                cst.setString(11, listaAbonado.get(i).getFECHA_FIN());
                cst.setString(12, listaAbonado.get(i).getSUBPRODUCTO());
                cst.setString(13, listaAbonado.get(i).getCIUDAD());
                cst.setString(14, null);
                cst.setString(15, null);
                cst.setString(16, null);
                cst.setString(17, null);
                cst.setString(18, null);
                cst.setString(19, null);
                cst.setString(20, null);
                cst.setString(21, null);
                cst.setString(22, null);
                cst.setString(23, null);
                cst.setString(24, null);
                cst.setString(25, null);
                cst.setString(26, listaAbonado.get(i).getMIGRACION());
                cst.setString(27, null);
                cst.setString(28, null);
                cst.setString(29, listaAbonado.get(i).getDESCRIP_ADICIONAL());
                cst.setString(30, listaAbonado.get(i).getDESCRIP_VALOR());
                cst.setString(31, listaAbonado.get(i).getHOJAEXCEL());
                cst.executeUpdate();
                cst.close();
                if (n == 100) {
                    n = 0;
                    con.commit();
                }
            }
            con.commit();
            con.close();
        }
        catch (Exception ex) {
            con.rollback();
            ConsumoWs.log.error((Object)("Proceso: " + proceso + ": " + "Error al insertar datos del abonado Ax FIS_DATOS_SOLICITUD - ConsumoWs.insertaAbonado " + ex.getMessage()));
            ConsumoWs.log.error((Object)ex.toString(), (Throwable)ex);
            throw new Exception(ex.toString());
        }
        finally {
            try {
                if (cnx.getConexion() != null && !cnx.getConexion().isClosed()) {
                    cnx.getConexion().close();
                }
            }
            catch (Exception e2) {
                ConsumoWs.log.fatal((Object)("Cerrar Conexion: " + e2.toString()), (Throwable)e2);
            }
        }
        try {
            if (cnx.getConexion() != null && !cnx.getConexion().isClosed()) {
                cnx.getConexion().close();
            }
        }
        catch (Exception e2) {
            ConsumoWs.log.fatal((Object)("Cerrar Conexion: " + e2.toString()), (Throwable)e2);
        }
    }
    
    public static void procesaWSGeolocalizador(final String id_solicitud, final String valor, final String usuarioIngreso) throws Exception {
        final String proceso = "Procesa Geolocalizador";
        try {
            final Properties props = new Propiedades().getPropiedades();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            final Date fechaActual = new Date();
            final String fecha = sdf.format(fechaActual);
            sdf = new SimpleDateFormat("HH:mm");
            final String hora = sdf.format(fechaActual);
            String resultXml = "";
            final String user_app = props.getProperty("userAppWSGeo");
            final String clave_app = props.getProperty("claveAppWSGeo");
            final String canal_app = props.getProperty("canalAppWSGeo");
            final String entidad = props.getProperty("entidadWSGeo");
            final String Motivo = props.getProperty("MotivoWSGeo");
            final String Concesionario = props.getProperty("ConcesionarioWSGeo");
            final String Cod_Referencia = props.getProperty("CodReferenciaWSGeo");
            final String geoWSUrl = props.getProperty("geolocalizadorUrl");
            final String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dat=\"http://datos.claro/\"> <soapenv:Header/> <soapenv:Body> <dat:ConsultaLocalizacionWeb> <Numero>" + valor + "</Numero> " + "<Institucion>" + Concesionario + "</Institucion>" + "<Usuario>" + user_app + "</Usuario>" + "<Contrasenia>" + clave_app + "</Contrasenia> " + "<Fecha>" + fecha + " " + hora + "</Fecha>" + " <Motivo>" + Motivo + "</Motivo>" + " <Descripcion>" + Cod_Referencia + " para la solicitud " + id_solicitud + " ingresada por " + usuarioIngreso + "</Descripcion>" + "</dat:ConsultaLocalizacionWeb>" + "</soapenv:Body>" + "</soapenv:Envelope>";
            final String strXMLFilename = "request.xml";
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros de ingreso Ws GeoLocalizador xml: " + xml));
            final File input = ConvertString_File(xml, strXMLFilename);
            final PostMethod post = new PostMethod(geoWSUrl);
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Parametros de ingreso Ws GeoLocalizador:"));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " user_app --> " + user_app));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " clave_app --> " + clave_app));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " canal_app --> " + canal_app));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " entidad --> " + entidad));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Motivo --> " + Motivo));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Concesionario --> " + Concesionario));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " NumeroCelular --> " + valor));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Fecha --> " + fecha));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " hora --> " + hora));
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " Cod_Referencia --> " + Cod_Referencia));
            try {
                post.setRequestEntity((RequestEntity)new InputStreamRequestEntity((InputStream)new FileInputStream(input), input.length()));
                post.setRequestHeader("Content-type", "text/xml");
                post.setRequestHeader("Content-Length", String.valueOf(input.length()));
                final HttpClient httpclient = new HttpClient();
                httpclient.executeMethod((HttpMethod)post);
                resultXml = post.getResponseBodyAsString();
            }
            catch (Exception e) {
                post.releaseConnection();
                throw new Exception(e.toString());
            }
            finally {
                post.releaseConnection();
            }
            post.releaseConnection();
            resultXml = resultXml.replaceAll("&", "&amp;");
            final List<Suase> datos = new ArrayList<Suase>();
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " : " + resultXml));
            final String idRespuesta = getFullNameFromXmlStr(resultXml, "codigoResultado");
            ConsumoWs.log.info((Object)("solicitud: " + id_solicitud + " codigoResultado: " + idRespuesta));
            if (!idRespuesta.equals("0")) {
                final Suase suase = new Suase(id_solicitud, "N/A", (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, (String)null, "G");
                datos.add(suase);
                insertaSuase(datos);
                return;
            }
            final String nombre_completo = getFullNameFromXmlStr(resultXml, "propietario");
            final String provincia = getFullNameFromXmlStr(resultXml, "rbsProvincia");
            final String cantonCiudad = getFullNameFromXmlStr(resultXml, "rbsCiudad");
            final String direccion = getFullNameFromXmlStr(resultXml, "rbsDireccion");
            final String nombreCelda = getFullNameFromXmlStr(resultXml, "rbsNombre");
            final String celda = getFullNameFromXmlStr(resultXml, "rbsCelda");
            String longitud = getFullNameFromXmlStr(resultXml, "rbsLongitud");
            String latitud = getFullNameFromXmlStr(resultXml, "rbsLatitud");
            final String azimut = getFullNameFromXmlStr(resultXml, "rbsAzimut");
            longitud = caracteresLatitudlongitud(longitud);
            latitud = caracteresLatitudlongitud(latitud);
            final Suase suase = new Suase(id_solicitud, nombre_completo, provincia, cantonCiudad, direccion, celda, nombreCelda, longitud, latitud, azimut, "G");
            datos.add(suase);
            insertaSuase(datos);
        }
        catch (Exception e2) {
            ConsumoWs.log.error((Object)("solicitud: " + id_solicitud + " Proceso: " + proceso + ": " + e2.getMessage()));
            e2.printStackTrace();
            ConsumoWs.log.error((Object)e2.toString(), (Throwable)e2);
            throw new Exception(e2.toString());
        }
    }
    
    public static File ConvertString_File(final String xml, final String nameFile) {
        final File input = new File(nameFile);
        final String varXml = xml;
        try {
            final FileWriter w = new FileWriter(input);
            final BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(input), "UTF-8"));
            final PrintWriter wr = new PrintWriter(bw);
            wr.write(varXml);
            wr.close();
            bw.close();
        }
        catch (Exception ex) {}
        return input;
    }
    
    public static org.w3c.dom.Document loadXMLString(final String response) throws Exception {
        final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        final DocumentBuilder db = dbf.newDocumentBuilder();
        final InputSource is = new InputSource(new StringReader(response));
        return db.parse(is);
    }
    
    public static String getFullNameFromXmlStr(final String response, final String tagName) throws Exception {
        final org.w3c.dom.Document xmlDoc = loadXMLString(response);
        final NodeList nodeList = xmlDoc.getElementsByTagName(tagName);
        String valor = "";
        for (int i = 0; i < nodeList.getLength(); ++i) {
            valor = nodeList.item(i).getFirstChild().getNodeValue();
        }
        return valor;
    }
    
    public static boolean isNumeric(final String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        }
        catch (NumberFormatException excepcion) {
            resultado = false;
        }
        return resultado;
    }
}
