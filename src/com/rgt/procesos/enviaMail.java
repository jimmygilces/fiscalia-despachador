package com.rgt.procesos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;
import com.rgt.conexion.ConexionO;
import com.rgt.utils.Propiedades;
import org.apache.logging.log4j.Logger;

public class enviaMail
{
    static Logger log;
    
    static {
        enviaMail.log = Propiedades.getMyLogger();
    }
    
    public static void enviar(final Solicitud sol) throws Exception {
        final ConexionO cnx = new ConexionO();
        ArrayList<String> correos = new ArrayList<String>();
        final String idSolicitud = sol.getIdSolicitud();
        final String oficio = sol.getOficio();
        final String tipoReporte = sol.getTipoReporte();
        final String descServicio = sol.getDescServicio();
        String fechaIni = sol.getFecha_inicio();
        String fechaFin = sol.getFecha_fin();
        final String valor = sol.getServicio();
        String descTipoReporte = "";
        String descripcion = "";
        String codError = "";
        String msjError = "";
        final String userIngreso = ObtenerUserIngreso(idSolicitud);
        final String nombreArchivo = getFileName(idSolicitud);
        final String remitente = "medcau@claro.com.ec";
        String destinatario = "";
        final String titulo = "Importante - Solicitud: " + idSolicitud + " Historica";
        String mensaje = "";
        try {
            correos = ObtenerDestinatarios(2);
            for (int i = 0; i < correos.size(); ++i) {
                destinatario = String.valueOf(destinatario) + correos.get(i) + ",";
            }
            if (tipoReporte.equals("1")) {
                descTipoReporte = "Datos Generales del Abonado";
                if (descServicio.equals("NUMERO_CEDULA")) {
                    descripcion = "Numero de Cedula: ";
                }
                if (descServicio.equals("NUMERO_CELULAR")) {
                    descripcion = "Numero de Celular: ";
                }
                if (descServicio.equals("IMEI")) {
                    descripcion = String.valueOf(descServicio) + " :";
                }
                if (descServicio.equals("IMSI")) {
                    descripcion = String.valueOf(descServicio) + " :";
                }
                if (descServicio.equals("ICCID")) {
                    descripcion = String.valueOf(descServicio) + " :";
                }
                fechaIni = "--";
                fechaFin = "--";
            }
            if (tipoReporte.equals("2")) {
                descTipoReporte = "Informacion Tecnica y Posicionamiento del Equipo";
                descripcion = "Numero de Celular: ";
                fechaIni = "--";
                fechaFin = "--";
            }
            if (tipoReporte.equals("3")) {
                descTipoReporte = "Registro de Llamadas";
                descripcion = "Numero de Celular: ";
            }
            if (tipoReporte.equals("4")) {
                descripcion = "Numero de Celular: ";
                descTipoReporte = "Registro de Mensajes";
            }
            if (tipoReporte.equals("5")) {
                descTipoReporte = "Informacion por Radio Base";
                descripcion = "Celda: ";
            }
            if (tipoReporte.equals("6")) {
                descTipoReporte = "Ubicacion de Radio Base";
                descripcion = "Celda: ";
                fechaIni = "--";
                fechaFin = "--";
            }
            final Connection con = cnx.conectar().getConexion();
            mensaje = "Estimado Adminsitrador \n\nLa peticion para " + descTipoReporte + " de la solicitud: " + idSolicitud + " \n" + "No. de Tramite SRT: " + oficio + "\n" + "Corresponde a informacion historica\n\n" + "Datos de la Solicitud:\n" + "Usuario que ingreso la solicitud: " + userIngreso + "\n" + descripcion + valor + "\n" + "Fecha Inicio: " + fechaIni + "\n" + "Fecha    Fin: " + fechaFin + "\n\n" + "Este mensaje e-mail fue generado por el sistema. Por favor no responder a esta direccion.";
            final byte[] msjb = mensaje.getBytes("UTF8");
            final String mens = new String(msjb);
            enviaMail.log.info((Object)("solicitud: " + idSolicitud + " " + mens));
            final CallableStatement cst = con.prepareCall("{call FISK_API_PORTAL_SEGURIDAD.FISP_ENVIA_CORREO(?,?,?,?,?,?)}");
            cst.setString(1, mens);
            cst.setString(2, remitente);
            cst.setString(3, destinatario);
            cst.setString(4, titulo);
            cst.registerOutParameter(5, 12);
            cst.registerOutParameter(6, 12);
            cst.execute();
            codError = cst.getString(5);
            msjError = cst.getString(6);
            cst.close();
            con.close();
            if (codError.equals("1")) {
                throw new Exception("Error, Solicitud: " + idSolicitud + " " + msjError);
            }
        }
        catch (Exception e) {
            enviaMail.log.error((Object)("solicitud: " + idSolicitud + " " + e.getMessage()));
            e.printStackTrace();
            enviaMail.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception("Error al enviar Mail: " + e.getMessage());
        }
        finally {
            cnx.getConexion().close();
        }
        cnx.getConexion().close();
    }
    
    public static void enviaMailError(final String idSolicitud, final String proceso, final String estado, final String oficio, final String tipoReporte, final String msj) {
        final String procesos = "Envia Mail de Error";
        ArrayList<String> correos = new ArrayList<String>();
        String destinatario = "";
        String codError = "";
        String msjError = "";
        String userIngreso = "";
        final ConexionO cnx = new ConexionO();
        try {
            final Properties props = new Propiedades().getPropiedades();
            final String remitente = props.getProperty("remitenteError");
            correos = ObtenerDestinatarios(1);
            for (int i = 0; i < correos.size(); ++i) {
                destinatario = String.valueOf(destinatario) + correos.get(i) + ",";
            }
            final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            final Date fechaActual = new Date();
            final String fecha = sdf.format(fechaActual);
            userIngreso = ObtenerUserIngreso(idSolicitud);
            final String titulo = "Importante - Error al Procesar la Solicitud: " + idSolicitud;
            String mensaje = "";
            final Connection con = cnx.conectar().getConexion();
            mensaje = "Estimado Administrador:\n\nSe a producido un error en el proceso: " + proceso + ", de la solicitud: " + idSolicitud + " \n" + "Correspondiente al No. de Tramite SRT: " + oficio + "\n\n" + "Usuario que ingreso la solicitud: " + userIngreso + "\n" + "Tipo de Reporte: " + tipoReporte + " \n" + "Fecha de Error: " + fecha + " \n" + "Estado al momento del Error: " + estado + " \n" + "Detalle del Error:\n\n" + msj + "\n\n\n" + "Este mensaje e-mail fue generado por el sistema. Por favor no responder a esta direccion.";
            final byte[] msjb = mensaje.getBytes("UTF8");
            final String mens = new String(msjb);
            enviaMail.log.info((Object)("solicitud: " + idSolicitud + " " + mens));
            final CallableStatement cst = con.prepareCall("{CALL FISK_API_PORTAL_SEGURIDAD.FISP_ENVIA_CORREO(?,?,?,?,?,?)}");
            cst.setString(1, mens);
            cst.setString(2, remitente);
            cst.setString(3, destinatario);
            cst.setString(4, titulo);
            cst.registerOutParameter(5, 12);
            cst.registerOutParameter(6, 12);
            cst.execute();
            codError = cst.getString(5);
            msjError = cst.getString(6);
            con.close();
            if (codError.equals("1")) {
                throw new Exception("Error, Solicitud: " + idSolicitud + " " + msjError);
            }
        }
        catch (Exception e) {
            enviaMail.log.error((Object)("solicitud: " + idSolicitud + " Proceso: " + procesos + ": " + e.getMessage()));
            e.printStackTrace();
            enviaMail.log.error((Object)e.toString(), (Throwable)e);
        }
    }
    
    public static ArrayList<String> ObtenerDestinatarios(final int tipoDest) throws Exception {
        final String proceso = "Obtener Destinatarios";
        final ArrayList<String> correos = new ArrayList<String>();
        final ConexionO cnx = new ConexionO();
        try {
            final Connection con = cnx.conectar().getConexion();
            final Properties props = new Propiedades().getPropiedades();
            final String IdTipoParametro = props.getProperty("IdTipoParametro");
            String gvIdParametro;
            if (tipoDest == 1) {
                gvIdParametro = "GV_FIS_CORREOS_GSI";
            }
            else {
                gvIdParametro = "GV_FIS_CORREOS_HIST";
            }
            final String query = "select lower(g.valor) from gv_parametros g where g.id_tipo_parametro =" + IdTipoParametro + " AND g.id_parametro = '" + gvIdParametro + "'";
            final Statement st = con.createStatement();
            final ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                correos.add(rs.getString(1));
            }
            rs.close();
            st.close();
            con.close();
        }
        catch (Exception ex) {
            enviaMail.log.error((Object)("Proceso: " + proceso + ": " + ex.toString()), (Throwable)ex);
            ex.printStackTrace();
            enviaMail.log.error((Object)ex.toString(), (Throwable)ex);
            throw new Exception(ex.toString());
        }
        return correos;
    }
    
    public static String ObtenerUserIngreso(final String idSolicitud) throws Exception {
        final String proceso = "Obtener User de Ingreso";
        final ConexionO cnx = new ConexionO();
        try {
            String userSolicitud = "";
            final Connection con = cnx.conectar().getConexion();
            final String query = "select USUARIO_INGRESO from fis_solicitudes where ID_SOLICITUD=" + idSolicitud;
            final PreparedStatement pst = con.prepareStatement(query);
            final ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                userSolicitud = rs.getString(1);
                if (userSolicitud == null) {
                    rs.close();
                    throw new Exception("Solicitud: " + idSolicitud + " invalida o no existe");
                }
            }
            rs.close();
            pst.close();
            con.close();
            return userSolicitud;
        }
        catch (Exception ex) {
            enviaMail.log.error((Object)("Proceso: " + proceso + ": " + ex.toString()), (Throwable)ex);
            ex.printStackTrace();
            enviaMail.log.error((Object)ex.toString(), (Throwable)ex);
            throw new Exception(ex.toString());
        }
    }
    
    public static String getFileName(final String idSolicitud) throws Exception {
        final String proceso = "Get File Name";
        String usuarioIngreso = "";
        String tipoReporte = "";
        String nombreArchivo = "";
        try {
            final ConexionO cnx = new ConexionO();
            final Connection con = cnx.conectar().getConexion();
            final String query = "select USUARIO_INGRESO,ID_TIPO_REPORTE from fis_solicitudes where ID_SOLICITUD=" + idSolicitud;
            final PreparedStatement pst = con.prepareStatement(query);
            final ResultSet rs = pst.executeQuery();
            if (!rs.next()) {
                rs.close();
                throw new Exception("Solicitud: " + idSolicitud + " invalida o no existe");
            }
            usuarioIngreso = rs.getString(1);
            tipoReporte = rs.getString(2);
            rs.close();
            pst.close();
            con.close();
            nombreArchivo = String.valueOf(usuarioIngreso) + "_" + idSolicitud + "_" + tipoReporte + ".xlsx";
        }
        catch (Exception ex) {
            enviaMail.log.error((Object)("solicitud: " + idSolicitud + " Proceso: " + proceso + ": " + ex.getMessage()));
            ex.printStackTrace();
            enviaMail.log.error((Object)ex.toString(), (Throwable)ex);
            throw new Exception(ex.toString());
        }
        return nombreArchivo;
    }
    
    public static String ObtenerTipoREporte(final int idSolicitud) throws Exception {
        final String proceso = "Obtener Tipo de Reporte";
        try {
            final ConexionO cnx = new ConexionO();
            final Connection con = cnx.conectar().getConexion();
            String descTipoReporte = "";
            final String query = "select r.descripcion from fis_solicitudes s, fis_tipo_reporte r where s.id_tipo_reporte = r.id_tipo_reporte and s.id_solicitud =" + idSolicitud;
            final Statement st = con.createStatement();
            final ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                descTipoReporte = rs.getString(1);
                if (descTipoReporte == null) {
                    rs.close();
                    con.close();
                    throw new Exception("Error, Solicitud: " + idSolicitud + " Tipo de reporte no existe");
                }
            }
            rs.close();
            st.close();
            con.close();
            return descTipoReporte;
        }
        catch (Exception ex) {
            enviaMail.log.error((Object)("solicitud: " + idSolicitud + " Proceso: " + proceso + ": " + ex.getMessage()));
            ex.printStackTrace();
            enviaMail.log.error((Object)ex.toString(), (Throwable)ex);
            throw new Exception(ex.toString());
        }
    }
}