package com.rgt.procesos;

import java.sql.ResultSet;
import java.util.Date;
import java.util.Locale;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.sql.Statement;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.Properties;
import com.rgt.conexion.ConexionOpr;
import com.rgt.utils.Propiedades;
import org.apache.logging.log4j.Logger;

public class VistaTmpAbonado
{
    static Logger log;
    
    static {
        VistaTmpAbonado.log = Propiedades.getMyLogger();
    }
    
    public static void main(final String[] args) {
        try {
            createVistaTmp("6465", "2020-01-01 00:00:00", "2021-06-16 23:59:59");
        }
        catch (Exception e) {
            VistaTmpAbonado.log.error((Object)e.toString(), (Throwable)e);
        }
    }
    
    public static void CreaVistaTmp(final String idSolicitud, final String fechaIni, final String fechaFin) throws Exception {
        final Properties props = new Propiedades().getPropiedades();
        final String usertablaImsiimei = props.getProperty("usertablaImsiimei");
        final String bloque = "DECLARE\n\n   id_solicitud   VARCHAR2(100) := '" + idSolicitud + "';\n" + "   Ld_FechaInicio Date := TO_DATE(substr('" + fechaIni + "',1,10), 'DD/MM/YYYY');\n" + "   Ld_FechaFin    Date := TO_DATE(substr('" + fechaFin + "',1,10), 'DD/MM/YYYY');\n" + "   Ln_bandera     NUMBER := 0;\n" + "   Lv_Vista       VARCHAR(20000);\n" + "   Lv_NombreVista VARCHAR(100);\n" + "   Lv_NombreTabla VARCHAR(100);\n" + "\n" + "BEGIN\n" + "\n" + "   Lv_NombreVista := 'FIS_DATOS_AIC_' || id_solicitud;\n" + "\n" + "   Lv_Vista := 'CREATE OR REPLACE VIEW ' || Lv_NombreVista || ' AS';\n" + "\n" + "   loop\n" + "\n" + "     Lv_NombreTabla := 'IMSI_IMEI_' || TO_CHAR(Ld_FechaInicio,\n" + "'YYYYMMDD') || '_2';\n" + "\n" + "     if Ln_bandera = 0 then\n" + "       Ln_bandera := 1;\n" + "       Lv_Vista   := Lv_Vista || ' SELECT \"MSISDN\",\"IMEI\",\"IMSI\" FROM \n" + usertablaImsiimei + "' ||\n" + "                     Lv_NombreTabla;\n" + "     else\n" + "       Lv_Vista := Lv_Vista ||\n" + "                   ' UNION ALL SELECT \"MSISDN\",\"IMEI\",\"IMSI\" FROM VLR.' \n" + "||\n" + "                   Lv_NombreTabla;\n" + "     end if;\n" + "\n" + "     Ld_FechaInicio := Ld_FechaInicio + 1;\n" + "     EXIT WHEN Ld_FechaInicio > Ld_FechaFin;\n" + "   end loop;\n" + "\n" + "   EXECUTE IMMEDIATE Lv_Vista;\n" + "   END;\n";
        final ConexionOpr cnx = new ConexionOpr();
        final Connection con = cnx.conectar().getConexion();
        final CallableStatement cs = con.prepareCall(bloque);
        cs.execute();
        cs.close();
        con.close();
    }
    
    public static void eliminaVistaTmp(final String id_solicitud) throws Exception {
        final Properties props = new Propiedades().getPropiedades();
        if (props.getProperty("bandeliminaVistaAbonado").equalsIgnoreCase("N")) {
            return;
        }
        final String nombreVista = "FIS_DATOS_AIC_" + id_solicitud;
        final ConexionOpr cnx = new ConexionOpr();
        final Connection con = cnx.conectar().getConexion();
        Statement std = con.createStatement();
        std = con.createStatement();
        std.executeQuery("drop view " + nombreVista);
        std.close();
        con.close();
    }
    
    public static void createVistaTmp(final String idSolicitud, final String fechaIni, final String fechaFin) throws Exception {
        final Date fechaI = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaIni);
        final Date fechaF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaFin);
        final String nombreVista = "FIS_DATOS_AIC_" + idSolicitud;
        final String queryVista = "Select * from " + nombreVista;
        String nombreTabla = null;
        boolean primeraVuelta = true;
        final Properties props = new Propiedades().getPropiedades();
        final String usertablaImsiimei = props.getProperty("usertablaImsiimei");
        String queryCreateVista = null;
        String queryValidaTablas = "select table_name from USER_TABLES where table_name in ";
        String tramaIn = null;
        ConexionOpr cnx = null;
        final String usrToGrant = props.getProperty("usrOperaciones");
        final String grants = "grant select on " + nombreVista + " to " + usrToGrant;
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(fechaI);
        VistaTmpAbonado.log.info((Object)("[SUD] Vista a crear: " + nombreVista));
        do {
            nombreTabla = "imsi_imei_" + new SimpleDateFormat("yyyyMMdd").format(calendar.getTime()) + "_2";
            if (primeraVuelta) {
                tramaIn = "'" + nombreTabla + "'";
                primeraVuelta = false;
            }
            else {
                tramaIn = String.valueOf(tramaIn) + "," + "'" + nombreTabla + "'";
            }
            calendar.add(6, 1);
        } while (!calendar.getTime().after(fechaF));
        primeraVuelta = true;
        tramaIn = "(" + tramaIn + ")";
        queryValidaTablas = String.valueOf(queryValidaTablas) + tramaIn;
        queryValidaTablas = queryValidaTablas.toUpperCase(Locale.ENGLISH);
        cnx = new ConexionOpr();
        final Connection con = cnx.conectar().getConexion();
        Statement st = con.createStatement();
        final ResultSet rs = st.executeQuery(queryValidaTablas);
        while (rs.next()) {
            if (primeraVuelta) {
                primeraVuelta = false;
                queryCreateVista = "create or replace view " + nombreVista + " as";
                queryCreateVista = String.valueOf(queryCreateVista) + " SELECT MSISDN,IMEI,IMSI,'" + rs.getString("table_name") + "' TABLA_DIARIA FROM " + usertablaImsiimei + rs.getString("table_name");
            }
            else {
                queryCreateVista = String.valueOf(queryCreateVista) + " union all SELECT MSISDN,IMEI,IMSI,'" + rs.getString("table_name") + "' TABLA_DIARIA FROM " + usertablaImsiimei + rs.getString("table_name");
            }
        }
        rs.close();
        if (queryCreateVista == null || queryCreateVista.isEmpty()) {
            throw new Exception("Error, No existen tablas diarias imsi_imei_xxxxx para el rango de fechas: " + fechaIni + " - " + fechaFin);
        }
        st = con.createStatement();
        VistaTmpAbonado.log.info((Object)("[SUD] Query creaci\u00f3n de Vista: " + queryCreateVista));
        st.executeUpdate(queryCreateVista);
        st.close();
        if (props.getProperty("bandeliminaVistaAbonado").equalsIgnoreCase("N")) {
            st = con.createStatement();
            st.executeUpdate(grants);
            st.close();
        }
        con.close();
    }
}