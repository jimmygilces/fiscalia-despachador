package com.rgt.procesos;

import java.util.Date;
import com.rgt.timer.Demonio;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import com.rgt.conexion.ConexionO;
import java.util.ArrayList;

import com.rgt.utils.LogErrores;
import com.rgt.utils.Propiedades;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.List;
import org.apache.logging.log4j.Logger;

public class ProcesaSolicitudes
{
    static Logger log;
    private static List<Solicitud> listaSolicitudes;
    private static Connection cnx;
    public static int contSolP345;
    public static int contSolP126;
    public static int contSolHisP345;
    
    static {
        ProcesaSolicitudes.log = Propiedades.getMyLogger();
        ProcesaSolicitudes.listaSolicitudes = new ArrayList<Solicitud>();
        ProcesaSolicitudes.contSolP345 = 0;
        ProcesaSolicitudes.contSolP126 = 0;
        ProcesaSolicitudes.contSolHisP345 = 0;
    }
    
    public static void consultaDetallesSolicitud() throws Exception {
    	log.info("[SUD] -> Dentro de comsultaDetallesSolicitud");
    	
        try {
            if (ProcesaSolicitudes.cnx == null || ProcesaSolicitudes.cnx.isClosed()) {
                ProcesaSolicitudes.cnx = new ConexionO().conectar().getConexion();
            }
            
            final String nDiasHistorico = "0";
            final Properties props = new Propiedades().getPropiedades();
            final String lsHoraInicio = props.getProperty("HisHoraInicio");
            final String lsHoraFin = props.getProperty("HisHoraFin");
            Statement st = ProcesaSolicitudes.cnx.createStatement();
            final String query = "SELECT * FROM (               \n              SELECT DISTINCT S.ID_SOLICITUD,\n              S.OFICIO, \n\t\t\t   S.USUARIO_INGRESO, \n              MAX (NVL(CASE WHEN DR.CAMPO NOT IN ('FECHA_HORA_FIN','FECHA_HORA_INICIO') THEN DS.VALOR END,'0')) SERVICIO,\n              S.ID_TIPO_REPORTE,             \n              NVL(TO_CHAR(MIN(TO_DATE((CASE WHEN DR.CAMPO = 'FECHA_HORA_INICIO' THEN DS.VALOR END),'DD/MM/RRRR HH24:MI:SS')),'RRRR-MM-DD hh24:mi:ss'),'0') FECHA_INI,\n              NVL(TO_CHAR(MIN(TO_DATE((CASE WHEN DR.CAMPO = 'FECHA_HORA_FIN' THEN DS.VALOR END),'DD/MM/RRRR HH24:MI:SS')),'RRRR-MM-DD hh24:mi:ss'),'0') FECHA_FIN,\n              NVL(TO_CHAR(MIN(TO_DATE((CASE WHEN DR.CAMPO = 'FECHA_HORA_INICIO' THEN DS.VALOR END),'DD/MM/RRRR HH24:MI:SS')),'RRRR'),'0') ANIO,  \n              S.ESTADO,\n              S.TIPO_PROCESO,\n              MIN(CASE WHEN DR.CAMPO NOT IN ('FECHA_HORA_FIN','FECHA_HORA_INICIO') THEN DR.CAMPO END) DESCRIP_SERVICIO,\n              S.HILO \n              FROM FIS_DETALLE_SOLICITUD DS, \t\t\t   FIS_SOLICITUDES S, \t\t\t   FIS_DETALLE_REPORTE DR \n              WHERE DS.ID_SOLICITUD = S.ID_SOLICITUD\n              AND DS.ID_DETALLE_REPORTE= DR.ID_DETALLE_REPORTE  \n              AND S.ESTADO not in ('E','D') \n              AND S.HILO IS NULL\n\t\t\t\tAND S.TIPO_PROCESO='O'\n               GROUP BY S.ID_SOLICITUD, S.OFICIO,S.USUARIO_INGRESO, \n              S.ID_TIPO_REPORTE, \n              S.FECHA_INGRESO, S.ESTADO, S.TIPO_PROCESO , S.HILO              \n\t\t\t\tUNION ALL        \n\t\t\t\t  SELECT DISTINCT S.ID_SOLICITUD,  \n\t\t\t\t    S.OFICIO,    \n\t\t\t\tS.USUARIO_INGRESO,  \n\t\t\t\t    MAX (NVL(CASE WHEN DR.CAMPO NOT IN ('FECHA_HORA_FIN','FECHA_HORA_INICIO') THEN DS.VALOR END,'0')) SERVICIO,  \n\t\t\t\t    S.ID_TIPO_REPORTE,              \n\t\t\t\t    NVL(TO_CHAR(MIN(TO_DATE((CASE WHEN DR.CAMPO = 'FECHA_HORA_INICIO' THEN DS.VALOR END),'DD/MM/RRRR HH24:MI:SS')),'RRRR-MM-DD hh24:mi:ss'),'0') FECHA_INI, \n\t\t\t\t    NVL(TO_CHAR(MIN(TO_DATE((CASE WHEN DR.CAMPO = 'FECHA_HORA_FIN' THEN DS.VALOR END),'DD/MM/RRRR HH24:MI:SS')),'RRRR-MM-DD hh24:mi:ss'),'0') FECHA_FIN,  \n\t\t\t\t    NVL(TO_CHAR(MIN(TO_DATE((CASE WHEN DR.CAMPO = 'FECHA_HORA_INICIO' THEN DS.VALOR END),'DD/MM/RRRR HH24:MI:SS')),'RRRR'),'0') ANIO, \n\t\t\t\t    S.ESTADO,  \n\t\t\t\t    S.TIPO_PROCESO,  \n\t\t\t\t    MIN(CASE WHEN DR.CAMPO NOT IN ('FECHA_HORA_FIN','FECHA_HORA_INICIO') THEN DR.CAMPO END) DESCRIP_SERVICIO, \n\t\t\t\t    S.HILO  \n\t\t\t\t    FROM FIS_DETALLE_SOLICITUD DS, \n\t\t\t\tFIS_SOLICITUDES S,  \n\t\t\t\tFIS_DETALLE_REPORTE DR  \n\t\t\t\t    WHERE DS.ID_SOLICITUD = S.ID_SOLICITUD \n\t\t\t\t    AND DS.ID_DETALLE_REPORTE= DR.ID_DETALLE_REPORTE \n\t\t\t\t    AND S.ESTADO not in ('E','D')   \n\t\t\t\t    AND S.HILO IS NULL  \n\t\t\t\t\tAND S.TIPO_PROCESO='H' \n\t\t\t\t\tAND to_date(to_char(sysdate,'DD/MM/YYYY HH24:MI:SS'),'dd/mm/yyyy hh24:mi:ss')  BETWEEN \n\t\t\t\t\tTO_DATE(TO_CHAR(SYSDATE,   'DD/MM/YYYY')||' " + lsHoraInicio + "','DD/MM/YYYY HH24:MI:SS') AND \n" + "\t\t\t\t\t\t\t TO_DATE(TO_CHAR(SYSDATE+1, 'DD/MM/YYYY')||' " + lsHoraFin + "','DD/MM/YYYY HH24:MI:SS') \n" + "\t\t\t\t    GROUP BY S.ID_SOLICITUD, S.OFICIO,S.USUARIO_INGRESO,  \n" + "\t\t\t\t    S.ID_TIPO_REPORTE,  \n" + "\t\t\t\t    S.FECHA_INGRESO, S.ESTADO, S.TIPO_PROCESO , S.HILO\n" + "              ) X              \n" + "              ORDER BY X.ID_SOLICITUD ";
            st = ProcesaSolicitudes.cnx.createStatement();
            final ResultSet rs = st.executeQuery(query);
            
            log.info("[SUD] -> El query a ejecutar es: " + query);
            
            while (rs.next()) {
                final String idSolicitud = rs.getString("ID_SOLICITUD");
                final String oficio = rs.getString("OFICIO");
                final String servicio = rs.getString("SERVICIO");
                final String idTipoReporte = rs.getString("ID_TIPO_REPORTE");
                final String fechaInicio = rs.getString("FECHA_INI");
                final String fechaFin = rs.getString("FECHA_FIN");
                final String anio = rs.getString("ANIO");
                final String estado = rs.getString("ESTADO");
                final String tipoProceso = rs.getString("TIPO_PROCESO");
                final String descServicio = rs.getString("DESCRIP_SERVICIO");
                final String hilo = rs.getString("HILO");
                final String usuarioIngreso = rs.getString("USUARIO_INGRESO");
                final Solicitud sol = new Solicitud(idSolicitud, oficio, servicio, idTipoReporte, fechaInicio, fechaFin, anio, estado, tipoProceso, descServicio, hilo, usuarioIngreso);
                ProcesaSolicitudes.listaSolicitudes.add(sol);
            }
            rs.close();
            st.close();
        }
        catch (Exception ex) {
            ProcesaSolicitudes.log.error((Object)ex.toString(), (Throwable)ex);
            throw new Exception(ex.toString());
        }
    }
    
    public static void actualizaEstadoHilo(final String solicitudes) throws Exception {
        try {
            if (ProcesaSolicitudes.cnx == null || ProcesaSolicitudes.cnx.isClosed()) {
                ProcesaSolicitudes.cnx = new ConexionO().conectar().getConexion();
            }
            final Statement st = ProcesaSolicitudes.cnx.createStatement();
            st.executeUpdate("UPDATE FIS_SOLICITUDES S SET S.HILO = 'S' WHERE S.ID_SOLICITUD in(" + solicitudes + ")");
            st.close();
        }
        catch (Exception e) {
            throw new Exception("Error al actualizar Hilo de solicitudes: " + e);
        }
    }
    
    public static void seteaHiloProcesamiento(final String idSolicitud) throws Exception {
        try {
            if (ProcesaSolicitudes.cnx == null || ProcesaSolicitudes.cnx.isClosed()) {
                ProcesaSolicitudes.cnx = new ConexionO().conectar().getConexion();
            }
            final Statement st = ProcesaSolicitudes.cnx.createStatement();
            st.executeUpdate("UPDATE FIS_SOLICITUDES S SET S.HILO = 'S' WHERE S.ID_SOLICITUD =" + idSolicitud);
            st.close();
        }
        catch (Exception e) {
            throw new Exception("Error al actualizar Hilo de solicitudes" + e);
        }
    }
    
    public static void procesarSolicitudes() throws InterruptedException, Exception {
    	log.info("[SUD] -> Dentro de procesarSolicitudes");
    	
        final long initialTime = System.currentTimeMillis();
        final String tipoReportes = "345";
        boolean canAddToProcessStack = true;
        final int indice = 0;
        
        try {
        	log.info("[SUD] -> Se consulta el detalle de las solicitudes a procesar");
            consultaDetallesSolicitud();
            
            /*prueba*/
            //final Properties props = new Propiedades().getPropiedades();
            //final String dirPrincipal = props.getProperty("dirPrincipal");
            //final String javaPath8 = props.getProperty("javaPath8");
            //final String procesoEjecuta = "C:\\Program Files\\Java\\jdk1.8.0_202\\bin\\" + "java.exe -jar " + "C:\\Users\\Desarrollo\\Desktop\\medcaud\\Modificados\\" + "ArchivosTransferencia.jar 39393";
            
            //Solicitud.ejecutaJar(procesoEjecuta, "39393", "PRUEBA", "G");
            /*******/
            
            
            if(ProcesaSolicitudes.listaSolicitudes.isEmpty()) log.info("[SUD] -> No se encontraron solicitudes para procesar.");
            else log.info("[SUD] -> Se encontraron " + listaSolicitudes.size());
            
            while (!ProcesaSolicitudes.listaSolicitudes.isEmpty()) {
                canAddToProcessStack = true;
                if ("345".indexOf(ProcesaSolicitudes.listaSolicitudes.get(0).getTipoReporte()) >= 0) {
                    if (ProcesaSolicitudes.listaSolicitudes.get(0).getTipoProceso().equalsIgnoreCase("O")) {
                        if (ProcesaSolicitudes.contSolP345 >= Demonio.maxHilos345) {
                            canAddToProcessStack = false;
                            ProcesaSolicitudes.listaSolicitudes.remove(0);
                        }
                    }
                    else if (ProcesaSolicitudes.contSolHisP345 >= Demonio.maxHilosHis345) {
                        canAddToProcessStack = false;
                        ProcesaSolicitudes.listaSolicitudes.remove(0);
                    }
                }
                else if (ProcesaSolicitudes.contSolP126 >= Demonio.maxHilos126) {
                    canAddToProcessStack = false;
                    ProcesaSolicitudes.listaSolicitudes.remove(0);
                }
                if (canAddToProcessStack) {
                    ProcesaSolicitudes.log.info((Object)("Inicia nuevo Hilo " + new Date()));
                    final Solicitud sol = new Solicitud(ProcesaSolicitudes.listaSolicitudes.get(0).getIdSolicitud(), ProcesaSolicitudes.listaSolicitudes.get(0).getOficio(), ProcesaSolicitudes.listaSolicitudes.get(0).getServicio(), ProcesaSolicitudes.listaSolicitudes.get(0).getTipoReporte(), ProcesaSolicitudes.listaSolicitudes.get(0).getFecha_inicio(), ProcesaSolicitudes.listaSolicitudes.get(0).getFecha_fin(), ProcesaSolicitudes.listaSolicitudes.get(0).getAnio(), ProcesaSolicitudes.listaSolicitudes.get(0).getEstado(), ProcesaSolicitudes.listaSolicitudes.get(0).getTipoProceso(), ProcesaSolicitudes.listaSolicitudes.get(0).getDescServicio(), ProcesaSolicitudes.listaSolicitudes.get(0).getHilo(), ProcesaSolicitudes.listaSolicitudes.get(0).getUsuarioIngreso(), initialTime);
                    seteaHiloProcesamiento(sol.getIdSolicitud());
                    if ("345".indexOf(sol.getTipoReporte()) >= 0) {
                        if (sol.getTipoProceso().equalsIgnoreCase("O")) {
                            ++ProcesaSolicitudes.contSolP345;
                        }
                        else {
                            ++ProcesaSolicitudes.contSolHisP345;
                        }
                    }
                    else {
                        ++ProcesaSolicitudes.contSolP126;
                    }
                    ProcesaSolicitudes.listaSolicitudes.remove(0);
                    sol.start();
                    Thread.sleep(1000L);
                }
            }
        }
        catch (Exception e) {
            ProcesaSolicitudes.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e);
        }
        finally {
            try {
                if (ProcesaSolicitudes.cnx != null && !ProcesaSolicitudes.cnx.isClosed()) {
                    ProcesaSolicitudes.cnx.close();
                }
            }
            catch (Exception e2) {
                ProcesaSolicitudes.log.fatal((Object)("Cerrar Conexion: " + e2.toString()), (Throwable)e2);
            }
        }
        try {
            if (ProcesaSolicitudes.cnx != null && !ProcesaSolicitudes.cnx.isClosed()) {
                ProcesaSolicitudes.cnx.close();
            }
        }
        catch (Exception e2) {
            ProcesaSolicitudes.log.fatal((Object)("Cerrar Conexion: " + e2.toString()), (Throwable)e2);
        }
        try {
            if (ProcesaSolicitudes.cnx != null && !ProcesaSolicitudes.cnx.isClosed()) {
                ProcesaSolicitudes.cnx.close();
            }
        }
        catch (Exception e3) {
            ProcesaSolicitudes.log.fatal((Object)("Cerrar Conexion: " + e3.toString()), (Throwable)e3);
        }
    }
    
    public static String consultaGvParametros() throws Exception {
        String bandera = "0";
        final String proceso = "Consulta Gv Parametros";
        try {
            final Properties props = new Propiedades().getPropiedades();
            final String idTipoParametro = props.getProperty("IdTipoParametro");
            final ConexionO con = new ConexionO();
            con.conectar();
            final String sqlGvParametro = "SELECT P.VALOR FROM GV_PARAMETROS P WHERE P.ID_TIPO_PARAMETRO =" + idTipoParametro + " AND P.NOMBRE = 'GV_BANDERA_PROCESO'";
            final ResultSet rs = con.consultar(sqlGvParametro);
            while (rs.next()) {
                bandera = rs.getString("VALOR");
            }
            rs.close();
            con.getConexion().close();
        }
        catch (Exception e) {
            ProcesaSolicitudes.log.error((Object)("Proceso: Consulta Gv Parametros: " + e.toString()), (Throwable)e);
            e.printStackTrace();
            ProcesaSolicitudes.log.error((Object)e.toString(), (Throwable)e);
            throw new Exception(e.getMessage());
        }
        return bandera;
    }
}