package com.rgt.timer;

import java.util.Properties;

import org.apache.logging.log4j.Logger;

import com.rgt.procesos.ProcesaSolicitudes;
import com.rgt.utils.Propiedades;

public class Demonio {
	static Logger log = Propiedades.getMyLogger();

	public static int maxHilos345;
	public static int maxHilos126;
	public static int maxHilosHis345;

	public static void main(String args[]) {
		try {
			
			System.setProperty("java.awt.headless", "true");

			Properties props = new Propiedades().getPropiedades();
			int segundos = Integer.parseInt(props.getProperty("segEjecucion"));

			maxHilos126 = Integer.parseInt(props.getProperty("nHilos"));
			maxHilos345 = Integer.parseInt(props.getProperty("nHilos345"));
			maxHilosHis345 = Integer.parseInt(props.getProperty("nHiloHist345"));
			Demonio dem;

			while (true) {
				dem = new Demonio();
				dem.ejecutaDemonio();
				Thread.sleep(segundos*1000);
			}

		} catch (Exception ex) {

			log.fatal(ex.toString(), ex);

		}

	}

	public void ejecutaDemonio() {
		// String proceso="ejecutaDemonio";
		// segundos++;
		log.info("UP");
 
		try {
			log.info("[SUD] -> Se procede a Procesar solicitudes");
			ProcesaSolicitudes.procesarSolicitudes();
 

		} catch (Exception e) {
			// log.error("Proceso: "+proceso+": "+e.toString(),e);
			e.printStackTrace();
			log.error(e.toString(), e);
		}

	}

}
