package com.rgt.timer;

import java.awt.Toolkit;
import java.util.Timer;
import java.util.TimerTask;
import com.rgt.utils.Propiedades;

import org.apache.logging.log4j.Logger;
/**
 * Simple demo that uses java.util.Timer to schedule a task to execute once 5
 * seconds have passed.
 */
public class ReminderBeep {
	
	static Logger log = Propiedades.getMyLogger();

    Toolkit toolkit;
    Timer timer;

    public ReminderBeep(int seconds) {
        toolkit = Toolkit.getDefaultToolkit();
        timer = new Timer();
        timer.schedule(new RemindTask(), seconds * 1000);
    }

    class RemindTask extends TimerTask {

        public void run() {
        	log.info("Time's up!");
            toolkit.beep();
            //timer.cancel(); //Not necessary because we call System.exit
            System.exit(0); //Stops the AWT thread (and everything else)
        }
    }

    public static void main(String args[]) {
    	log.info("About to schedule task.");
        new ReminderBeep(5);
        log.info("Task scheduled.");
    }
}
