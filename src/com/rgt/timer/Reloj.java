package com.rgt.timer;

 
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask; 
 

import org.apache.logging.log4j.Logger;

import com.rgt.utils.Propiedades;

public class Reloj {
	
	static Logger log = Propiedades.getMyLogger();
	
	//13/11/2015 - jjalon despacho por bloques
	public static int numBloques;
	public static List<String> hilosEnProceso;
    Timer timer = new Timer(); // El timer que se encarga de administrar los tiempo de repeticion
    //public int segundos; // manejar el valor del contador
    public int numHilo;
    public boolean frozen; // manejar el estado del contador TIMER AUTOMATICO -- True Detenido | False Corriendo

    // clase interna que representa una tarea, se puede crear varias tareas y asignarle al timer luego
    class MiTarea extends TimerTask {

        public void run() {
        	String proceso="Run";
            //segundos++;
        	log.info("UP - hilos en proceso: "+hilosEnProceso.size()+" de: "+numBloques);
            // aqui se puede escribir el codigo de la tarea que necesitamos ejecutar
            try {
            	if (hilosEnProceso.size()<numBloques)
            	{
            		//ProcesaSolicitudes.consultaDetallesSolicitud();
            	}
				
			} catch (Exception e) {
				log.error("Proceso: "+proceso+": "+e.toString(),e);
				e.printStackTrace();
				log.error(e.toString(),e);
			}
        }// end run()
    }// end SincronizacionAutomatica

    public void Start(int pSeg, int pnumHilo) throws Exception {
        frozen = false;
        numHilo = pnumHilo;
        // le asignamos una tarea al timer
        timer.schedule(new MiTarea(), 0, pSeg * 1000); //* 10 * 60);
    }// end Start

    public void Stop() {
    	log.info("Stop");
        frozen = true;
    }// end Stop

    public void Reset() {
    	log.info("Reset");
        frozen = true;
        //segundos = 0;
    }// end Reset
    
    public static void main(String... args){
        Reloj mireloj = new Reloj();
        
        try {
        	
        	
        	Properties props= new Propiedades().getPropiedades();
        	int  segundos =Integer.parseInt(props.getProperty("segEjecucion"));
        	int numHilos =Integer.parseInt(props.getProperty("nHilos"));
        	
        	//13/11/2015 jjalon **************************************************** 
        	//controlar despacho de solicitudes
        	// DEspacho por bloques
        	numBloques = Integer.parseInt(props.getProperty("numBloques"));
        	hilosEnProceso = new ArrayList<String>();
        	//**********************************************************************
        	
            mireloj.Start(segundos, numHilos);
            mireloj.Stop();
            
        } catch (Exception ex) {
            //Logger.getLogger(Reloj.class.getName()).log(Level.SEVERE, null, ex);
        	//ex.printStackTrace();
        	log.error(ex.toString(),ex);
        	//LogErrores.escribirLog(ex.toString());
        }
    }
}// end Reloj
